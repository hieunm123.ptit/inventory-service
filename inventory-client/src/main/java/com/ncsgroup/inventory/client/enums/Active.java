package com.ncsgroup.inventory.client.enums;

import java.util.Objects;

public enum Active {

  ACTIVE(0),

  IN_ACTIVE(1);
  private final int value;

  private Active(int value) {
    this.value = value;
  }

  public static Active valueOf(Integer value) {
    for (Active active : values()) {
      if (Objects.equals(active.getValue(), value)) {
        return active;
      }
    }
    throw new IllegalArgumentException("Invalid ThresholdTime value: " + value);
  }

  public Integer getValue() {
    return value;
  }
}
