package com.ncsgroup.inventory.client.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import com.ncsgroup.inventory.client.annotation.ValidationPhoneNumber;

import static com.ncsgroup.inventory.client.constanst.Constants.Validate.INVALID_ADDRESS;
import static com.ncsgroup.inventory.client.constanst.Constants.Validate.INVALID_NAME;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class WarehouseRequest {
  @NotBlank(message = INVALID_NAME)
  private String name;
  @ValidationPhoneNumber
  private String phoneNumber;
  @NotBlank(message = INVALID_ADDRESS)
  private String addressId;


}
