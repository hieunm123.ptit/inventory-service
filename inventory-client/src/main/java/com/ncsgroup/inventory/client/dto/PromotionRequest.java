package com.ncsgroup.inventory.client.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.client.annotation.ValidationFormatDate;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.ncsgroup.inventory.client.constanst.Constants.Validate.NOT_NULL;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PromotionRequest {
  @ValidationFormatDate
  private String startDay;
  @ValidationFormatDate
  private String endDay;
  @NotNull(message = NOT_NULL)
  private Integer type;
  private Double discountPercent;
  private Integer moneyDeducted;
  private String description;
  private Integer coin;

}
