package com.ncsgroup.inventory.client.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductWarehouseRequest {
  private String warehouseId;
  private String productItemCode;
  private Double importPrice;
  private Double exportPrice;
  private Integer quantity;
  private Integer quantitySold;
  private String currencyCode;
}
