package com.ncsgroup.inventory.client.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@AllArgsConstructor
@NoArgsConstructor
public class ClassificationRequest {
  private String id;
  private String attributeId;
  private String value;
  private ProductItemRequest productItem;
  private List<ClassificationRequest> subClassifications;
}