package com.ncsgroup.inventory.client.constanst;

public class Constants {
  private Constants() {
  }

  public static class Validate {
    private Validate() {
    }

    public static final String INVALID_NAME = "Invalid name";
    public static final String INVALID_PHONE_NUMBER = "Invalid Phone Number";
    public static final String INVALID_ADDRESS = "Invalid Address";
    public static final String INVALID_CODE = "Invalid code";
    public static final String INVALID_DATE = "Invalid date";
    public static final String NOT_BLANK = "Not blank";
    public static final String NOT_NULL = "Not null";
  }

}
