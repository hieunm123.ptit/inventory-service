package com.ncsgroup.inventory.client.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.client.enums.Active;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductAdminFilterRequest {
  private String code;
  private String productName;
  private Integer minPrice;
  private Integer maxPrice;
  private Integer quantitySold;
  private Integer quantityRemaining;
  private Integer range;
  private List<String> categories;
  private List<String> warehouses;
  private List<AttributeProductRequest> attributes;
  @Enumerated(EnumType.ORDINAL)
  private Active active;
}
