package com.ncsgroup.inventory.client.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PromotionSearchRequest {
  private String code;
  private String fromStartDate;
  private String toStartDate;
  private String fromEndDate;
  private String toEndDate;
  private Integer type;
  private Double discountPercent;
  private Integer moneyDeducted;
  private Integer isActive;
}
