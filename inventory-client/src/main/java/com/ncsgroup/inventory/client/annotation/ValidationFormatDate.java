package com.ncsgroup.inventory.client.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.text.ParseException;
import java.text.SimpleDateFormat;


import static com.ncsgroup.inventory.client.constanst.Constants.Validate.INVALID_DATE;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidationFormatDate.DateValidate.class)
public @interface ValidationFormatDate {
  String message() default INVALID_DATE;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};


  class DateValidate implements ConstraintValidator<ValidationFormatDate, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
      if (value == null) {
        return true;
      }

      // Kiểm tra xem giá trị đã nhập có khớp với định dạng ngày/tháng/năm hay không
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
      dateFormat.setLenient(false);

      try {
        dateFormat.parse(value);
        return true;
      } catch (ParseException e) {
        return false;
      }
    }
  }
}