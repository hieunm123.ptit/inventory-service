package com.ncsgroup.inventory.client.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductAttributeUpdateRequest {
  private String id;
  private String attributeId;
  private String productItemId;
  private String value;
  private String code;
  private List<ProductAttributeUpdateRequest> updateRequests;
}
