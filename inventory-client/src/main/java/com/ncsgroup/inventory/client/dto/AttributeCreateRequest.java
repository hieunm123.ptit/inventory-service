package com.ncsgroup.inventory.client.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.client.annotation.ValidationName;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.ncsgroup.inventory.client.constanst.Constants.Validate.INVALID_NAME;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AttributeCreateRequest {
  @ValidationName
  @NotBlank(message = INVALID_NAME)
  private String name;
}
