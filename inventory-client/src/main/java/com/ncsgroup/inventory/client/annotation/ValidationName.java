package com.ncsgroup.inventory.client.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.ncsgroup.inventory.client.constanst.Constants.Validate.INVALID_NAME;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidationName.NameValidate.class)
public @interface ValidationName {
  String message() default INVALID_NAME;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};


  class NameValidate implements ConstraintValidator<ValidationName, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
      if (value == null) {
        return true;
      }
      return value.matches("^[a-zA-Z0-9\\s]*$");
    }
  }
}
