package com.ncsgroup.inventory.client.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import static com.ncsgroup.inventory.client.constanst.Constants.Validate.*;

@AllArgsConstructor
@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductRequest {
  @NotBlank(message = INVALID_CODE)
  private String code;
  @NotBlank(message = INVALID_NAME)
  private String name;
  private String unit;
  private Integer type;
  private String origin;
  private String description;
  @NotBlank
  private String sellerId;
  private List<String> imageIds;
  private List<String> categoryId;
  private List<AttributeProductRequest> attributes;
  private List<ClassificationRequest> classifications;
  @NotNull(message = NOT_NULL)
  private List<ProductWarehouseRequest> warehouses;

}
