package com.ncsgroup.inventory.client.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.client.annotation.ValidationName;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CategoryCreateRequest {
  @ValidationName
  @NotBlank(message = "Name don't be blank")
  private String name;
  private String imageId;
  private Integer isActive;
  @Valid
  private List<CategoryCreateRequest> categoryRequests;
}
