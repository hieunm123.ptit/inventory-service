package com.ncsgroup.inventory.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncsgroup.inventory.client.dto.CartItemRequest;
import com.ncsgroup.inventory.client.dto.product.ProductCartDeletionRequest;
import com.ncsgroup.inventory.client.dto.product.ProductCartRequest;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.exception.cart_item.CartItemInvalidException;
import com.ncsgroup.inventory.server.exception.cart_item.FieldKeyNotFoundException;
import com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException;
import com.ncsgroup.inventory.server.exception.product.ProductNotFoundException;
import com.ncsgroup.inventory.server.facade.CartRedisFacadeService;
import com.ncsgroup.inventory.server.service.MessageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CartController.class)
class CartControllerTest {

  @MockBean
  CartRedisFacadeService cartRedisFacadeService;
  @Autowired
  ObjectMapper objectMapper;
  @Autowired
  MockMvc mockMvc;

  @Autowired
  CartController cartController;

  @MockBean
  MessageService messageService;

  private static final String END_POINT_PATH = "/api/v1/carts";

  private CartItemRequest mockCartItemRequest() {
    return new CartItemRequest(
          "productId",
          "productItemId",
          3
    );
  }

  private ProductCartRequest mockProductCartRequest() {
    List<CartItemRequest> mock = new ArrayList<>();
    mock.add(mockCartItemRequest());
    mock.add(mockCartItemRequest());

    return new ProductCartRequest(mock);
  }


  private ProductCartDeletionRequest mockProductCartDeletionRequest() {
    return new ProductCartDeletionRequest(
          "productIds",
          "productItemIds"
    );
  }

  private CartItemResponse mockCartItemResponse() {
    return new CartItemResponse(
          "product",
          "productItem",
          "ao ret",
          10,
          1000.0,
          "xanh",
          "id",
          "xL",
          "id1"
    );
  }

  private List<CartItemResponse> mockListCartItemResponse() {
    List<CartItemResponse> list = new ArrayList<>();
    list.add(mockCartItemResponse());

    return list;
  }


  @Test
  void testAddProductToCart_WhenIdProductNotFound_ReturnProductNotFoundException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new ProductNotFoundException())
          .when(cartRedisFacadeService).addProductToCart("test", mockProductCartRequest());


    mockMvc.perform(
                post(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductNotFoundException"))
          .andDo(print());

  }

  @Test
  void testAddProductToCart_WhenIdProductItemNotFound_ReturnProductItemNotFoundException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new ProductItemCodeNotFoundException())
          .when(cartRedisFacadeService).addProductToCart("test", mockProductCartRequest());

    mockMvc.perform(
                post(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException"))
          .andDo(print());

  }

  @Test
  void testAddProductToCart_WhenAddSuccess_ReturnMessage200() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doNothing()
          .when(cartRedisFacadeService).addProductToCart("test", mockProductCartRequest());

    mockMvc.perform(
                post(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());
  }

  @Test
  void testAddProductToCart_WhenCartItemInvalid_ReturnCartItemInvalidException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new CartItemInvalidException())
          .when(cartRedisFacadeService).addProductToCart("test", mockProductCartRequest());

    mockMvc.perform(
                post(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.CartItem.CartItemInvalidException"))
          .andDo(print());

  }


  @Test
  void testDeleteProductInCart_WhenFieldKeyNotFound_ReturnFieldKeyNotFoundException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new FieldKeyNotFoundException())
          .when(cartRedisFacadeService).deleteProductInCart("test", mockProductCartDeletionRequest());

    mockMvc.perform(
                delete(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartDeletionRequest())))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.CartItem.FieldKeyNotFoundException"))
          .andDo(print());
  }


  @Test
  void testDeleteProductInCart_WhenDeleteSuccess_ReturnMessage200() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doNothing()
          .when(cartRedisFacadeService).deleteProductInCart("test", mockProductCartDeletionRequest());

    mockMvc.perform(
                delete(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartDeletionRequest())))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());
  }

  @Test
  void testUpdateProductInCart_WhenIdProductNotFound_Return404AndException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new ProductNotFoundException())
          .when(cartRedisFacadeService).updateProductInCart("test", mockProductCartRequest());

    mockMvc.perform(
                put(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductNotFoundException"))
          .andDo(print());

  }

  @Test
  void testUpdateProductInCart_WhenIdProductItemNotFound_Return404AndException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new ProductItemCodeNotFoundException())
          .when(cartRedisFacadeService).updateProductInCart("test", mockProductCartRequest());

    mockMvc.perform(
                put(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException"))
          .andDo(print());

  }

  @Test
  void testUpdateProductInCart_WhenSuccess_Return200AndMessage() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doNothing()
          .when(cartRedisFacadeService).updateProductInCart("test", mockProductCartRequest());

    mockMvc.perform(
                put(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());

  }

  @Test
  void testGetProductFromCart_WhenSuccess_Return200AndResponseBody() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");

    Mockito.when(cartRedisFacadeService.getProductFromCart("test")).thenReturn(mockListCartItemResponse());

    MvcResult result = mockMvc.perform(
                get(END_POINT_PATH + "/{customer_id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductCartRequest())))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(cartController.getProductFromCart("test", "en")));

  }
}


