package com.ncsgroup.inventory.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncsgroup.inventory.client.dto.product.*;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.dto.response.product.*;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.exception.attribute.AttributeNotFoundException;
import com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException;
import com.ncsgroup.inventory.server.exception.product.*;
import com.ncsgroup.inventory.server.exception.promotion.PromotionAlreadyExistException;
import com.ncsgroup.inventory.server.exception.promotion.PromotionNotFoundException;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException;
import com.ncsgroup.inventory.server.facade.ProductFacadeService;
import com.ncsgroup.inventory.server.service.MessageService;
import com.ncsgroup.inventory.server.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.inventory.client.constanst.Constants.Validate.*;
import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageCode.CREATE_PRODUCT_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.UPDATE_PRODUCT_SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
class ProductControllerTest {
  @MockBean
  ProductFacadeService productFacadeService;
  @MockBean
  ProductService productService;
  @MockBean
  MessageService messageService;

  @Autowired
  ObjectMapper objectMapper;
  @Autowired
  MockMvc mockMvc;
  @Autowired
  ProductController productController;


  private static final String END_POINT_PATH = "/api/v1/products";

  private List<String> categoriesId() {
    List<String> categoryIds = new ArrayList<>();
    categoryIds.add("CategoryId1");
    categoryIds.add("CategoryId2");
    return categoryIds;
  }

  private List<CategoryProductResponse> mockCategoryResponse() {
    List<CategoryProductResponse> mock = new ArrayList<>();
    mock.add(new CategoryProductResponse("CategoryId1", "test1"));
    mock.add(new CategoryProductResponse("CategoryId2", "test2"));
    return mock;
  }


  private List<String> warehouseId() {
    List<String> warehouse = new ArrayList<>();
    warehouse.add("w1");
    warehouse.add("w2");
    return warehouse;
  }


  private List<AttributeProductRequest> mockAttributeProductRequest() {


    AttributeProductRequest request1 = new AttributeProductRequest("attributeId1", "value1");
    AttributeProductRequest request2 = new AttributeProductRequest("attributeId2", "value2");

    List<AttributeProductRequest> attributeProductRequests = new ArrayList<>();
    attributeProductRequests.add(request1);
    attributeProductRequests.add(request2);

    return attributeProductRequests;
  }

  private List<ProductAttributeResponse> mockAttributeProductResponse() {

    ProductAttributeResponse response1 = new ProductAttributeResponse(
          "attributeId1",
          "color1",
          "value1",
          true
    );
    ProductAttributeResponse response2 = new ProductAttributeResponse(
          "attributeId2",
          "color2",
          "value2",
          true
    );

    List<ProductAttributeResponse> productAttributeResponses = new ArrayList<>();
    productAttributeResponses.add(response1);
    productAttributeResponses.add(response2);

    return productAttributeResponses;
  }

  private List<String> promotionId() {
    List<String> promotion = new ArrayList<>();
    promotion.add("p1");
    promotion.add("p2");
    return promotion;
  }

  private ProductPromotionRequest mockProductPromotionRequest() {
    return new ProductPromotionRequest(promotionId());
  }

  private ProductItemRequest mockProductItemRequest(String code) {
    return new ProductItemRequest(code);
  }

  private ProductItemResponse mockProductItemResponse(String id, String code) {
    return ProductItemResponse.of(id, code);
  }

  private List<ClassificationRequest> mockClassificationRequestsType1() {
    List<ClassificationRequest> mock = new ArrayList<>();
    ClassificationRequest mockClassificationRequest1 = new ClassificationRequest(
          "test",
          "attributeId3",
          "hen xui",
          mockProductItemRequest("test1"),
          null
    );
    ClassificationRequest mockClassificationRequest2 = new ClassificationRequest(
          "test2",
          "attributeId4",
          "hen xui",
          mockProductItemRequest("test2"),
          null
    );
    mock.add(mockClassificationRequest1);
    mock.add(mockClassificationRequest2);
    return mock;

  }


  private List<ClassificationResponse> mockClassificationResponsesType1() {
    List<ClassificationResponse> mock = new ArrayList<>();
    ClassificationResponse mockClassificationResponse1 = new ClassificationResponse(
          "test",
          "attributeId3", "attributeId3",
          "hen xui",
          "test1", "test1",
          false
    );
    ClassificationResponse mockClassificationResponse2 = new ClassificationResponse(
          "test2",
          "attributeId4", "attributeId4",
          "hen xui",
          "test2", "test2",
          false
    );
    mock.add(mockClassificationResponse1);
    mock.add(mockClassificationResponse2);
    return mock;

  }

  private List<ClassificationRequest> mockClassificationRequestsType2() {
    List<ClassificationRequest> mockSubClassification = new ArrayList<>();
    List<ClassificationRequest> mockClassificationRequests = new ArrayList<>();

    ClassificationRequest mockSubClassificationRequest1 = new ClassificationRequest(
          "test1",
          "attributeId3",
          "hen xui",
          mockProductItemRequest("test1"),
          null
    );

    ClassificationRequest mockSubClassificationRequest2 = new ClassificationRequest(
          "test2",
          "attributeId3",
          "hen xui",
          mockProductItemRequest("test2"),
          null
    );

    mockSubClassification.add(mockSubClassificationRequest1);
    mockSubClassification.add(mockSubClassificationRequest2);

    ClassificationRequest mockClassificationRequest = new ClassificationRequest(
          "test3",
          "attributeId",
          "lom dom",
          null,
          mockSubClassification
    );

    mockClassificationRequests.add(mockClassificationRequest);

    return mockClassificationRequests;

  }


  private List<ClassificationResponse> mockClassificationResponsesType2() {
    List<ClassificationResponse> mockSubClassification = new ArrayList<>();
    List<ClassificationResponse> mockClassificationResponses = new ArrayList<>();

    ClassificationResponse mockSubClassificationResponse1 = new ClassificationResponse(
          "test1",
          "attributeId3", "attributeId3",
          "hen xui",
          "test1", "test1",
          false
    );

    ClassificationResponse mockSubClassificationResponse2 = new ClassificationResponse(
          "test2",
          "attributeId3", "attributeId3",
          "hen xui",
          "test2", "test2",
          false
    );

    mockSubClassification.add(mockSubClassificationResponse1);
    mockSubClassification.add(mockSubClassificationResponse2);

    ClassificationResponse mockClassificationResponse = new ClassificationResponse(
          "test3",
          "attributeId", "attributeId",
          "lom dom",
          false,
          null,
          mockSubClassification
    );

    mockClassificationResponses.add(mockClassificationResponse);

    return mockClassificationResponses;

  }

  private List<ProductWarehouseRequest> mockProductWarehouseRequests() {
    ProductWarehouseRequest mockRequest1 = new ProductWarehouseRequest(
          "testWarehouse1",
          "test1",
          1000.0,
          10000.0,
          200,
          20,
          "VND"

    );

    ProductWarehouseRequest mockRequest2 = new ProductWarehouseRequest(
          "testWarehouse2",
          "test2",
          1000.0,
          10000.0,
          200,
          20,
          "USD"

    );

    List<ProductWarehouseRequest> mock = new ArrayList<>();
    mock.add(mockRequest1);
    mock.add(mockRequest2);

    return mock;
  }

  private List<ProductWarehouseResponse> mockProductWarehouseResponse() {
    ProductWarehouseResponse mockResponse1 = ProductWarehouseResponse.of(
          WarehouseResponse.from("testWarehouse1", "kho1", "0374259818", "hn"),
          mockProductItemResponse("test1", "test1"),
          1000.0,
          10000.0,
          200,
          20,
          "VND"

    );

    ProductWarehouseResponse mockResponse2 = ProductWarehouseResponse.of(
          WarehouseResponse.from("testWarehouse2", "kho2", "0374259818", "hn"),
          mockProductItemResponse("test2", "test2"),
          1000.0,
          10000.0,
          200,
          20,
          "VND"

    );


    List<ProductWarehouseResponse> mock = new ArrayList<>();
    mock.add(mockResponse1);
    mock.add(mockResponse2);

    return mock;
  }

  private ProductAdminResponse mockProductAdminResponse1() {
    return new ProductAdminResponse(
          "1",
          "test1",
          "quan bong da",
          mockCategoryResponse(),
          mockProductWarehouseResponse(),
          null, null,
          Active.ACTIVE
    );
  }

  private ProductAdminResponse mockProductAdminResponse2() {
    return new ProductAdminResponse(
          "2",
          "test2",
          "ao bong da",
          mockCategoryResponse(),
          mockProductWarehouseResponse(),
          null, null,
          Active.ACTIVE
    );

  }


  private ProductRequest mockRequest() {
    return new ProductRequest(
          "code", "ao bong da", "cai",
          1, "VN", "mem", "seller", null,
          categoriesId(), mockAttributeProductRequest(), null, mockProductWarehouseRequests()

    );
  }


  private ProductResponse mockResponse() {
    return ProductResponse.of(
          "test", "code", "ao bong da", "cai",

          1, "VN", "mem", "seller", "Active", null,
          mockCategoryResponse(), mockAttributeProductResponse(),
          null, mockProductWarehouseResponse(), null

    );
  }

  private ProductOrderRequest mockProductOrderRequest1() {
    return new ProductOrderRequest(
          "product",
          null,
          100,
          900.0);
  }

  private ProductOrderRequest mockProductOrderRequest2() {
    return new ProductOrderRequest(
          null,
          "productItem",
          100,
          900.0);
  }

  private ProductOrderCreateRequest mockProductOrderCreateRequest() {
    return new ProductOrderCreateRequest(
          List.of(mockProductOrderRequest1(), mockProductOrderRequest2()),
          "warehouse"
    );
  }


  @Test
  void update_WhenInputProduct_Return200AndBody() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setClassifications(mockClassificationRequestsType2());
    ProductResponse mockResponse = mockResponse();
    mockResponse.setClassifications(mockClassificationResponsesType2());

    Mockito.when(messageService.getMessage(UPDATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");
    Mockito.when(productFacadeService.update("1", mockRequest)).thenReturn(mockResponse);

    MvcResult mvcResult = mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsBytes(mockRequest)))
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String responseBody = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
    Assertions.assertEquals(responseBody,
          objectMapper.writeValueAsString(productController.update("1", mockRequest, "en")));


  }

  @Test
  void update_WhenInputProductItemCodeNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(UPDATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");
    Mockito.when(productFacadeService.update("1", mockRequest)).thenThrow(new ProductItemCodeNotFoundException());

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException"))
          .andDo(print());

  }

  @Test
  void update_WhenInputCodeProductInvalid_Return400BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setCode("");

    Mockito.when(messageService.getMessage(UPDATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value(INVALID_CODE))
          .andDo(print());

  }

  @Test
  void update_WhenInputNameProductInvalid_Return400BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setName("");

    Mockito.when(messageService.getMessage(UPDATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value(INVALID_NAME))
          .andDo(print());

  }

  @Test
  void update_WhenInputCategoryNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");
    Mockito.when(productFacadeService.update("1", mockRequest)).thenThrow(new CategoryNotFoundException());

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException"))
          .andDo(print());

  }

  @Test
  void update_WhenInputAttributeNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");
    Mockito.when(productFacadeService.update("1", mockRequest)).thenThrow(new AttributeNotFoundException());

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.attribute.AttributeNotFoundException"))
          .andDo(print());

  }

  @Test
  void update_WhenInputWarehouseNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");
    Mockito.when(productFacadeService.update("1", mockRequest)).thenThrow(new WarehouseNotFoundException());

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException"))
          .andDo(print());

  }

  @Test
  void update_WhenInputProductItemCodeDuplicate_Return409BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");
    Mockito.when(productFacadeService.update("1", mockRequest)).thenThrow(new ProductItemDuplicateCodeException());

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isConflict())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductItemDuplicateCodeException"))
          .andDo(print());

  }

  @Test
  void update_WhenInputProductCodeDuplicate_Return409BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("Update product successfully");
    Mockito.when(productFacadeService.update("1", mockRequest)).thenThrow(new ProductDuplicateCodeException());

    mockMvc.perform(
                put(END_POINT_PATH + "/1")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isConflict())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductDuplicateCodeException"))
          .andDo(print());

  }


  private ProductClientResponse mockProductClientResponse1() {
    return new ProductClientResponse(
          "test",
          "ao bong da",
          100L,
          1000.0);
  }

  private ProductClientResponse mockProductClientResponse2() {
    return new ProductClientResponse(
          "test1",
          "quan bong da",
          100L,
          1000.0);
  }

  private ProductFilterRequest mockProductFilterRequest() {
    return new ProductFilterRequest(
          "ao",
          0,
          1000,
          categoriesId(),
          mockAttributeProductRequest()
    );
  }

  private ProductAdminFilterRequest mockProductAdminFilterRequest() {
    return new ProductAdminFilterRequest(
          "1", "test",
          100, 1000, 1000, 10000, 10,
          categoriesId(),
          warehouseId(),
          mockAttributeProductRequest(),
          com.ncsgroup.inventory.client.enums.Active.ACTIVE
    );
  }


  @Test
  void testValid_WhenInputCodeProductInvalid_Return400BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setCode("");

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value(INVALID_CODE))
          .andDo(print());

  }

  @Test
  void testValid_WhenInputNameProductInvalid_Return400BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setName("");

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value(INVALID_NAME))
          .andDo(print());

  }

  @Test
  void testValid_WhenProductWarehouseNull_Returns400BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setWarehouses(null);

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value(NOT_NULL))
          .andDo(print());

  }


  @Test
  void testCreate_WhenInputCategoryNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenThrow(new CategoryNotFoundException());

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException"))
          .andDo(print());

  }

  @Test
  void testCreate_WhenInputWarehouseNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenThrow(new WarehouseNotFoundException());

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException"))
          .andDo(print());

  }

  @Test
  void testCreate_WhenInputAttributeNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenThrow(new AttributeNotFoundException());

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.attribute.AttributeNotFoundException"))
          .andDo(print());

  }

  @Test
  void testCreate_WhenInputProductItemCodeDuplicate_Return409BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenThrow(new ProductItemDuplicateCodeException());

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isConflict())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductItemDuplicateCodeException"))
          .andDo(print());

  }

  @Test
  void testCreate_WhenInputProductCodeDuplicate_Return409BadRequest() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenThrow(new ProductDuplicateCodeException());

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isConflict())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductDuplicateCodeException"))
          .andDo(print());

  }

  @Test
  void testCreate_WhenInputProductItemCodeNotFound_Return404NotFound() throws Exception {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenThrow(new ProductItemCodeNotFoundException());

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException"))
          .andDo(print());

  }

  @Test
  void testCreate_WhenInputProductType1_Return201AndBody() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setClassifications(mockClassificationRequestsType1());
    ProductResponse mockResponse = mockResponse();
    mockResponse.setClassifications(mockClassificationResponsesType1());

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenReturn(mockResponse);

    MvcResult mvcResult = mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsBytes(mockRequest)))
          .andExpect(status().isCreated())
          .andDo(print())
          .andReturn();

    String responseBody = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
    Assertions.assertEquals(responseBody,
          objectMapper.writeValueAsString(productController.create(mockRequest, "en")));

  }

  @Test
  void testCreate_WhenInputProductType2_Return201AndBody() throws Exception {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setClassifications(mockClassificationRequestsType2());
    ProductResponse mockResponse = mockResponse();
    mockResponse.setClassifications(mockClassificationResponsesType2());

    Mockito.when(messageService.getMessage(CREATE_PRODUCT_SUCCESS, "en"))
          .thenReturn("create product successfully");
    Mockito.when(productFacadeService.createProduct(mockRequest)).thenReturn(mockResponse);

    MvcResult mvcResult = mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsBytes(mockRequest)))
          .andExpect(status().isCreated())
          .andDo(print())
          .andReturn();

    String responseBody = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
    Assertions.assertEquals(responseBody,
          objectMapper.writeValueAsString(productController.create(mockRequest, "en")));


  }

  @Test
  void testDetail_WhenProductIdNotFound_Return404ProductNotFoundException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en"))
          .thenReturn("success");
    Mockito.when(productFacadeService.detailProduct("test")).thenThrow(new ProductNotFoundException());

    mockMvc.perform(
                get(END_POINT_PATH + "/test")
                      .contentType("application/json"))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductNotFoundException"))
          .andDo(print());
  }

  @Test
  void testDetail_WhenDetailSuccess_Return200AndResponseBody() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en"))
          .thenReturn("success");
    Mockito.when(productFacadeService.detailProduct("test")).thenReturn(mockResponse());

    MvcResult result = mockMvc.perform(
                get(END_POINT_PATH + "/test")
                      .contentType("application/json"))
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(response, objectMapper.writeValueAsString(productController.details("test", "en")));
  }

  @Test
  void testListInClientSide_WhenSuccess_Return200AndResponsesBody() throws Exception {
    List<ProductClientResponse> mock = new ArrayList<>();
    mock.add(mockProductClientResponse1());
    mock.add(mockProductClientResponse2());
    PageResponse<ProductClientResponse> mockResponse = new PageResponse<>();
    mockResponse.setContent(mock);
    mockResponse.setAmount(mock.size());

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.when(productFacadeService.listInClientSide(0, 10)).thenReturn(mockResponse);

    MvcResult result = mockMvc.perform(
                get(END_POINT_PATH + "/client")
                      .contentType("application/json")
                      .param("size", String.valueOf(10))
                      .param("page", String.valueOf(0))
          )
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(productController.listInClientSide(0, 10, "en")));
  }


  @Test
  void testFilterInClientSide_WhenSuccess_Return200AndResponseBody() throws Exception {
    ProductFilterRequest mockRequest = mockProductFilterRequest();
    List<ProductClientResponse> mock = new ArrayList<>();
    mock.add(mockProductClientResponse1());
    mock.add(mockProductClientResponse2());
    PageResponse<ProductClientResponse> mockResponse = new PageResponse<>();
    mockResponse.setContent(mock);
    mockResponse.setAmount(mock.size());

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.when(productFacadeService.filterInClientSide(mockRequest, 0, 10)).thenReturn(mockResponse);

    MvcResult result = mockMvc.perform(
                post(END_POINT_PATH + "/filter/client")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest))
                      .param("size", String.valueOf(10))
                      .param("page", String.valueOf(0))
          )
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(
                productController.filterInClientSide(mockRequest, 0, 10, "en"))
    );
  }

  @Test
  void testListInAdminSide_WhenSuccess_Return200AndResponsesBody() throws Exception {
    List<ProductAdminResponse> mock = new ArrayList<>();
    mock.add(mockProductAdminResponse1());
    mock.add(mockProductAdminResponse2());
    PageResponse<ProductAdminResponse> mockResponse = new PageResponse<>();
    mockResponse.setContent(mock);
    mockResponse.setAmount(mock.size());

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.when(productFacadeService.listInAdminSide(0, 10)).thenReturn(mockResponse);

    MvcResult result = mockMvc.perform(
                get(END_POINT_PATH + "/admin")
                      .contentType("application/json")
                      .param("size", String.valueOf(10))
                      .param("page", String.valueOf(0))
          )
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(productController.listInAdminSide(0, 10, "en")));
  }

  @Test
  void testFilterInAdminSide_WhenSuccess_Return200AndResponseBody() throws Exception {
    ProductAdminFilterRequest request = mockProductAdminFilterRequest();
    List<ProductAdminResponse> mock = new ArrayList<>();
    mock.add(mockProductAdminResponse1());
    mock.add(mockProductAdminResponse2());
    PageResponse<ProductAdminResponse> mockResponse = new PageResponse<>();
    mockResponse.setContent(mock);
    mockResponse.setAmount(mock.size());

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.when(productFacadeService.filterInAdminSide(request, 0, 10)).thenReturn(mockResponse);

    MvcResult result = mockMvc.perform(
                post(END_POINT_PATH + "/filter/admin")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(request))
                      .param("size", String.valueOf(10))
                      .param("page", String.valueOf(0)))
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();

    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(productController.filterInAdminSide(request, 0, 10, "en")));

  }

  @Test
  void testDelete_WhenIdProductNotFount_ReturnProductNotFoundException() throws Exception {
    Mockito.doThrow(new ProductNotFoundException()).when(productService).deleteById("test");

    mockMvc.perform(
                delete(END_POINT_PATH + "/{id}", "test")
                      .contentType("application/json"))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductNotFoundException"))
          .andDo(print());


  }

  @Test
  void testDelete_WhenSuccess_ReturnMessageSuccess() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doNothing().when(productService).deleteById("test");

    mockMvc.perform(
                delete(END_POINT_PATH + "/{id}", "test")
                      .contentType("application/json"))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());
  }

  @Test
  void testAddPromotionToProduct_WhenIdProductNotFound_ReturnProductNotFoundException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new ProductNotFoundException())
          .when(productFacadeService).addPromotionToProduct("test", mockProductPromotionRequest());

    mockMvc.perform(
                post(END_POINT_PATH + "/{id}/promotions", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductPromotionRequest())))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.ProductNotFoundException"))
          .andDo(print());

  }

  @Test
  void testAddPromotionToProduct_WhenIdPromotionNotFound_ReturnPromotionNotFoundException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new PromotionNotFoundException())
          .when(productFacadeService).addPromotionToProduct("test", mockProductPromotionRequest());

    mockMvc.perform(
                post(END_POINT_PATH + "/{id}/promotions", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductPromotionRequest())))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.base.PromotionNotFoundException"))
          .andDo(print());

  }

  @Test
  void testAddPromotionToProduct_WhenIdAlreadyExist_ReturnPromotionAlreadyExistException() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new PromotionAlreadyExistException())
          .when(productFacadeService).addPromotionToProduct("test", mockProductPromotionRequest());

    mockMvc.perform(
                post(END_POINT_PATH + "/{id}/promotions", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductPromotionRequest())))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.base.PromotionAlreadyExistException"))
          .andDo(print());

  }


  @Test
  void testAddPromotionToProduct_WhenAddSuccess_ReturnMessage200() throws Exception {
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doNothing()
          .when(productFacadeService).addPromotionToProduct("test", mockProductPromotionRequest());

    mockMvc.perform(
                post(END_POINT_PATH + "/{id}/promotions", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductPromotionRequest())))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());
  }

  @Test
  void testDeductProduct_WhenQuantityProductNotEnough_Return404AndQuantityProductNotEnoughException() throws Exception {
    final String warehouseId = "warehouse";

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doThrow(new QuantityProductNotEnoughException())
          .when(productService)
          .deductProduct(List.of(mockProductOrderRequest1(), mockProductOrderRequest2()), warehouseId);

    mockMvc.perform(
                post(END_POINT_PATH + "/deduct")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductOrderCreateRequest())))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.product.QuantityProductNotEnoughException"))
          .andDo(print());


  }

  @Test
  void testDeductProduct_WhenSuccess_Return200AndMessageSuccess() throws Exception {
    final String warehouseId = "warehouse";

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.doNothing()
          .when(productService)
          .deductProduct(List.of(mockProductOrderRequest1(), mockProductOrderRequest2()), warehouseId);

    mockMvc.perform(
                post(END_POINT_PATH + "/deduct")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockProductOrderCreateRequest())))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());


  }
}


