package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.CategoryCreateRequest;
import com.ncsgroup.inventory.client.dto.CategoryUpdateRequest;
import com.ncsgroup.inventory.server.configuration.InventoryTestConfiguration;
import com.ncsgroup.inventory.server.dto.response.category.CategoryResponse;
import com.ncsgroup.inventory.server.entity.Category;
import com.ncsgroup.inventory.server.exception.category.CategoryAlreadyExistException;
import com.ncsgroup.inventory.server.exception.category.CategoryDuplicateNameException;
import com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException;
import com.ncsgroup.inventory.server.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(CategoryService.class)
@ContextConfiguration(classes = {InventoryTestConfiguration.class})
class CategoryServiceTest {

  @MockBean
  CategoryRepository repository;

  @Autowired
  CategoryService categoryService;

  private CategoryCreateRequest getCategoryRequest() {
    return new CategoryCreateRequest(
          "Shot",
          "1",
          0,
          null
    );
  }


  private CategoryUpdateRequest getCategoryUpdateRequest() {
    return new CategoryUpdateRequest(
          "1",
          "Shot",
          "1",
          0,
          null
    );
  }

  private CategoryUpdateRequest getCategoryUpdateRequest1() {
    return new CategoryUpdateRequest(
          "2",
          "Basic",
          "2",
          0,
          null
    );
  }

  private Category getCategory() {
    return Category.from(
          "Shot",
          "1",
          0
    );
  }

  private Category getCategory1() {
    return Category.from(
          "Basic",
          "2",
          0,
          "1"
    );
  }


  private CategoryResponse getCategoryResponse1() {
    return new CategoryResponse(
          "1",
          "Shot",
          "1",
          0,
          null,
          null
    );
  }

  private CategoryResponse getCategoryResponse2() {
    return new CategoryResponse(
          "2",
          "Basic",
          "2",
          0,
          "1",
          null
    );
  }


  @Test
  void create_WhenInputValid_ReturnAlreadyExistException() {
    CategoryCreateRequest request = getCategoryRequest();
    List<String> list = new ArrayList<>();
    list.add(request.getName());
    Mockito.when(repository.totalCategory(list)).thenReturn(1);

    assertThrows(CategoryAlreadyExistException.class, () -> categoryService.create(request));

  }

  @Test
  void create_WhenInputValid_ReturnCategoryDuplicateNameException() {
    CategoryCreateRequest request = getCategoryRequest();
    List<CategoryCreateRequest> categoryCreateRequests = new ArrayList<>();
    CategoryCreateRequest categoryCreateRequest = new CategoryCreateRequest(
          "Shot",
          "2",
          0,
          null
    );
    categoryCreateRequests.add(categoryCreateRequest);
    request.setCategoryRequests(categoryCreateRequests);


    assertThrows(CategoryDuplicateNameException.class, () -> categoryService.create(request));

  }


  @Test
  void detail_WhenInputValid_ReturnNotFoundException() {
    String id = "1";
    Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

    assertThrows(CategoryNotFoundException.class, () -> categoryService.detail(id));
  }


  @Test
  void detail_WhenInputCategoryNoParentId_Return200() {
    String id = "1";
    Category category = getCategory();
    category.setId(id);

    List<Category> categories = new ArrayList<>();
    Category category1 = getCategory1();
    categories.add(category1);

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));
    Mockito.when(repository.getCategoryByParentId(id)).thenReturn(categories);

    CategoryResponse categoryResponse = categoryService.detail(id);

    assertEquals(category.getId(), categoryResponse.getId());
    assertEquals(category.getName(), categoryResponse.getName());
    assertEquals(category.getParentId(), categoryResponse.getParenId());
    assertEquals(categories.get(0).getId(), categoryResponse.getCategories().get(0).getId());
    assertEquals(categories.get(0).getName(), categoryResponse.getCategories().get(0).getName());
    assertEquals(categories.get(0).getParentId(), categoryResponse.getCategories().get(0).getParenId());
    assertEquals(categories.get(0).getIsActive(), categoryResponse.getCategories().get(0).getIsActive());
  }

  @Test
  void detail_WhenInputCategoryParentIdInValid_Return200() {
    String id = "2";
    Category category = getCategory1();
    category.setId(id);

    Category category1 = getCategory();
    category1.setId("1");

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));
    Mockito.when(repository.getCategory(category.getParentId())).thenReturn(category1);

    CategoryResponse categoryResponse = categoryService.detail(id);

    assertEquals(category.getId(), categoryResponse.getCategories().get(0).getId());
    assertEquals(category.getName(), categoryResponse.getCategories().get(0).getName());
    assertEquals(category.getParentId(), categoryResponse.getCategories().get(0).getParenId());
    assertEquals(category.getIsActive(), categoryResponse.getCategories().get(0).getIsActive());
    assertEquals(category1.getId(), categoryResponse.getId());
    assertEquals(category1.getName(), categoryResponse.getName());
    assertEquals(category1.getParentId(), categoryResponse.getParenId());
    assertEquals(category1.getIsActive(), categoryResponse.getIsActive());
  }


  @Test
  void delete_WhenInputValid_ReturnNotFoundException() {
    String id = "1";
    Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

    assertThrows(CategoryNotFoundException.class, () -> categoryService.deleteById(id));
  }

  @Test
  void delete_WhenIsDeletedTrue_ReturnNotFoundException() {
    String id = "1";
    Category category = getCategory();
    category.setIsDeleted(true);
    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));

    assertThrows(CategoryNotFoundException.class, () -> categoryService.deleteById(id));
  }

  @Test
  void delete_WhenInputValidCategoryParentIdIsNull_Return200() {
    String id = "1";
    Category category = getCategory();
    category.setId(id);

    List<Category> categories = new ArrayList<>();
    Category category1 = getCategory1();
    category1.setId("2");
    categories.add(category1);

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));
    Mockito.when(repository.getCategoryByParentId(id)).thenReturn(categories);

    categoryService.deleteById(id);

    assertFalse(category.getIsDeleted());
    assertFalse(categories.get(0).getIsDeleted());
  }

  @Test
  void delete_WhenInputValidCategoryParentIdIsNotNull_Return200() {
    String id = "1";
    Category category = getCategory1();
    category.setId(id);

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));

    categoryService.deleteById(id);

    assertFalse(category.getIsDeleted());
  }

  @Test
  void update_WhenInputValid_ReturnNotFoundException() {
    String id = "1";
    CategoryUpdateRequest request = getCategoryUpdateRequest();
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(getCategoryUpdateRequest1());
    request.setCategoryRequests(categoryUpdateRequests);

    Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

    assertThrows(CategoryNotFoundException.class, () -> categoryService.update(id, request));
  }

  @Test
  void update_WhenInputValidIdCategoryRequest_ReturnNotFoundException() {
    String id = "1";
    CategoryUpdateRequest request = getCategoryUpdateRequest();
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(getCategoryUpdateRequest1());
    request.setCategoryRequests(categoryUpdateRequests);

    Category category = getCategory();

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));
    Mockito.when(repository.findById(request.getCategoryRequests().get(0).getId())).thenReturn(Optional.empty());

    assertThrows(CategoryNotFoundException.class, () -> categoryService.update(id, request));
  }

  @Test
  void update_WhenInputValidRequestCategoryFullId_Return200() {
    String id = "1";
    CategoryUpdateRequest request = getCategoryUpdateRequest();
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(getCategoryUpdateRequest1());
    request.setCategoryRequests(categoryUpdateRequests);

    List<Category> categories = new ArrayList<>();
    Category category = getCategory();
    Category category1 = getCategory1();
    categories.add(category);
    categories.add(category1);

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));
    Mockito.when(repository.findById(request.getCategoryRequests().get(0).getId())).thenReturn(Optional.of(category1));
    Mockito.when(repository.saveAll(categories)).thenReturn(null);

    CategoryResponse categoryResponse = categoryService.update(id, request);

    assertEquals(category.getId(), categoryResponse.getId());
    assertEquals(category.getName(), categoryResponse.getName());
    assertEquals(category.getImageId(), categoryResponse.getImageId());
    assertEquals(category.getParentId(), categoryResponse.getParenId());
    assertEquals(category.getIsActive(), categoryResponse.getIsActive());
    assertEquals(category1.getId(), categoryResponse.getCategories().get(0).getId());
    assertEquals(category1.getName(), categoryResponse.getCategories().get(0).getName());
    assertEquals(category1.getImageId(), categoryResponse.getCategories().get(0).getImageId());
    assertEquals(category1.getIsActive(), categoryResponse.getCategories().get(0).getIsActive());

  }

  @Test
  void update_WhenInputValidRequestCategoryNotId_Return200() {
    String id = "1";
    CategoryUpdateRequest request = getCategoryUpdateRequest();
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(new CategoryUpdateRequest(
          null,
          "Basic",
          "2",
          0,
          null

    ));
    request.setCategoryRequests(categoryUpdateRequests);

    List<Category> categories = new ArrayList<>();
    Category category = getCategory();
    Category category1 = getCategory1();
    categories.add(category);
    categories.add(category1);

    CategoryResponse categoryResponse = getCategoryResponse1();
    List<CategoryResponse> categoryResponses = new ArrayList<>();
    categoryResponses.add(getCategoryResponse2());
    categoryResponse.setCategories(categoryResponses);

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(category));
    Mockito.when(repository.saveAll(categories)).thenReturn(null);

    CategoryResponse categoryResponse1 = categoryService.update(id, request);

    assertEquals(category.getId(), categoryResponse1.getId());
    assertEquals(category.getName(), categoryResponse1.getName());
    assertEquals(category.getImageId(), categoryResponse1.getImageId());
    assertEquals(category.getParentId(), categoryResponse1.getParenId());
    assertEquals(category.getIsActive(), categoryResponse1.getIsActive());
  }


  @Test
  void listAll_WhenInputValid_Return200() {
    List<Category> categories = new ArrayList<>();
    List<Category> categories1 = new ArrayList<>();

    Category category = getCategory();
    category.setId("1");
    Category category1 = getCategory1();
    category1.setId("2");

    categories.add(category);

    categories1.add(category1);

    Mockito.when(repository.getAllCategoryParent()).thenReturn(categories);
    Mockito.when(repository.getCategoryByParentId("1")).thenReturn(categories1);

    List<CategoryResponse> categoryResponse = categoryService.getAll();

    assertEquals(1, categoryResponse.size());
    assertEquals(categories.get(0).getId(), categoryResponse.get(0).getId());
    assertEquals(categories.get(0).getName(), categoryResponse.get(0).getName());
    assertEquals(categories.get(0).getParentId(), categoryResponse.get(0).getParenId());
    assertEquals(categories.get(0).getImageId(), categoryResponse.get(0).getImageId());
    assertEquals(categories.get(0).getIsActive(), categoryResponse.get(0).getIsActive());
    assertEquals(categories1.get(0).getId(), categoryResponse.get(0).getCategories().get(0).getId());
    assertEquals(categories1.get(0).getName(), categoryResponse.get(0).getCategories().get(0).getName());
    assertEquals(categories1.get(0).getParentId(), categoryResponse.get(0).getCategories().get(0).getParenId());
    assertEquals(categories1.get(0).getIsActive(), categoryResponse.get(0).getCategories().get(0).getIsActive());
  }


}
