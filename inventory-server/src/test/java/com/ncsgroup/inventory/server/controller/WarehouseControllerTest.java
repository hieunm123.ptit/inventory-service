package com.ncsgroup.inventory.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.ncsgroup.inventory.server.dto.response.warehouse.WarehousePageResponse;

import com.ncsgroup.inventory.client.dto.WarehouseRequest;

import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.Warehouse;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseAlreadyExistException;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException;
import com.ncsgroup.inventory.server.service.MessageService;
import com.ncsgroup.inventory.server.service.WarehouseService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageCode.CREATE_WAREHOUSE_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageCode.UPDATE_WAREHOUSE_SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WarehouseController.class)
class WarehouseControllerTest {
  @Autowired
  MockMvc mockMvc;
  @MockBean
  WarehouseService warehouseService;
  @MockBean
  MessageService messageService;
  @Autowired
  ObjectMapper objectMapper;

  @Autowired
  private WarehouseController warehouseController;

  private WarehouseRequest mockWarehouseRequest() {
    WarehouseRequest request = new WarehouseRequest();
    request.setName("Hieu so handsome");
    request.setPhoneNumber("0374259818");
    request.setAddressId("HN");
    return request;
  }


  private Warehouse mockWarehouse(WarehouseRequest request) {

    return Warehouse.from(
          request.getName(),
          request.getPhoneNumber(),
          request.getAddressId()
    );
  }

  @Test
  void testCreate_WhenCreatedWarehouseSuccessfully_Return201AndBody() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockWarehouse = mockWarehouse(mockRequest);
    Mockito.when(warehouseService.create(mockRequest)).
          thenReturn(WarehouseResponse.of(
                mockWarehouse.getCode(),
                mockWarehouse.getName(),
                mockWarehouse.getPhoneNumber(),
                mockWarehouse.getAddressId()
          ));

    Mockito.when(messageService.getMessage(CREATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("create warehouse successfully");

    MvcResult mvcResult = mockMvc.perform(
                post("/api/v1/warehouses")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsBytes(mockRequest)))
          .andDo(print())
          .andReturn();
    int status = mvcResult.getResponse().getStatus();
    Assertions.assertEquals(201, status);
    String responseBody = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
    Assertions.assertEquals(responseBody,
          objectMapper.writeValueAsString(warehouseController.create(mockRequest, "en")));

  }

  @Test
  void testCreate_WhenCreatedWarehouseWhenNameAlreadyExists_ReturnWarehouseAlreadyExist() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();

    Mockito.when(warehouseService.create(mockRequest)).thenThrow(new WarehouseAlreadyExistException());
    Mockito.when(messageService.getMessage(CREATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("create warehouse successfully");

    mockMvc.perform(post("/api/v1/warehouses")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.warehouse.WarehouseAlreadyExistException"))
          .andDo(print());

  }

  @Test
  void testValid_InputPhoneNumberWarehouseInvalid_Return400BadRequest() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    mockRequest.setPhoneNumber("123");

    Mockito.when(messageService.getMessage(CREATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("create warehouse successfully");

    mockMvc.perform(
                post("/api/v1/warehouses")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value("Invalid Phone Number"))
          .andDo(print());

  }

  @Test
  void testValid_InputNameWarehouseInvalid_Return400BadRequest() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    mockRequest.setName("");

    Mockito.when(messageService.getMessage(CREATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("create warehouse successfully");

    mockMvc.perform(post("/api/v1/warehouses")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value("Invalid name"))
          .andDo(print());

  }

  @Test
  void testValid_InputAddressIdWarehouseInvalid_Return400BadRequest() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    mockRequest.setAddressId("");

    Mockito.when(messageService.getMessage(CREATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("create warehouse successfully");

    mockMvc.perform(post("/api/v1/warehouses")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code").value("Invalid Address"))
          .andDo(print());

  }

  @Test
  void testUpdate_WhenUpdateWarehouseSuccess_Return200AndBody() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockWarehouse = mockWarehouse(mockRequest);

    Mockito.when(warehouseService.update("test", mockRequest)).
          thenReturn(WarehouseResponse.of(
                mockWarehouse.getCode(),
                mockWarehouse.getName(),
                mockWarehouse.getPhoneNumber(),
                mockWarehouse.getAddressId())
          );
    Mockito.when(messageService.getMessage(UPDATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("update warehouse successfully");

    MvcResult mvcResult = mockMvc.perform(
                put("/api/v1/warehouses/{id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsBytes(mockRequest)))
          .andDo(print())
          .andReturn();
    int status = mvcResult.getResponse().getStatus();
    Assertions.assertEquals(200, status);
    String responseBody = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);
    Assertions.assertEquals(responseBody,
          objectMapper.writeValueAsString(warehouseController.update("test", mockRequest, "en")));

  }

  @Test
  void testUpdate_WhenIdNotFound_Return404WarehouseNotFoundException() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();

    Mockito.when(warehouseService.update("test", mockRequest)).thenThrow(new WarehouseNotFoundException());
    Mockito.when(messageService.getMessage(UPDATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("update warehouse successfully");

    mockMvc.perform(
                put("/api/v1/warehouses/{id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsBytes(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException"))
          .andDo(print());
  }

  @Test
  void testUpdate_WhenNameRequestAlreadyExist_Return400WarehouseAlreadyExistException() throws Exception {
    WarehouseRequest mockRequest = mockWarehouseRequest();

    Mockito.when(messageService.getMessage(UPDATE_WAREHOUSE_SUCCESS, "en"))
          .thenReturn("update warehouse successfully");
    Mockito.when(warehouseService.update("test", mockRequest)).thenThrow(new WarehouseAlreadyExistException());

    mockMvc.perform(
                put("/api/v1/warehouses/{id}", "test")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")

                .value("com.ncsgroup.inventory.server.exception.warehouse.WarehouseAlreadyExistException"))
          .andDo(print());
  }

  @Test
  void testList_WhenListAllWarehouse_Return200AndBody() throws Exception {
    WarehousePageResponse mock = new WarehousePageResponse();
    List<WarehouseResponse> warehouseResponses = new ArrayList<>();

    warehouseResponses.add(WarehouseResponse.from(
          "test1",
          "DuyHieu",
          "0374259888",
          "HN")
    );

    warehouseResponses.add(WarehouseResponse.from(
          "test2",
          "DuyHieu",
          "0374259889",
          "HN")
    );

    mock.setWarehouseResponses(warehouseResponses);

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("success");
    Mockito.when(warehouseService.list("", 10, 0, true)).thenReturn(mock);

    MvcResult mvcResult = mockMvc.perform(get("/api/v1/warehouses")
                .param("keyword", "")
                .param("size", String.valueOf(10))
                .param("page", String.valueOf(0))
                .param("all", String.valueOf(true)))
          .andExpect(status().isOk())
          .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertEquals(responseBody,
          objectMapper.writeValueAsString(warehouseController.list("", 10, 0, true, "en")));

  }

  @Test
  void testList_WhenListWarehouseByKeyword_Return200AndBody() throws Exception {
    WarehousePageResponse mock = new WarehousePageResponse();
    List<WarehouseResponse> warehouseResponses = new ArrayList<>();

    warehouseResponses.add(WarehouseResponse.from(
          "test1",
          "DuyHieu",
          "0374259888",
          "HN")
    );

    warehouseResponses.add(WarehouseResponse.from(
          "test2",
          "DuyHieu",
          "0374259889",
          "HN")
    );

    mock.setWarehouseResponses(warehouseResponses);
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("success");
    Mockito.when(warehouseService.list("test", 10, 0, false)).thenReturn(mock);

    MvcResult mvcResult = mockMvc.perform(get("/api/v1/warehouses")
                .param("keyword", "test")
                .param("size", String.valueOf(10))
                .param("page", String.valueOf(0))
                .param("all", String.valueOf(false)))
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();
    String responseBody = mvcResult.getResponse().getContentAsString();
    Assertions.assertEquals(responseBody,
          objectMapper.writeValueAsString(warehouseController.list("test", 10, 0, false, "en")));

  }

  @Test
  void testDelete_WhenIdNotFound_Return400WarehouseNotFoundException() throws Exception {
    Mockito.doThrow(new WarehouseNotFoundException())
          .when(warehouseService).deleteById("test");

    mockMvc.perform(
                delete("/api/v1/warehouses/{id}", "test")
                      .contentType("application/json"))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException"))
          .andDo(print());
  }

  @Test
  void testDelete_WhenDeleteWarehouseSuccess_Return200Success() throws Exception {
    Mockito.doNothing().when(warehouseService).deleteById("test");

    mockMvc.perform(
                delete("/api/v1/warehouses/{id}", "test")
                      .contentType("application/json"))
          .andExpect(status().isOk())
          .andDo(print());

  }
}


