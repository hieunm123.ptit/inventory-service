package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.AttributeCreateRequest;
import com.ncsgroup.inventory.server.configuration.InventoryTestConfiguration;
import com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.entity.Attribute;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.exception.attribute.AttributeAlreadyExistException;
import com.ncsgroup.inventory.server.exception.attribute.AttributeNotFoundException;
import com.ncsgroup.inventory.server.repository.AttributeRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@WebMvcTest(AttributeService.class)
@ContextConfiguration(classes = {InventoryTestConfiguration.class})
class AttributeServiceTest {
  @MockBean
  AttributeRepository repository;

  @Autowired
  AttributeService attributeService;

  int size = 2;
  int page = 0;
  Pageable pageable = PageRequest.of(page, size);

  private AttributeCreateRequest getAttributeCreateRequest() {
    return new AttributeCreateRequest(
          "San suat"
    );
  }


  private Attribute getAttribute() {
    return Attribute.from(
          "San suat",
          Active.ACTIVE
    );
  }

  private AttributeResponse getAttributeResponse() {
    return new AttributeResponse(
          "1",
          "Viet Nam",
          Active.ACTIVE
    );
  }

  private AttributeResponse getAttributeResponse1() {
    return new AttributeResponse(
          "2",
          "Trung Quoc",
          Active.ACTIVE
    );
  }

  @Test
  void create_WhenInputValid_Return200OK() {
    AttributeCreateRequest request = getAttributeCreateRequest();
    Attribute attribute = getAttribute();

    Mockito.when(repository.existsByName(attribute.getName())).thenReturn(false);
    Mockito.when(repository.save(attribute)).thenReturn(attribute);

    AttributeResponse response = attributeService.create(request);

    assertEquals(attribute.getName(), response.getName());
    assertEquals(attribute.getIsActive(), response.getIsActive());
  }

  @Test
  void create_WhenInputValid_ReturnAlreadyExists() {
    AttributeCreateRequest request = getAttributeCreateRequest();

    Mockito.when(repository.existsByName(request.getName())).thenReturn(true);

    assertThrows(AttributeAlreadyExistException.class, () -> attributeService.create(request));
  }

  @Test
  void update_WhenInputValid_ReturnNotFound() {
    String id = "1";
    AttributeCreateRequest request = getAttributeCreateRequest();

    Mockito.when(repository.findAttribute(id)).thenReturn(null);

    assertThrows(AttributeNotFoundException.class, () -> attributeService.update(id, request));
  }

  @Test
  void update_WhenInputValid_ReturnAlreadyExists() {
    String id = "1";
    AttributeCreateRequest request = getAttributeCreateRequest();

    Attribute attribute = getAttribute();
    attribute.setId("1");

    Mockito.when(repository.existsByName(request.getName())).thenReturn(true);

    assertThrows(AttributeNotFoundException.class, () -> attributeService.update(id, request));
  }

  @Test
  void update_WhenInputValid_ReturnAttributeResponse() {
    String id = "1";
    AttributeCreateRequest request = getAttributeCreateRequest();

    Attribute attribute = Attribute.from(
          "Trung Quoc",
          Active.ACTIVE
    );
    attribute.setId("1");
    attribute.setName("San suat");

    Mockito.when(repository.findAttribute(id)).thenReturn(attribute);
    Mockito.when(repository.existsByName(request.getName())).thenReturn(false);
    Mockito.when(repository.save(attribute)).thenReturn(attribute);

    AttributeResponse response = attributeService.update(id, request);

    assertEquals(attribute.getName(), response.getName());
    assertEquals(attribute.getIsActive(), response.getIsActive());
  }

  @Test
  void list_WhenIsAllTrue_Return200OK() {
    List<AttributeResponse> attributeResponses = new ArrayList<>();
    attributeResponses.add(getAttributeResponse());
    attributeResponses.add(getAttributeResponse1());

    Mockito.when(repository.getAll()).thenReturn(attributeResponses);

    PageResponse<AttributeResponse> attributePageResponse = attributeService.list(null, 2, 0, true, 1);

    assertEquals(attributeResponses.size(), attributePageResponse.getAmount());
    assertEquals(attributeResponses.get(0).getId(), attributePageResponse.getContent().get(0).getId());
    assertEquals(attributeResponses.get(0).getName(), attributePageResponse.getContent().get(0).getName());
    assertEquals(attributeResponses.get(0).getIsActive(), attributePageResponse.getContent().get(0).getIsActive());
  }

  @Test
  void list_WhenIsAllFalse_Return200OK() {
    List<AttributeResponse> attributeResponses = new ArrayList<>();
    attributeResponses.add(getAttributeResponse());
    attributeResponses.add(getAttributeResponse1());
    Page<AttributeResponse> pageResponse = new PageImpl<>(attributeResponses);

    Mockito.when(repository.search(pageable, null)).thenReturn(pageResponse);

    PageResponse<AttributeResponse> attributePageResponse = attributeService.list(null, 2, 0, false, 1);

    assertEquals(pageResponse.getTotalElements(), attributePageResponse.getAmount());
    assertEquals(pageResponse.getContent().get(0).getId(), attributePageResponse.getContent().get(0).getId());
    assertEquals(pageResponse.getContent().get(0).getName(), attributePageResponse.getContent().get(0).getName());
    assertEquals(pageResponse.getContent().get(0).getIsActive(), attributePageResponse.getContent().get(0).getIsActive());
  }

  @Test
  void list_WhenIsValid_Return200OK() {
    String keyword = "Viet";
    List<AttributeResponse> attributeResponses = new ArrayList<>();
    attributeResponses.add(getAttributeResponse());
    Page<AttributeResponse> pageResponse = new PageImpl<>(attributeResponses);

    Mockito.when(repository.search(pageable, keyword)).thenReturn(pageResponse);

    PageResponse<AttributeResponse> attributePageResponse = attributeService.list(keyword, 2, 0, false, 1);

    assertEquals(pageResponse.getTotalElements(), attributePageResponse.getAmount());
    assertEquals(pageResponse.getContent().get(0).getId(), attributePageResponse.getContent().get(0).getId());
    assertEquals(pageResponse.getContent().get(0).getName(), attributePageResponse.getContent().get(0).getName());
    assertEquals(pageResponse.getContent().get(0).getIsActive(), attributePageResponse.getContent().get(0).getIsActive());
  }

  @Test
  void list_WhenIsValidType_Return200OK() {
    AttributeResponse attributeResponse = new AttributeResponse(
          "2",
          "Trung Quoc",
          Active.ACTIVE
    );
    List<AttributeResponse> attributeResponses = new ArrayList<>();
    attributeResponses.add(getAttributeResponse());
    attributeResponses.add(attributeResponse);

    Page<AttributeResponse> pageResponse = new PageImpl<>(attributeResponses);

    Mockito.when(repository.searchClient(pageable, null)).thenReturn(pageResponse);

    PageResponse<AttributeResponse> attributePageResponse = attributeService.list(null, 2, 0, false, 0);

    assertEquals(pageResponse.getTotalElements(), attributePageResponse.getAmount());
    assertEquals(pageResponse.getContent().get(0).getId(), attributePageResponse.getContent().get(0).getId());
    assertEquals(pageResponse.getContent().get(0).getName(), attributePageResponse.getContent().get(0).getName());
    assertEquals(pageResponse.getContent().get(0).getIsActive(), attributePageResponse.getContent().get(0).getIsActive());
  }

}
