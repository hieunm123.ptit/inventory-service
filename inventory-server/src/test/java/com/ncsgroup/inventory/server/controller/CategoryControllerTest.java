package com.ncsgroup.inventory.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncsgroup.inventory.client.dto.CategoryCreateRequest;
import com.ncsgroup.inventory.client.dto.CategoryUpdateRequest;
import com.ncsgroup.inventory.server.dto.response.category.CategoryResponse;
import com.ncsgroup.inventory.server.exception.category.CategoryAlreadyExistException;
import com.ncsgroup.inventory.server.exception.category.CategoryDuplicateNameException;
import com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException;
import com.ncsgroup.inventory.server.service.CategoryService;
import com.ncsgroup.inventory.server.service.MessageService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@WebMvcTest(CategoryController.class)
class CategoryControllerTest {

  private static final String END_POINT_PATH = "/api/v1/categories";

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper objectMapper;


  @MockBean
  CategoryService categoryService;

  @MockBean
  MessageService messageService;

  private CategoryCreateRequest getCategoryRequest() {
    return new CategoryCreateRequest("Shot", "1", 0, null);
  }

  private CategoryCreateRequest getCategoryRequest1() {
    return new CategoryCreateRequest("Basic", "2", 0, null);
  }

  private CategoryUpdateRequest getCategoryUpdateRequest() {
    return new CategoryUpdateRequest("1", "Shot", "1", 0, null);
  }

  private CategoryUpdateRequest getCategoryUpdateRequest1() {
    return new CategoryUpdateRequest("2", "Basic", "2", 0, null);
  }

  private CategoryResponse getCategoryResponse1() {
    return new CategoryResponse("1", "Shot", "1", 0, null, null);
  }

  private CategoryResponse getCategoryResponse2() {
    return new CategoryResponse("2", "Basic", "2", 0, "1", null);
  }


  @Test
  void create_WhenInputValid_Return201() throws Exception {
    List<CategoryCreateRequest> createRequests = new ArrayList<>();
    createRequests.add(getCategoryRequest1());
    CategoryCreateRequest request = getCategoryRequest();
    request.setCategoryRequests(createRequests);

    List<CategoryResponse> categoryResponses = new ArrayList<>();
    categoryResponses.add(getCategoryResponse2());
    CategoryResponse response = getCategoryResponse1();
    response.setCategories(categoryResponses);

    String successMessage = "Creat category successfully";

    Mockito.when(messageService.getMessage(CREATE_CATEGORY_SUCCESS, "en")).thenReturn(successMessage);
    Mockito.when(categoryService.create(request)).thenReturn(response);

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isCreated())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(response.getId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(response.getName()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.image_id").value(response.getImageId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.is_active").value(response.getIsActive()));

  }

  @Test
  void create_WhenInputValid_ReturnCategoryDuplicateName() throws Exception {
    List<CategoryCreateRequest> createRequests = new ArrayList<>();
    createRequests.add(getCategoryRequest1());
    CategoryCreateRequest request = getCategoryRequest();
    request.setCategoryRequests(createRequests);

    List<CategoryResponse> categoryResponses = new ArrayList<>();
    categoryResponses.add(getCategoryResponse2());
    CategoryResponse response = getCategoryResponse1();
    response.setCategories(categoryResponses);

    Mockito.when(categoryService.create(request)).thenThrow(new CategoryDuplicateNameException());

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isConflict())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Conflict"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("com.ncsgroup.inventory.server.exception.category.CategoryDuplicateNameException"));
  }

  @Test
  void create_WhenInputValid_ReturnCategoryAlreadyExist() throws Exception {
    List<CategoryCreateRequest> createRequests = new ArrayList<>();
    createRequests.add(getCategoryRequest1());
    CategoryCreateRequest request = getCategoryRequest();
    request.setCategoryRequests(createRequests);

    List<CategoryResponse> categoryResponses = new ArrayList<>();
    categoryResponses.add(getCategoryResponse2());
    CategoryResponse response = getCategoryResponse1();
    response.setCategories(categoryResponses);

    Mockito.when(categoryService.create(request)).thenThrow(new CategoryAlreadyExistException());

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isConflict())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Conflict"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("com.ncsgroup.inventory.server.exception.category.CategoryAlreadyExistException"));
  }

  @Test
  void create_WhenInputNameIsNull_Return400BadRequest() throws Exception {
    List<CategoryCreateRequest> createRequests = new ArrayList<>();
    createRequests.add(getCategoryRequest1());
    CategoryCreateRequest request = getCategoryRequest();
    request.setName("");
    request.setCategoryRequests(createRequests);

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Name don't be blank"));
  }

  @Test
  void create_WhenInputNameValidate_Return400BadRequest() throws Exception {
    List<CategoryCreateRequest> createRequests = new ArrayList<>();
    createRequests.add(getCategoryRequest1());
    CategoryCreateRequest request = getCategoryRequest();
    request.setName("@Phone");
    request.setCategoryRequests(createRequests);

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Invalid name"));
  }

  @Test
  void detail_WhenInputValid_Return200() throws Exception {
    String id = "1";

    List<CategoryResponse> categoryResponses = new ArrayList<>();
    categoryResponses.add(getCategoryResponse2());
    CategoryResponse response = getCategoryResponse1();
    response.setCategories(categoryResponses);

    String successMessage = "Detail category successfully";

    Mockito.when(messageService.getMessage(DETAIL_CATEGORY_SUCCESS, "en")).thenReturn(successMessage);
    Mockito.when(categoryService.detail(id)).thenReturn(response);

    mockMvc.perform(get(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(response.getId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(response.getName()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.image_id").value(response.getImageId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.is_active").value(response.getIsActive()));

  }

  @Test
  void detail_WhenInputValid_Return404NotFound() throws Exception {
    String id = "1";

    Mockito.when(categoryService.detail(id)).thenThrow(new CategoryNotFoundException());

    mockMvc.perform(get(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isNotFound())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Not Found"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException"));

  }

  @Test
  void delete_WhenInputValid_Return404NotFound() throws Exception {
    String id = "1";

    Mockito.doThrow(new CategoryNotFoundException()).when(categoryService).deleteById(id);

    mockMvc.perform(delete(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isNotFound())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Not Found"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException"));

  }

  @Test
  void delete_WhenInputValid_Return200OK() throws Exception {
    String id = "1";

    String successMessage = "Delete category successfully";

    Mockito.when(messageService.getMessage(DELETE_CATEGORY_SUCCESS, "en")).thenReturn(successMessage);
    Mockito.doNothing().when(categoryService).deleteById(id);

    mockMvc.perform(delete(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage));

  }

  @Test
  void update_WhenInputValid_Return200OK() throws Exception {
    String id = "1";

    CategoryUpdateRequest categoryUpdateRequest = getCategoryUpdateRequest();
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(getCategoryUpdateRequest1());
    categoryUpdateRequest.setCategoryRequests(categoryUpdateRequests);

    CategoryResponse response = getCategoryResponse1();
    CategoryResponse categoryResponse = getCategoryResponse2();
    List<CategoryResponse> categoryResponses = new ArrayList<>();
    categoryResponses.add(categoryResponse);
    response.setCategories(categoryResponses);

    String successMessage = "Update category successfully";

    Mockito.when(messageService.getMessage(UPDATE_CATEGORY_SUCCESS, "en")).thenReturn(successMessage);
    Mockito.when(categoryService.update(id, categoryUpdateRequest)).thenReturn(response);

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryUpdateRequest))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(response.getId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(response.getName()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.image_id").value(response.getImageId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.is_active").value(response.getIsActive()));

  }

  @Test
  void update_WhenInputValid_Return400NotFound() throws Exception {
    String id = "1";

    CategoryUpdateRequest categoryUpdateRequest = getCategoryUpdateRequest();
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(getCategoryUpdateRequest1());
    categoryUpdateRequest.setCategoryRequests(categoryUpdateRequests);


    Mockito.when(categoryService.update(id, categoryUpdateRequest)).thenThrow(new CategoryNotFoundException());

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryUpdateRequest))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isNotFound())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Not Found"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException"));

  }

  @Test
  void update_WhenInputNameIsEmpty_Return400BadRequest() throws Exception {
    CategoryUpdateRequest categoryUpdateRequest = new CategoryUpdateRequest("1", "", "1", 0, null);
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(getCategoryUpdateRequest1());
    categoryUpdateRequest.setCategoryRequests(categoryUpdateRequests);

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryUpdateRequest))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Name don't be blank"));
  }

  @Test
  void update_WhenInputNameValidate_Return400BadRequest() throws Exception {
    CategoryUpdateRequest categoryUpdateRequest = new CategoryUpdateRequest("1", "@Tag", "1", 0, null);
    List<CategoryUpdateRequest> categoryUpdateRequests = new ArrayList<>();
    categoryUpdateRequests.add(getCategoryUpdateRequest1());
    categoryUpdateRequest.setCategoryRequests(categoryUpdateRequests);

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(categoryUpdateRequest))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Invalid name"));
  }

  @Test
  void listAll_WhenInputValid_Return200OK() throws Exception {
    List<CategoryResponse> categoryResponseList = new ArrayList<>();
    List<CategoryResponse> categoryResponses = new ArrayList<>();
    categoryResponses.add(getCategoryResponse2());
    CategoryResponse response = getCategoryResponse1();
    response.setCategories(categoryResponses);
    categoryResponseList.add(response);

    String successMessage = "Success";

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn(successMessage);
    Mockito.when(categoryService.getAll()).thenReturn(categoryResponseList);

    mockMvc.perform(get(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage));

  }


}
