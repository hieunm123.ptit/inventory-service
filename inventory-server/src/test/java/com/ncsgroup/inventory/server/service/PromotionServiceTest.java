package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.PromotionRequest;
import com.ncsgroup.inventory.client.dto.PromotionSearchRequest;
import com.ncsgroup.inventory.server.configuration.InventoryTestConfiguration;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.Promotion;
import com.ncsgroup.inventory.server.exception.promotion.PromotionNotFoundException;
import com.ncsgroup.inventory.server.repository.PromotionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.inventory.server.utils.DateUtils.convertToDateString;
import static com.ncsgroup.inventory.server.utils.DateUtils.convertToTimestamp;

@WebMvcTest(PromotionService.class)
@ContextConfiguration(classes = InventoryTestConfiguration.class)
class PromotionServiceTest {
  @MockBean
  PromotionRepository repository;

  @Autowired
  PromotionService promotionService;

  private static final String mockId = "id";

  private PromotionRequest mockPromotionRequest() {
    return new PromotionRequest(
          "01/01/2023",
          "02/02/2023",
          0,
          30.0,
          100,
          null,
          30

    );
  }

  private Promotion mockPromotion() {
    return Promotion.from(
          convertToTimestamp(mockPromotionRequest().getStartDay()),
          convertToTimestamp(mockPromotionRequest().getEndDay()),
          mockPromotionRequest().getType(),
          mockPromotionRequest().getDiscountPercent(),
          mockPromotionRequest().getMoneyDeducted(),
          mockPromotionRequest().getDescription(),
          mockPromotionRequest().getCoin()
    );
  }

  private PromotionResponse mockPromotionResponse() {
    return PromotionResponse.from(
          "PR_1",
          "01/01/2023",
          "02/02/2023",
          0,
          30.0,
          100,
          null,
          30,
          Active.INACTIVE
    );
  }

  private PromotionSearchRequest promotionSearchRequest() {
    return new PromotionSearchRequest(
          "PR_1",
          "01/01/2023",
          "02/02/2023",
          "22/12/2023",
          "23/12/2023",
          0,
          30.0,
          100,
          1
    );
  }

  @Test
  void testCreate_WhenCreateSuccess_ReturnPromotionResponse() {
    PromotionRequest mockRequest = mockPromotionRequest();
    Promotion mockEntity = mockPromotion();


    Mockito.when(repository.saveAndFlush(mockEntity)).thenReturn(mockEntity);
    Mockito.when(repository.getResponseById(mockEntity.getId())).thenReturn(mockPromotionResponse());


    PromotionResponse response = promotionService.create(mockRequest);
    Assertions.assertEquals(convertToDateString(mockEntity.getStartDay()), response.getStartDay());
    Assertions.assertEquals(convertToDateString(mockEntity.getEndDay()), response.getEndDay());
    Assertions.assertEquals(mockEntity.getDiscountPercent(), response.getDiscountPercent());
    Assertions.assertEquals(mockEntity.getMoneyDeducted(), response.getMoneyDeducted());
    Assertions.assertEquals(mockEntity.getCoin(), response.getCoin());
    Assertions.assertEquals(mockEntity.getIsActive(), response.getIsActive());
  }


  @Test
  void testUpdate_WhenIdPromotionNotFound_ReturnPromotionNotFoundException() {
    Mockito.when(repository.find(mockId)).thenReturn(null);

    PromotionRequest mockRequest = mockPromotionRequest();

    Assertions.assertThrows(
          PromotionNotFoundException.class,
          () -> promotionService.update(mockId, mockRequest));
  }



  @Test
  void testDelete_WhenIdPromotionNotFound_ReturnPromotionNotFoundException() {
    Mockito.when(repository.existsById(mockId)).thenReturn(false);

    Assertions.assertThrows(
          PromotionNotFoundException.class,
          () -> promotionService.deleteById(mockId));
  }


  @Test
  void testUpdate_WhenUpdateSuccess_ReturnPromotionResponse() {
    String id = "id";
    PromotionRequest mockRequest = mockPromotionRequest();
    Promotion mockEntity = mockPromotion();

    Mockito.when(repository.find(id)).thenReturn(mockEntity);
    Mockito.when(repository.save(mockEntity)).thenReturn(mockEntity);

    PromotionResponse response = promotionService.update(id, mockRequest);
    Assertions.assertEquals(mockRequest.getStartDay(), response.getStartDay());
    Assertions.assertEquals(mockRequest.getEndDay(), response.getEndDay());
    Assertions.assertEquals(mockRequest.getDiscountPercent(), response.getDiscountPercent());
    Assertions.assertEquals(mockRequest.getMoneyDeducted(), response.getMoneyDeducted());
    Assertions.assertEquals(mockRequest.getCoin(), response.getCoin());

  }

  @Test
  void testChangeUpdate_WhenIdNotFound_ReturnPromotionNotFoundException() {
    Mockito.when(repository.existsById(mockId)).thenReturn(false);

    Assertions.assertThrows(
          PromotionNotFoundException.class,
          () -> promotionService.changeActive(mockId));
  }

  @Test
  void testList_WhenIsAllTrue_ReturnPageResponse() {
    List<PromotionResponse> mockResponse = new ArrayList<>();
    mockResponse.add(mockPromotionResponse());
    mockResponse.add(mockPromotionResponse());

    Mockito.when(repository.getAll(
          promotionSearchRequest().getCode(),
          convertToTimestamp(promotionSearchRequest().getFromStartDate()),
          convertToTimestamp(promotionSearchRequest().getToStartDate()),
          convertToTimestamp(promotionSearchRequest().getFromEndDate()),
          convertToTimestamp(promotionSearchRequest().getToEndDate()),
          promotionSearchRequest().getType(),
          promotionSearchRequest().getDiscountPercent(),
          promotionSearchRequest().getMoneyDeducted(),
          Active.valueOf(promotionSearchRequest().getIsActive())
    )).thenReturn(mockResponse);

    PageResponse<PromotionResponse> pageResponse = promotionService.list(
          promotionSearchRequest(), 0, 10, true);
    Assertions.assertEquals(mockResponse.size(), pageResponse.getAmount());
    Assertions.assertEquals(mockResponse.size(), pageResponse.getContent().size());
  }

  @Test
  void testList_WhenIsAllFalse_ReturnPageResponse() {
    List<PromotionResponse> mockResponse = new ArrayList<>();
    mockResponse.add(mockPromotionResponse());
    mockResponse.add(mockPromotionResponse());
    Page<PromotionResponse> mockPage = new PageImpl<>(mockResponse);

    Mockito.when(repository.filter(
          promotionSearchRequest().getCode(),
          convertToTimestamp(promotionSearchRequest().getFromStartDate()),
          convertToTimestamp(promotionSearchRequest().getToStartDate()),
          convertToTimestamp(promotionSearchRequest().getFromEndDate()),
          convertToTimestamp(promotionSearchRequest().getToEndDate()),
          promotionSearchRequest().getType(),
          promotionSearchRequest().getDiscountPercent(),
          promotionSearchRequest().getMoneyDeducted(),
          Active.valueOf(promotionSearchRequest().getIsActive()),
          PageRequest.of(0, 10))).thenReturn(mockPage);

    PageResponse<PromotionResponse> pageResponse = promotionService.list(
          promotionSearchRequest(), 0, 10, false);
    Assertions.assertEquals(mockPage.getTotalElements(), pageResponse.getAmount());
    Assertions.assertEquals(mockPage.getContent().size(), pageResponse.getContent().size());

  }

}
