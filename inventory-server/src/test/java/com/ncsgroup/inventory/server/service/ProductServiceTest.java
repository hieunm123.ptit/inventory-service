package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.product.*;
import com.ncsgroup.inventory.server.configuration.InventoryTestConfiguration;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.dto.response.product.*;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.product.Product;
import com.ncsgroup.inventory.server.exception.product.ProductAlreadyExistException;
import com.ncsgroup.inventory.server.exception.product.ProductDuplicateCodeException;
import com.ncsgroup.inventory.server.exception.product.ProductNotFoundException;
import com.ncsgroup.inventory.server.exception.product.QuantityProductNotEnoughException;
import com.ncsgroup.inventory.server.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;


@WebMvcTest(ProductService.class)
@ContextConfiguration(classes = {InventoryTestConfiguration.class})
class ProductServiceTest {
  @MockBean
  ProductRepository repository;

  @Autowired
  ProductService productService;


  private List<String> categoriesId() {
    List<String> categoryIds = new ArrayList<>();
    categoryIds.add("CategoryId1");
    categoryIds.add("CategoryId2");
    return categoryIds;
  }

  private List<CategoryProductResponse> mockCategoryResponse() {
    List<CategoryProductResponse> mock = new ArrayList<>();
    mock.add(new CategoryProductResponse("CategoryId1", "test1"));
    mock.add(new CategoryProductResponse("CategoryId2", "test2"));
    return mock;
  }

  private List<String> warehouseId() {
    List<String> warehouse = new ArrayList<>();
    warehouse.add("w1");
    warehouse.add("w2");
    return warehouse;
  }

  private List<AttributeProductRequest> mockAttributeProductRequest() {

    AttributeProductRequest request1 = new AttributeProductRequest("attributeId1", "value1");
    AttributeProductRequest request2 = new AttributeProductRequest("attributeId2", "value2");

    List<AttributeProductRequest> attributeProductRequests = new ArrayList<>();
    attributeProductRequests.add(request1);
    attributeProductRequests.add(request2);

    return attributeProductRequests;
  }

  private List<String> attributeId() {
    List<String> attributeIds = new ArrayList<>();
    attributeIds.add("attributeId1");
    attributeIds.add("attributeId2");

    return attributeIds;
  }

  private List<String> values() {
    List<String> values = new ArrayList<>();
    values.add("value1");
    values.add("value2");

    return values;
  }

  private ProductItemResponse mockProductItemResponse(String id, String code) {
    return ProductItemResponse.of(id, code);
  }

  private List<ProductWarehouseResponse> mockProductWarehouseResponse() {
    ProductWarehouseResponse mockResponse1 = ProductWarehouseResponse.of(WarehouseResponse.from("testWarehouse1", "kho1", "0374259818", "hn"), mockProductItemResponse("test1", "test1"), 1000.0, 10000.0, 200, 20, "VND"

    );

    ProductWarehouseResponse mockResponse2 = ProductWarehouseResponse.of(WarehouseResponse.from("testWarehouse2", "kho2", "0374259818", "hn"), mockProductItemResponse("test2", "test2"), 1000.0, 10000.0, 200, 20, "VND"

    );


    List<ProductWarehouseResponse> mock = new ArrayList<>();
    mock.add(mockResponse1);
    mock.add(mockResponse2);

    return mock;
  }


  private ProductRequest mockRequest() {
    return new ProductRequest(
          "code",
          "ao bong da",
          "cai",
          1,
          "VN",
          "mem",
          "seller",
          null,
          null,
          null,
          null,
          null

    );
  }


  private Product mockEntity() {
    return Product.from(
          "code",
          "ao bong da",
          "cai",
          1,
          "VN",
          "mem",
          "seller"
    );
  }

  private ProductClientResponse mockProductClientResponse1() {
    return new ProductClientResponse(
          "test",
          "ao bong da",
          100L,
          1000.0
    );
  }

  private ProductClientResponse mockProductClientResponse2() {
    return new ProductClientResponse("test1",
          "quan bong da",
          100L,
          1000.0);
  }

  private ProductAdminResponse mockProductAdminResponse1() {
    return new ProductAdminResponse("1",
          "test1",
          "quan bong da",
          mockCategoryResponse(),
          mockProductWarehouseResponse(),
          null, null,
          Active.ACTIVE
    );
  }

  private ProductAdminResponse mockProductAdminResponse2() {
    return new ProductAdminResponse(
          "2",
          "test2",
          "ao bong da",
          mockCategoryResponse(),
          mockProductWarehouseResponse(),
          null, null,
          Active.ACTIVE
    );

  }


  private ProductFilterRequest mockProductFilterRequest1() {
    return new ProductFilterRequest("ao", 0, 1000, categoriesId(), mockAttributeProductRequest());
  }


  private ProductAdminFilterRequest mockProductAdminFilterRequest() {
    return new ProductAdminFilterRequest("1", "test", 100, 1000, 1000, 10000, 10, categoriesId(), warehouseId(), mockAttributeProductRequest(), com.ncsgroup.inventory.client.enums.Active.ACTIVE);
  }

  private ProductOrderRequest mockProductOrderRequest1() {
    return new ProductOrderRequest(
          "product",
          null,
          100,
          900.0);
  }

  private ProductOrderRequest mockProductOrderRequest2() {
    return new ProductOrderRequest(
          null,
          "productItem",
          100,
          900.0);
  }


  @Test
  void testCreate_WhenCodeProductAlreadyExists_ReturnsProDuplicateCodeException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(repository.existsByCode(mockRequest.getCode())).thenReturn(true);

    Assertions.assertThrows(ProductDuplicateCodeException.class, () -> productService.create(mockRequest));
  }

  @Test
  void testCreate_WhenSuccessAndResponseBody_ReturnProductResponse() {
    ProductRequest mockRequest = mockRequest();
    Product mockEntity = mockEntity();

    Mockito.when(repository.existsByCode(mockEntity.getCode())).thenReturn(false);
    Mockito.when(repository.saveAndFlush(mockEntity)).thenReturn(mockEntity);

    ProductResponse response = productService.create(mockRequest);
    Assertions.assertEquals(mockEntity.getCode(), response.getCode());
    Assertions.assertEquals(mockEntity.getName(), response.getName());
    Assertions.assertEquals(mockEntity.getUnit(), response.getUnit());
    Assertions.assertEquals(mockEntity.getType(), response.getType());
    Assertions.assertEquals(mockEntity.getOrigin(), response.getOrigin());
    Assertions.assertEquals(mockEntity.getDescription(), response.getDescription());
    Assertions.assertEquals(mockEntity.getSellerId(), response.getSellerId());
  }

  @Test
  void testDetails_WhenIdProductNotFound_ReturnProductNotFoundException() {
    String id = "test";

    Mockito.when(repository.existsById(id)).thenReturn(false);

    Assertions.assertThrows(ProductNotFoundException.class, () -> productService.detail(id));
  }

  @Test
  void testDetails_WhenProductExists_ReturnProductResponse() {
    String id = "test";

    Mockito.when(repository.existsById(id)).thenReturn(true);
    Product mockEntity = mockEntity();
    Mockito.when(repository.find(id)).thenReturn(mockEntity);

    ProductResponse response = productService.detail(id);

    Assertions.assertEquals(mockEntity.getCode(), response.getCode());
    Assertions.assertEquals(mockEntity.getName(), response.getName());
    Assertions.assertEquals(mockEntity.getUnit(), response.getUnit());
    Assertions.assertEquals(mockEntity.getType(), response.getType());
    Assertions.assertEquals(mockEntity.getOrigin(), response.getOrigin());
    Assertions.assertEquals(mockEntity.getDescription(), response.getDescription());
    Assertions.assertEquals(mockEntity.getSellerId(), response.getSellerId());

  }

  @Test
  void update_WhenInputValid_ReturnNotFoundException() {
    String id = "1";
    ProductRequest request = mockRequest();

    Mockito.when(repository.findProductById(id)).thenReturn(null);

    assertThrows(ProductNotFoundException.class, () -> productService.update(id, request));
  }

  @Test
  void update_WhenInputValid_ReturnAlreadyExistException() {
    String id = "1";
    ProductRequest request = mockRequest();

    Product product = Product.from(
          "code1", "ao bong da", "cai",
          1, "VN", "mem", "seller"
    );

    product.setId(id);

    Mockito.when(repository.findProductById(id)).thenReturn(product);
    Mockito.when(repository.existsByCode("code")).thenReturn(true);

    assertThrows(ProductAlreadyExistException.class, () -> productService.update(id, request));
  }

  @Test
  void update_WhenInputValid_Return200() {
    String id = "1";
    ProductRequest request = mockRequest();

    Product product = mockEntity();
    product.setId(id);

    Mockito.when(repository.findProductById(id)).thenReturn(product);
    Mockito.when(repository.save(product)).thenReturn(null);

    ProductResponse response = productService.update(id, request);

    Assertions.assertEquals(product.getCode(), response.getCode());
    Assertions.assertEquals(product.getName(), response.getName());
    Assertions.assertEquals(product.getUnit(), response.getUnit());
    Assertions.assertEquals(product.getType(), response.getType());
    Assertions.assertEquals(product.getOrigin(), response.getOrigin());
    Assertions.assertEquals(product.getDescription(), response.getDescription());
    Assertions.assertEquals(product.getSellerId(), response.getSellerId());
  }

  @Test
  void testListInClientSide_WhenSuccess_ReturnResponse() {

    List<ProductClientResponse> mock = new ArrayList<>();
    mock.add(mockProductClientResponse1());
    mock.add(mockProductClientResponse2());
    Page<ProductClientResponse> mockPage = new PageImpl<>(mock);

    Mockito.when(repository.listInClientSide(PageRequest.of(0, 10))).thenReturn(mockPage);

    PageResponse<ProductClientResponse> mockResponse = productService.listInClientSide(0, 10);

    Assertions.assertEquals(mockPage.getContent().size(), mockResponse.getContent().size());
    Assertions.assertEquals(mockPage.getTotalElements(), mockResponse.getAmount());

  }

  @Test
  void testFilterInClientSide_WhenSuccess_ReturnsResponse() {

    ProductFilterRequest mockRequest = mockProductFilterRequest1();
    List<ProductClientResponse> mock = new ArrayList<>();
    mock.add(mockProductClientResponse1());
    mock.add(mockProductClientResponse2());
    Page<ProductClientResponse> mockPage = new PageImpl<>(mock);


    Mockito.when(repository.listFilterClient(
          mockRequest.getProductName(),
          mockRequest.getMinPrice(), mockRequest.getMaxPrice(),
          categoriesId(),
          attributeId(), values(),
          PageRequest.of(0, 10))).thenReturn(mockPage);

    PageResponse<ProductClientResponse> pageResponse = productService.filterInClientSide(
          mockProductFilterRequest1(), 0, 10);

    Assertions.assertEquals(mockPage.getContent().size(), pageResponse.getContent().size());
    Assertions.assertEquals(mockPage.getTotalElements(), pageResponse.getAmount());
  }

  @Test
  void testListInAdminSide_WhenSuccess_ReturnPageResponseBody() {
    List<ProductAdminResponse> mock = new ArrayList<>();
    mock.add(mockProductAdminResponse1());
    mock.add(mockProductAdminResponse2());
    Page<ProductAdminResponse> mockPage = new PageImpl<>(mock);

    Mockito.when(repository.listInAdminSide(PageRequest.of(0, 10))).thenReturn(mockPage);
    PageResponse<ProductAdminResponse> pageResponse = productService.listInAdminSide(0, 10);

    Assertions.assertEquals(mockPage.getContent().size(), pageResponse.getContent().size());
    Assertions.assertEquals(mockPage.getTotalElements(), pageResponse.getAmount());
  }

  @Test
  void testFilterInClientSide_WhenSuccess_ReturnPageResponseBody() {
    ProductAdminFilterRequest request = mockProductAdminFilterRequest();
    List<ProductAdminResponse> mock = new ArrayList<>();
    mock.add(mockProductAdminResponse1());
    mock.add(mockProductAdminResponse2());
    Page<ProductAdminResponse> mockPage = new PageImpl<>(mock);

    Mockito.when(repository.filterInAdminSide(request.getCode(), request.getProductName(), request.getMinPrice(), request.getMaxPrice(), request.getQuantityRemaining(), request.getQuantitySold(), request.getRange(), request.getCategories(), attributeId(), values(), request.getWarehouses(), request.getActive(), PageRequest.of(0, 10))).thenReturn(mockPage);

    PageResponse<ProductAdminResponse> mockResponse = productService.filterInAdminSide(request, 0, 10);

    Assertions.assertEquals(mockPage.getContent().size(), mockResponse.getContent().size());
    Assertions.assertEquals(mockPage.getTotalElements(), mockResponse.getAmount());


  }

  @Test
  void testDelete_WhenIdProductNotFound_ReturnProductNotFoundException() {
    String mockId = "test";

    Mockito.when(repository.existsById(mockId)).thenReturn(false);

    Assertions.assertThrows(ProductNotFoundException.class, () -> productService.deleteById(mockId));

  }

  @Test
  void updateActive_WhenInputValid_ReturnNotFoundException() {
    String id = "1";

    Mockito.when(repository.findProductById(id)).thenReturn(null);

    assertThrows(ProductNotFoundException.class, () -> productService.updateActive(id));
  }

  @Test
  void updateActive_WhenInputValid_Return200() {
    String id = "1";

    Product product = mockEntity();
    product.setId(id);

    Mockito.when(repository.findProductById(id)).thenReturn(product);
    Mockito.when(repository.save(product)).thenReturn(null);

    ProductResponse response = productService.updateActive(id);

    Assertions.assertEquals(product.getCode(), response.getCode());
    Assertions.assertEquals(product.getName(), response.getName());
    Assertions.assertEquals(product.getUnit(), response.getUnit());
    Assertions.assertEquals(product.getType(), response.getType());
    Assertions.assertEquals(product.getOrigin(), response.getOrigin());
    Assertions.assertEquals(product.getDescription(), response.getDescription());
    Assertions.assertEquals(product.getSellerId(), response.getSellerId());
  }

  @Test
  void testDeductProduct_WhenProductItemIdNullAndQuantityNotEnough_ReturnQuantityProductNotFoundException() {
    final String warehouseId = "1";
    List<ProductOrderRequest> mockRequest = List.of(mockProductOrderRequest1());

    Mockito.when(
                repository.checkQuantityProduct(
                      mockProductOrderRequest1().getProductId(),
                      mockProductOrderRequest1().getQuantity(),
                      warehouseId))
          .thenReturn(false);

    Assertions.assertThrows(
          QuantityProductNotEnoughException.class,
          () -> productService.deductProduct(mockRequest, warehouseId));
  }

  @Test
  void testDeductProduct_WhenProductIdNullAndQuantityNotEnough_ReturnQuantityProductNotFoundException() {
    final String warehouseId = "1";
    List<ProductOrderRequest> mockRequest = List.of(mockProductOrderRequest2());

    Mockito.when(
                repository.checkQuantityProductItem(
                      mockProductOrderRequest2().getProductId(),
                      mockProductOrderRequest2().getQuantity(),
                      warehouseId))
          .thenReturn(false);

    Assertions.assertThrows(
          QuantityProductNotEnoughException.class,
          () -> productService.deductProduct(mockRequest, warehouseId));
  }


}
