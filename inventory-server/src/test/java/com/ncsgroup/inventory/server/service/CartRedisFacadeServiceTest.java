package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.CartItemRequest;
import com.ncsgroup.inventory.client.dto.product.ProductCartDeletionRequest;
import com.ncsgroup.inventory.client.dto.product.ProductCartRequest;
import com.ncsgroup.inventory.server.configuration.InventoryTestConfiguration;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.exception.cart_item.CartDeletionInvalidException;
import com.ncsgroup.inventory.server.exception.cart_item.CartItemInvalidException;
import com.ncsgroup.inventory.server.exception.cart_item.FieldKeyNotFoundException;
import com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException;
import com.ncsgroup.inventory.server.exception.product.ProductNotFoundException;
import com.ncsgroup.inventory.server.facade.CartRedisFacadeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ncsgroup.inventory.server.constanst.Constants.RedisConstants.CART_PRODUCT_ITEM_FIELD_PREFIX;

@WebMvcTest(CartRedisFacadeService.class)
@ContextConfiguration(classes = InventoryTestConfiguration.class)
class CartRedisFacadeServiceTest {
  @MockBean
  CartRedisService cartRedisService;

  @MockBean
  ProductService productService;

  @MockBean
  ProductItemService productItemService;

  @Autowired
  CartRedisFacadeService cartRedisFacadeService;

  private CartItemRequest mockCartItemRequest() {
    return new CartItemRequest(
          "productId",
          "productItemId",
          3
    );
  }

  private ProductCartRequest mockProductCartRequest() {
    List<CartItemRequest> mock = new ArrayList<>();
    mock.add(mockCartItemRequest());
    mock.add(mockCartItemRequest());

    return new ProductCartRequest(mock);
  }

  private ProductCartRequest mockProductCartRequest1() {
    List<CartItemRequest> mock = new ArrayList<>();
    mock.add(new CartItemRequest());

    return new ProductCartRequest(mock);
  }


  private ProductCartDeletionRequest mockProductCartDeletionRequest() {

    return new ProductCartDeletionRequest(
          "productIds", null
    );
  }


  private ProductCartDeletionRequest mockProductCartDeletionRequest2() {

    return new ProductCartDeletionRequest();
  }

  private CartItemResponse mockCartItemResponse() {
    return new CartItemResponse(
          "product",
          "productItem",
          "ao ret",
          10,
          1000.0,
          "xanh",
          "id",
          "xL",
          "id1"
    );
  }

  private Map<String, Integer> mockProduct() {
    Map<String, Integer> map = new HashMap<>();
    map.put(CART_PRODUCT_ITEM_FIELD_PREFIX + "productItem", 10);

    return map;
  }

  private List<CartItemResponse> mockListCartItemResponse() {
    List<CartItemResponse> list = new ArrayList<>();
    list.add(mockCartItemResponse());

    return list;
  }


  @Test
  void testAddProductToCart_WhenProductIdNotFound_ReturnException() {
    String mockId = "productId";

    Mockito.doThrow(new ProductNotFoundException())
          .when(productService).checkProductExist(List.of(mockId));
    Mockito.doNothing()
          .when(cartRedisService).addProductToCart("test", mockProductCartRequest().getItems());

    ProductCartRequest mockRequest = mockProductCartRequest();

    Assertions.assertThrows(
          ProductNotFoundException.class,
          () -> cartRedisFacadeService.addProductToCart("test", mockRequest));
  }


  @Test
  void testAddProductToCart_WhenProductItemIdNotFound_ReturnException() {
    String mockId = "productItemId";

    Mockito.doThrow(new ProductItemCodeNotFoundException())
          .when(productItemService).checkProductItemExisted(List.of(mockId));
    Mockito.doNothing()
          .when(cartRedisService).addProductToCart("test", mockProductCartRequest().getItems());

    ProductCartRequest mockRequest = mockProductCartRequest();

    Assertions.assertThrows(
          ProductItemCodeNotFoundException.class,
          () -> cartRedisFacadeService.addProductToCart("test", mockRequest));

  }

  @Test
  void testAddProductToCart_WhenCartItemInvalid_ReturnException() {
    ProductCartRequest mockRequest = mockProductCartRequest1();

    Assertions.assertThrows(
          CartItemInvalidException.class,
          () -> cartRedisFacadeService.addProductToCart("test", mockRequest));

  }

  @Test
  void testDeleteProductInCart_WhenFieldKeyNotFound_ReturnException() {
    Mockito.doThrow(new FieldKeyNotFoundException()).when(cartRedisService)
          .deleteProductInCart("test", mockProductCartDeletionRequest());

    ProductCartDeletionRequest productCartDeletionRequest = mockProductCartDeletionRequest();

    Assertions.assertThrows(
          FieldKeyNotFoundException.class,
          () -> cartRedisFacadeService.deleteProductInCart("test", productCartDeletionRequest)
    );
  }

  @Test
  void testDeleteProductInCart_WhenCartDeletionInvalid_ReturnException() {

    ProductCartDeletionRequest productCartDeletionRequest = mockProductCartDeletionRequest2();

    Assertions.assertThrows(
          CartDeletionInvalidException.class,
          () -> cartRedisFacadeService.deleteProductInCart("test", productCartDeletionRequest));

  }

  @Test
  void testUpdateProductInCart_WhenProductIdNotFound_ReturnException() {
    Mockito.doThrow(new ProductNotFoundException())
          .when(productService).checkProductExist(Mockito.anyList());

    Mockito.doNothing().when(cartRedisService)
          .updateProductInCart("test", mockProductCartRequest().getItems());

    ProductCartRequest mockRequest = mockProductCartRequest();

    Assertions.assertThrows(
          ProductNotFoundException.class,
          () -> cartRedisFacadeService.updateProductInCart("test", mockRequest));

  }

  @Test
  void testUpdateProductInCart_WhenProductItemIdNotFound_ReturnException() {
    Mockito.doThrow(new ProductItemCodeNotFoundException())
          .when(productItemService).checkProductItemExisted(Mockito.anyList());

    Mockito.doNothing().when(cartRedisService)
          .updateProductInCart("test", mockProductCartRequest().getItems());

    ProductCartRequest mockRequest = mockProductCartRequest();

    Assertions.assertThrows(
          ProductItemCodeNotFoundException.class,
          () -> cartRedisFacadeService.updateProductInCart("test", mockRequest));

  }

  @Test
  void testGetProductFromCart_WhenSuccess_ReturnListItemResponse() {
    Mockito.when(cartRedisService.getProductFromCart("test")).thenReturn(mockProduct());
    Mockito.when(productItemService.getItems(List.of("productItem"))).thenReturn(mockListCartItemResponse());

    List<CartItemResponse> itemResponses = cartRedisFacadeService.getProductFromCart("test");


    Assertions.assertEquals(
          mockProduct().get(CART_PRODUCT_ITEM_FIELD_PREFIX + "productItem"),
          itemResponses.get(0).getAmount()
    );
    Assertions.assertEquals(mockCartItemResponse().getProductId(), itemResponses.get(0).getProductId());
    Assertions.assertEquals(mockCartItemResponse().getProductItemId(), itemResponses.get(0).getProductItemId());
    Assertions.assertEquals(mockCartItemResponse().getClassificationId(), itemResponses.get(0).getClassificationId());
    Assertions.assertEquals(mockCartItemResponse().getName(), itemResponses.get(0).getName());

    Assertions.assertEquals(
          mockCartItemResponse().getSubClassificationId(),
          itemResponses.get(0).getSubClassificationId()
    );
    Assertions.assertEquals(
          mockCartItemResponse().getSubClassificationName(),
          itemResponses.get(0).getSubClassificationName()
    );


  }

}
