package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.WarehouseRequest;
import com.ncsgroup.inventory.server.configuration.InventoryTestConfiguration;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehousePageResponse;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.Warehouse;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseAlreadyExistException;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException;
import com.ncsgroup.inventory.server.repository.WarehouseRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(WarehouseService.class)
@ContextConfiguration(classes = InventoryTestConfiguration.class)
class WarehouseServiceTest {
  @MockBean
  WarehouseRepository repository;
  @Autowired
  WarehouseService warehouseService;


  private WarehouseRequest mockWarehouseRequest() {
    WarehouseRequest request = new WarehouseRequest();
    request.setName("Hieu so handsome");
    request.setPhoneNumber("0374259818");
    request.setAddressId("HN");
    return request;
  }

  private Warehouse mockWarehouse(WarehouseRequest request) {

    return Warehouse.from(request.getName(), request.getPhoneNumber(), request.getAddressId());
  }

  private WarehouseResponse mockWarehouseResponse1() {
    return WarehouseResponse.of(
          "WH_1",
          "kho 1",
          "0374259888",
          "hn"
    );
  }

  private WarehouseResponse mockWarehouseResponse2() {
    return WarehouseResponse.of(
          "WH_2",
          "kho 2",
          "0374259889",
          "hn"
    );
  }

  private static final String mockId = "DuyHieuBuuDien";

  @Test
  void testCreate_WhenNameWarehouseAlreadyExists_ReturnWarehouseAlreadyExistException() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockEntity = mockWarehouse(mockRequest);

    Mockito.when(repository.existsByName(mockRequest.getName())).thenReturn(true);
    Mockito.when(repository.getByName(mockRequest.getName())).thenReturn(mockEntity);

    Assertions.assertThatThrownBy(() -> warehouseService.create(mockRequest))
          .isInstanceOf(WarehouseAlreadyExistException.class);
  }

  @Test
  void testCreate_WhenPhoneNumberWarehouseAlreadyExists_ReturnWarehouseAlreadyExistException() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockEntity = mockWarehouse(mockRequest);

    Mockito.when(repository.existsByPhoneNumber(mockRequest.getPhoneNumber())).thenReturn(true);
    Mockito.when(repository.getByName(mockRequest.getName())).thenReturn(mockEntity);

    Assertions.assertThatThrownBy(() -> warehouseService.create(mockRequest))
          .isInstanceOf(WarehouseAlreadyExistException.class);
  }

  @Test
  void testCreate_WhenCreateWarehouse_Successfully() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockEntity = mockWarehouse(mockRequest);

    Mockito.when(repository.existsByName(mockRequest.getName())).thenReturn(false);
    Mockito.when(repository.existsByPhoneNumber(mockRequest.getPhoneNumber())).thenReturn(false);
    Mockito.when(repository.saveAndFlush(mockEntity)).thenReturn(mockEntity);
    Mockito.when(repository.getByName(mockRequest.getName())).thenReturn(mockEntity);

    WarehouseResponse response = warehouseService.create(mockRequest);
    Assertions.assertThat(mockEntity.getName()).isEqualTo(response.getName());
    Assertions.assertThat(mockEntity.getPhoneNumber()).isEqualTo(response.getPhoneNumber());
    Assertions.assertThat(mockEntity.getAddressId()).isEqualTo(response.getAddressId());

  }

  @Test
  void testUpdate_WhenNotFoundId_ReturnWarehouseNotFoundException() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Mockito.when(repository.findById(mockId)).thenThrow(WarehouseNotFoundException.class);

    Assertions.assertThatThrownBy(() -> warehouseService.update(mockId, mockRequest))
          .isInstanceOf(WarehouseNotFoundException.class);
  }

  @Test
  void testUpdate_WhenUpdateWarehouseSuccess_ReturnResponseBody() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockEntity = mockWarehouse(mockRequest);
    mockEntity.setId(mockId);

    Mockito.when(repository.findById(mockId)).thenReturn(Optional.of(mockEntity));
    Mockito.when(repository.save(mockEntity)).thenReturn(mockEntity);

    WarehouseResponse response = warehouseService.update(mockId, mockRequest);
    Assertions.assertThat(mockEntity.getName()).isEqualTo(response.getName());
    Assertions.assertThat(mockEntity.getPhoneNumber()).isEqualTo(response.getPhoneNumber());
    Assertions.assertThat(mockEntity.getAddressId()).isEqualTo(response.getAddressId());
  }

  @Test
  void testUpdate_WhenNameRequestAlreadyExist_ReturnWarehouseAlreadyExistException() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockEntity = mockWarehouse(mockRequest);
    mockEntity.setId(mockId);
    mockEntity.setName("HieuThuHai");
    Warehouse mockTestEntity = mockWarehouse(mockRequest);
    mockTestEntity.setDeleted(false);

    Mockito.when(repository.findById(mockId)).thenReturn(Optional.of(mockEntity));
    Mockito.when(repository.existsByName(mockRequest.getName())).thenReturn(true);
    Mockito.when(repository.getByName(mockRequest.getName())).thenReturn(mockTestEntity);

    Assertions.assertThatThrownBy(() -> warehouseService.update(mockId, mockRequest))
          .isInstanceOf(WarehouseAlreadyExistException.class);


  }

  @Test
  void testUpdate_WhenPhoneNumberRequestAlreadyExist_ReturnWarehouseAlreadyExistException() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockEntity = mockWarehouse(mockRequest);
    mockEntity.setId(mockId);
    mockEntity.setName("HieuThuHai");
    mockEntity.setPhoneNumber("0374259819");
    Warehouse mockTestEntity = mockWarehouse(mockRequest);
    mockTestEntity.setDeleted(false);

    Mockito.when(repository.findById(mockId)).thenReturn(Optional.of(mockEntity));
    Mockito.when(repository.existsByName(mockRequest.getName())).thenReturn(false);
    Mockito.when(repository.existsByPhoneNumber(mockRequest.getPhoneNumber())).thenReturn(true);
    Mockito.when(repository.getByName(mockRequest.getName())).thenReturn(mockTestEntity);

    Assertions.assertThatThrownBy(() -> warehouseService.update(mockId, mockRequest))
          .isInstanceOf(WarehouseAlreadyExistException.class);


  }

  @Test
  void testUpdate_WhenWarehouseIsDeleted_ReturnWarehouseNotFoundException() {
    WarehouseRequest mockRequest = mockWarehouseRequest();
    Warehouse mockEntity = mockWarehouse(mockRequest);
    mockEntity.setDeleted(true);
    Mockito.when(repository.findById(mockId)).thenReturn(Optional.of(mockEntity));

    Assertions.assertThatThrownBy(() -> warehouseService.update(mockId, mockRequest))
          .isInstanceOf(WarehouseNotFoundException.class);


  }

  @Test
  void testList_WhenIsAllTrue_ReturnWarehousePageResponse() {
    List<WarehouseResponse> warehouses = new ArrayList<>();
    warehouses.add(mockWarehouseResponse1());
    warehouses.add(mockWarehouseResponse2());

    Mockito.when(repository.getAll()).thenReturn(warehouses);

    WarehousePageResponse response = warehouseService.list(null, 10, 0, true);

    Assertions.assertThat(response.getWarehouseResponses()).hasSize(warehouses.size());
    Assertions.assertThat(response.getAmount()).isEqualTo(warehouses.size());


  }

  @Test
  void testList_WhenIsAllFalse_ReturnsWarehousePageResponseMatchesKeyWord() {
    PageRequest pageable = PageRequest.of(0, 10);
    List<WarehouseResponse> warehouses = new ArrayList<>();
    warehouses.add(mockWarehouseResponse1());
    warehouses.add(mockWarehouseResponse2());

    Mockito.when(repository.search("DuyHieu", pageable)).thenReturn(warehouses);
    Mockito.when(repository.countSearch("DuyHieu")).thenReturn(warehouses.size());

    WarehousePageResponse response = warehouseService.list("DuyHieu", 10, 0, false);
    Assertions.assertThat(response.getWarehouseResponses()).hasSize(warehouses.size());
    Assertions.assertThat(response.getAmount()).isEqualTo(warehouses.size());

  }

  @Test
  void testDelete_WhenIdNotFound_ReturnWarehouseNotFoundException() {
    Mockito.when(repository.existsById(mockId)).thenReturn(false);

    Assertions.assertThatThrownBy(() -> warehouseService.deleteById(mockId)).isInstanceOf(WarehouseNotFoundException.class);
  }


}
