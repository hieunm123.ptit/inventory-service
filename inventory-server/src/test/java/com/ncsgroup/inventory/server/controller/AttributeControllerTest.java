package com.ncsgroup.inventory.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncsgroup.inventory.client.dto.AttributeCreateRequest;
import com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.exception.attribute.AttributeAlreadyExistException;
import com.ncsgroup.inventory.server.service.AttributeService;
import com.ncsgroup.inventory.server.service.MessageService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.CREATE_ATTRIBUTE_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.UPDATE_ATTRIBUTE_SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@WebMvcTest(AttributeController.class)
class AttributeControllerTest {
  private static final String END_POINT_PATH = "/api/v1/attributes";

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper objectMapper;


  @MockBean
  AttributeService attributeService;

  @MockBean
  MessageService messageService;


  private AttributeCreateRequest getAttributeCreateRequest() {
    return new AttributeCreateRequest(
          "San suat"
    );
  }


  private AttributeResponse getAttributeResponse() {
    return new AttributeResponse(
          "1",
          "San suat",
          Active.ACTIVE
    );
  }

  @Test
  void create_WhenInputValid_Return201() throws Exception {
    AttributeCreateRequest request = getAttributeCreateRequest();

    AttributeResponse response = getAttributeResponse();

    String successMessage = "Creat attribute successfully";

    Mockito.when(messageService.getMessage(CREATE_ATTRIBUTE_SUCCESS, "en")).thenReturn(successMessage);
    Mockito.when(attributeService.create(request)).thenReturn(response);

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isCreated())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(response.getId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(response.getName()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.is_active").value(response.getIsActive().toString()));
  }

  @Test
  void create_WhenInputValid_ReturnCategoryAlreadyExist() throws Exception {
    AttributeCreateRequest request = getAttributeCreateRequest();

    Mockito.when(attributeService.create(request)).thenThrow(new AttributeAlreadyExistException());

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isConflict())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Conflict"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("com.ncsgroup.inventory.server.exception.attribute.AttributeAlreadyExistException"));
  }

  @Test
  void create_WhenInputNameIsNull_Return400BadRequest() throws Exception {
    AttributeCreateRequest request = getAttributeCreateRequest();
    request.setName("");

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Invalid name"));
  }

  @Test
  void create_WhenInputNameValidate_Return400BadRequest() throws Exception {
    AttributeCreateRequest request = getAttributeCreateRequest();
    request.setName("@@@");

    mockMvc.perform(post(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Invalid name"));
  }

  @Test
  void update_WhenInputNameValidate_Return400BadRequest() throws Exception {
    AttributeCreateRequest request = getAttributeCreateRequest();
    request.setName("@@@");

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Invalid name"));
  }

  @Test
  void update_WhenInputNameIsNull_Return400BadRequest() throws Exception {
    AttributeCreateRequest request = getAttributeCreateRequest();
    request.setName("");

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isBadRequest())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Bad Request"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("Invalid name"));
  }

  @Test
  void update_WhenInputValid_ReturnCategoryAlreadyExist() throws Exception {
    String id = "1";
    AttributeCreateRequest request = getAttributeCreateRequest();

    Mockito.when(attributeService.update(id, request)).thenThrow(new AttributeAlreadyExistException());

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isConflict())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Conflict"))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.code").value("com.ncsgroup.inventory.server.exception.attribute.AttributeAlreadyExistException"));
  }

  @Test
  void update_WhenInputValid_Return200() throws Exception {
    String id = "1";
    AttributeCreateRequest request = getAttributeCreateRequest();

    AttributeResponse response = getAttributeResponse();

    String successMessage = "Update attribute successfully";

    Mockito.when(messageService.getMessage(UPDATE_ATTRIBUTE_SUCCESS, "en")).thenReturn(successMessage);
    Mockito.when(attributeService.update(id, request)).thenReturn(response);

    mockMvc.perform(put(END_POINT_PATH + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(request))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").value(response.getId()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(response.getName()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.is_active").value(response.getIsActive().toString()));
  }

  @Test
  void list_WhenInputValid_Return200() throws Exception {
    List<AttributeResponse> responses = new ArrayList<>();
    responses.add(getAttributeResponse());
    PageResponse<AttributeResponse> response = PageResponse.of(responses, 1);

    String successMessage = "Success";

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn(successMessage);
    Mockito.when(attributeService.list("San", 2, 0, false, 1)).thenReturn(response);

    mockMvc.perform(get(END_POINT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .param("keyword", "San")
                .param("size", String.valueOf(2))
                .param("page", String.valueOf(0))
                .param("all", String.valueOf(false))
                .param("type", String.valueOf(1))
                .header("LANGUAGE", "en"))
          .andExpect(MockMvcResultMatchers.status().isOk())
          .andExpect(MockMvcResultMatchers.jsonPath("$.message").value(successMessage))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.amount").value(response.getAmount()))
          .andExpect(MockMvcResultMatchers.jsonPath("$.data.content[*].name").value(response.getContent().get(0).getName()));
  }
}
