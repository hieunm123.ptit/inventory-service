package com.ncsgroup.inventory.server.configuration;


import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.ncsgroup.inventory.server.facade.*;
import com.ncsgroup.inventory.server.facade.impl.*;
import com.ncsgroup.inventory.server.repository.*;
import com.ncsgroup.inventory.server.service.*;
import com.ncsgroup.inventory.server.service.impl.*;
import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@TestConfiguration
@EnableJpaRepositories(basePackages = {"com.ncsgroup.inventory.server.repository"},
      entityManagerFactoryRef = "testEntityManagerFactory",
      transactionManagerRef = "testTransactionManager")
@ComponentScan(basePackages = "com.ncsgroup.inventory.server.repository")
@EnableTransactionManagement
public class InventoryTestConfiguration {

  @Value("${spring.data.redis.host}")
  private String redisHostName;
  @Value("${spring.data.redis.port}")
  private int redisPort;

  @Value("${cloud.aws.credentials.access-key}")
  private String accessKey;

  @Value("${cloud.aws.credentials.secret-key}")
  private String secretKey;

  @Value("${cloud.aws.region.static}")
  private String region;

  @Bean
  public WarehouseService warehouseService(WarehouseRepository repository) {
    return new WarehouseServiceImpl(repository);
  }

  @Bean
  public CategoryService categoryServiceTest(CategoryRepository repository) {
    return new CategoryServiceImpl(repository);
  }

  @Bean
  public AttributeService attributeServiceTest(AttributeRepository repository) {
    return new AttributeServiceImpl(repository);
  }

  @Bean
  public ProductService productServiceTest(ProductRepository repository) {
    return new ProductServiceImpl(repository);
  }

  @Bean
  public ProductCategoryService productCategoryService(ProductCategoryRepository repository) {
    return new ProductCategoryServiceImpl(repository);
  }

  @Bean
  public ProductAttributeService productAttributeServiceTest(ProductAttributeRepository repository) {
    return new ProductAttributeServiceImpl(repository);
  }

  @Bean
  public ProductWarehouseService productWarehouseServiceTest(ProductWarehouseRepository repository) {
    return new ProductWarehouseServiceImpl(repository);
  }

  @Bean
  public ProductItemService productItemServiceTest(ProductItemRepository repository) {
    return new ProductItemServiceImpl(repository);
  }

  @Bean
  public ProductWarehouseFacadeService productWarehouseFacadeServiceTest(
        ProductWarehouseService productWarehouseService,
        ClassificationFacadeService classificationFacadeService
  ) {
    return new ProductWarehouseFacadeServiceImpl(productWarehouseService, classificationFacadeService);
  }

  @Bean
  public ClassificationFacadeService classificationFacadeServiceTest(
        ProductAttributeService productAttributeService,
        ProductItemService productItemService) {
    return new ClassificationFacadeServiceImpl(productAttributeService, productItemService);
  }

  @Bean
  public AmazonS3 s3() {
    AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
    return AmazonS3Client.builder()
          .withRegion(Regions.fromName(region))
          .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
          .build();
  }

  @Bean
  public AmazonService amazonService(AmazonS3 amazonS3) {
    return new AmazonServiceImpl(amazonS3);
  }

  @Bean
  public ImageService imageService(ImageRepository repository) {
    return new ImageServiceImpl(repository);
  }

  @Bean
  ImageFacadeService imageFacadeService(ImageService imageService, AmazonService amazonService) {
    return new ImageFacadeServiceImpl(imageService, amazonService);
  }

  @Bean
  public ProductFacadeService productFacadeService(
        ProductService productService,
        AttributeService attributeService,
        WarehouseService warehouseService,
        CategoryService categoryService,
        PromotionService promotionService,
        ProductCategoryService productCategoryService,
        ProductAttributeService productAttributeService,
        ClassificationFacadeService classificationFacadeService,
        ProductWarehouseService productWarehouseService,
        ProductWarehouseFacadeService productWarehouseFacadeService,
        ProductPromotionService promotionPromotionService,
        ImageService imageService,
        AmazonService amazonService
  ) {
    return new ProductFacadeServiceImpl(
          productService,
          categoryService,
          warehouseService,
          attributeService,
          promotionService,
          productCategoryService,
          productAttributeService,
          productWarehouseService,
          classificationFacadeService,
          productWarehouseFacadeService,
          promotionPromotionService,
          imageService,
          amazonService
    );

  }

  @Bean
  public PromotionService promotionService(PromotionRepository repository) {
    return new PromotionServiceImpl(repository);
  }

  @Bean
  public ProductPromotionService productPromotionService(ProductPromotionRepository repository) {
    return new ProductPromotionServiceImpl(repository);
  }

  @Bean
  public CartRedisService cartService(RedisTemplate<String, Object> redisTemplate) {
    return new CartRedisServiceImpl(redisTemplate);
  }

  @Bean
  public CartRedisFacadeService cartFacadeService(
        CartRedisService cartRedisService,
        ProductService productService,
        ProductItemService productItemService
  ) {
    return new CartRedisFacadeServiceImpl(
          cartRedisService,
          productService,
          productItemService
    );
  }

  @Bean
  JedisConnectionFactory jedisConnectionFactory() {
    JedisConnectionFactory jedisConFactory
          = new JedisConnectionFactory();
    jedisConFactory.setHostName(redisHostName);
    jedisConFactory.setPort(redisPort);
    return jedisConFactory;
  }

  @Bean
  public RedisTemplate<String, Object> redisTemplate() {
    RedisTemplate<String, Object> template = new RedisTemplate<>();

    template.setConnectionFactory(jedisConnectionFactory());
    template.setKeySerializer(new StringRedisSerializer());
    template.setHashKeySerializer(new StringRedisSerializer());
    template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
    template.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
    return template;
  }


  @Bean
  public DataSource dataSource() {

    EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
    return builder.setType(EmbeddedDatabaseType.H2).build();
  }


  @Bean
  public EntityManagerFactory testEntityManagerFactory() {

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    vendorAdapter.setGenerateDdl(true);

    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setJpaVendorAdapter(vendorAdapter);
    factory.setPackagesToScan("com.ncsgroup.inventory.server.entity");
    factory.setDataSource(dataSource());
    factory.afterPropertiesSet();
    return factory.getObject();
  }

  @Bean
  public PlatformTransactionManager testTransactionManager() {
    JpaTransactionManager txManager = new JpaTransactionManager();
    txManager.setEntityManagerFactory(testEntityManagerFactory());
    return txManager;
  }

}
