package com.ncsgroup.inventory.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncsgroup.inventory.client.dto.PromotionRequest;
import com.ncsgroup.inventory.client.dto.PromotionSearchRequest;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;

import com.ncsgroup.inventory.server.exception.promotion.PromotionNotFoundException;
import com.ncsgroup.inventory.server.service.MessageService;
import com.ncsgroup.inventory.server.service.PromotionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;


import java.util.ArrayList;
import java.util.List;

import static com.ncsgroup.inventory.client.constanst.Constants.Validate.INVALID_DATE;
import static com.ncsgroup.inventory.client.constanst.Constants.Validate.NOT_NULL;
import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.CREATE_PROMOTION_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.UPDATE_PROMOTION_SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PromotionController.class)
class PromotionControllerTest {
  @MockBean
  PromotionService promotionService;

  @MockBean
  MessageService messageService;

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper objectMapper;

  @Autowired
  PromotionController promotionController;


  private static final String END_POINT_PATH = "/api/v1/promotions";
  private static final String mockId = "id";

  private PromotionRequest mockPromotionRequest() {
    return new PromotionRequest(
          "1/1/2023",
          "2/2/2023",
          0,
          20.0,
          30,
          null,
          30

    );
  }

  private PromotionResponse promotionResponse() {
    return PromotionResponse.from(
          "PR_1",
          "1/1/2023",
          "2/2/2023",
          0,
          10.0,
          30,
          null,
          10,
          Active.INACTIVE
    );
  }

  private PromotionSearchRequest promotionSearchRequest() {
    return new PromotionSearchRequest(
          "PR_1",
          "01/01/2023",
          "02/02/2023",
          "22/12/2023",
          "23/12/2023",
          0,
          30.0,
          100,
          1
    );
  }

  @Test
  void testValid_WhenStartDayInvalid_ReturnInvalidDate() throws Exception {
    PromotionRequest mockRequest = mockPromotionRequest();
    mockRequest.setStartDay("1");

    Mockito.when(messageService.getMessage(CREATE_PROMOTION_SUCCESS, "en")).thenReturn("Success");

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value(INVALID_DATE))
          .andDo(print());

  }

  @Test
  void testValid_WhenEndDayInvalid_ReturnInvalidDate() throws Exception {
    PromotionRequest mockRequest = mockPromotionRequest();
    mockRequest.setEndDay("1");

    Mockito.when(messageService.getMessage(CREATE_PROMOTION_SUCCESS, "en")).thenReturn("Success");

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value(INVALID_DATE))
          .andDo(print());

  }


  @Test
  void testValid_WhenTypeNull_ReturnNotNull() throws Exception {
    PromotionRequest mockRequest = mockPromotionRequest();
    mockRequest.setType(null);

    Mockito.when(messageService.getMessage(CREATE_PROMOTION_SUCCESS, "en")).thenReturn("Success");

    mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.data.code")
                .value(NOT_NULL))
          .andDo(print());

  }

  @Test
  void testCreate_WhenCreateSuccessfully_Return201AndResponseBody() throws Exception {
    PromotionRequest mockRequest = mockPromotionRequest();
    PromotionResponse mockResponse = promotionResponse();

    Mockito.when(messageService.getMessage(CREATE_PROMOTION_SUCCESS, "en")).thenReturn("Success");
    Mockito.when(promotionService.create(mockRequest)).thenReturn(mockResponse);

    MvcResult result = mockMvc.perform(
                post(END_POINT_PATH)
                      .contentType(MediaType.APPLICATION_JSON)
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isCreated())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(promotionController.create(mockRequest, "en")));
  }

  @Test
  void testUpdate_WhenUpdateSuccessfully_Return200AndResponseBody() throws Exception {
    PromotionRequest mockRequest = mockPromotionRequest();
    PromotionResponse mockResponse = promotionResponse();

    Mockito.when(messageService.getMessage(UPDATE_PROMOTION_SUCCESS, "en")).thenReturn("Success");
    Mockito.when(promotionService.update(mockId, mockRequest)).thenReturn(mockResponse);

    MvcResult result = mockMvc.perform(
                put(END_POINT_PATH + "/{id}", mockId)
                      .contentType(MediaType.APPLICATION_JSON)
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(promotionController.update(mockId, mockRequest, "en")));
  }

  @Test
  void testUpdate_WhenIdPromotionNotFound_Return404AndException() throws Exception {
    PromotionRequest mockRequest = mockPromotionRequest();

    Mockito.when(messageService.getMessage(UPDATE_PROMOTION_SUCCESS, "en")).thenReturn("Success");
    Mockito.when(promotionService.update(mockId, mockRequest)).thenThrow(new PromotionNotFoundException());

    mockMvc.perform(
                put(END_POINT_PATH + "/{id}", mockId)
                      .contentType(MediaType.APPLICATION_JSON)
                      .content(objectMapper.writeValueAsString(mockRequest)))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.base.PromotionNotFoundException"))
          .andDo(print());
  }

  @Test
  void testDelete_WhenIdPromotionNotFound_Return404AndException() throws Exception {
    Mockito.doThrow(new PromotionNotFoundException()).when(promotionService).deleteById(mockId);

    mockMvc.perform(
                delete(END_POINT_PATH + "/{id}", mockId)
                      .contentType("application/json"))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.base.PromotionNotFoundException"))
          .andDo(print());
  }

  @Test
  void testDelete_WhenSuccess_ReturnMessage200() throws Exception {
    Mockito.doNothing().when(promotionService).deleteById(mockId);
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");

    mockMvc.perform(
                delete(END_POINT_PATH + "/{id}", mockId)
                      .contentType("application/json"))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());
  }

  @Test
  void testChangeActive_WhenIdPromotionNotFound_Return404AndException() throws Exception {
    Mockito.doThrow(new PromotionNotFoundException()).when(promotionService).changeActive(mockId);

    mockMvc.perform(
                post(END_POINT_PATH + "/{id}", mockId)
                      .contentType("application/json"))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.data.code")
                .value("com.ncsgroup.inventory.server.exception.base.PromotionNotFoundException"))
          .andDo(print());
  }

  @Test
  void testChangeActive_WhenSuccess_ReturnMessage200() throws Exception {
    Mockito.doNothing().when(promotionService).changeActive(mockId);
    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");

    mockMvc.perform(
                post(END_POINT_PATH + "/{id}", mockId)
                      .contentType("application/json"))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.message")
                .value("Success"))
          .andDo(print());
  }

  @Test
  void testList_WhenIsAllTrue_Return200AndListResponse() throws Exception {
    List<PromotionResponse> mock = new ArrayList<>();
    mock.add(promotionResponse());
    mock.add(promotionResponse());
    PageResponse<PromotionResponse> mockPageResponse = new PageResponse<>();
    mockPageResponse.setContent(mock);
    mockPageResponse.setAmount(mock.size());

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.when(
          promotionService.list(promotionSearchRequest(), 0, 10, true)).thenReturn(mockPageResponse);

    MvcResult result = mockMvc.perform(
                post(END_POINT_PATH + "/list")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(promotionSearchRequest()))
                      .param("size", String.valueOf(10))
                      .param("page", String.valueOf(0))
                      .param("all", String.valueOf(true)))
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(
                promotionController.list(promotionSearchRequest(), 10, 0, true, "en")));

  }

  @Test
  void testList_WhenIsAllFalse_Return200AndListResponse() throws Exception {
    List<PromotionResponse> mock = new ArrayList<>();
    mock.add(promotionResponse());
    mock.add(promotionResponse());
    PageResponse<PromotionResponse> mockPageResponse = new PageResponse<>();
    mockPageResponse.setContent(mock);
    mockPageResponse.setAmount(mock.size());

    Mockito.when(messageService.getMessage(SUCCESS, "en")).thenReturn("Success");
    Mockito.when(
          promotionService.list(promotionSearchRequest(), 0, 10, false)).thenReturn(mockPageResponse);

    MvcResult result = mockMvc.perform(
                post(END_POINT_PATH + "/list")
                      .contentType("application/json")
                      .content(objectMapper.writeValueAsString(promotionSearchRequest()))
                      .param("size", String.valueOf(10))
                      .param("page", String.valueOf(0))
                      .param("all", String.valueOf(false)))
          .andExpect(status().isOk())
          .andDo(print())
          .andReturn();

    String response = result.getResponse().getContentAsString();
    Assertions.assertEquals(
          response,
          objectMapper.writeValueAsString(
                promotionController.list(promotionSearchRequest(), 10, 0, false, "en")));

  }


}




