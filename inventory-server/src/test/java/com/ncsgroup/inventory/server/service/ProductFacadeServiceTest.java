package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.product.*;
import com.ncsgroup.inventory.server.configuration.InventoryTestConfiguration;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.dto.response.product.*;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.exception.attribute.AttributeNotFoundException;
import com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException;
import com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException;
import com.ncsgroup.inventory.server.exception.product.ProductItemDuplicateCodeException;
import com.ncsgroup.inventory.server.exception.product.ProductNotFoundException;
import com.ncsgroup.inventory.server.exception.promotion.PromotionAlreadyExistException;
import com.ncsgroup.inventory.server.exception.promotion.PromotionNotFoundException;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException;
import com.ncsgroup.inventory.server.facade.ClassificationFacadeService;
import com.ncsgroup.inventory.server.facade.ProductFacadeService;
import com.ncsgroup.inventory.server.facade.ProductWarehouseFacadeService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(ProductFacadeService.class)
@ContextConfiguration(classes = {InventoryTestConfiguration.class})
@Slf4j
class ProductFacadeServiceTest {
  @MockBean
  ProductService productService;
  @MockBean
  CategoryService categoryService;
  @MockBean
  WarehouseService warehouseService;
  @MockBean
  AttributeService attributeService;

  @MockBean
  ImageService imageService;
  @MockBean
  PromotionService promotionService;
  @MockBean
  ProductCategoryService productCategoryService;
  @MockBean
  ProductAttributeService productAttributeService;
  @MockBean
  ProductWarehouseService productWarehouseService;
  @MockBean
  ClassificationFacadeService classificationFacadeService;
  @MockBean
  ProductWarehouseFacadeService productWarehouseFacadeService;
  @MockBean
  ProductPromotionService productPromotionService;
  @Autowired
  ProductFacadeService productFacadeService;

  private List<String> categoriesId() {
    List<String> categoryIds = new ArrayList<>();
    categoryIds.add("CategoryId1");
    categoryIds.add("CategoryId2");
    return categoryIds;
  }

  private List<CategoryProductResponse> mockCategoryResponse() {
    List<CategoryProductResponse> mock = new ArrayList<>();
    mock.add(new CategoryProductResponse("CategoryId1", "test1"));
    mock.add(new CategoryProductResponse("CategoryId2", "test2"));
    return mock;
  }


  private List<String> warehouseId() {
    List<String> warehouse = new ArrayList<>();
    warehouse.add("w1");
    warehouse.add("w2");
    return warehouse;
  }

  private List<String> promotionId() {
    List<String> promotion = new ArrayList<>();
    promotion.add("p1");
    promotion.add("p2");
    return promotion;
  }

  private ProductPromotionRequest mockProductPromotionRequest() {
    return new ProductPromotionRequest(promotionId());
  }


  private List<AttributeProductRequest> mockAttributeProductRequest() {


    AttributeProductRequest request1 = new AttributeProductRequest("attributeId1", "value1");
    AttributeProductRequest request2 = new AttributeProductRequest("attributeId2", "value2");

    List<AttributeProductRequest> attributeProductRequests = new ArrayList<>();
    attributeProductRequests.add(request1);
    attributeProductRequests.add(request2);

    return attributeProductRequests;
  }

  private List<ProductAttributeResponse> mockAttributeProductResponse() {

    ProductAttributeResponse response1 = new ProductAttributeResponse(
          "attributeId1",
          "color1",
          "value1",
          true
    );
    ProductAttributeResponse response2 = new ProductAttributeResponse(
          "attributeId2",
          "color2",
          "value2",
          true
    );

    List<ProductAttributeResponse> productAttributeResponses = new ArrayList<>();
    productAttributeResponses.add(response1);
    productAttributeResponses.add(response2);

    return productAttributeResponses;
  }


  private ProductItemRequest mockProductItemRequest(String code) {
    return new ProductItemRequest(code);
  }

  private ProductItemResponse mockProductItemResponse(String id, String code) {
    return ProductItemResponse.of(id, code);
  }

  private List<ClassificationRequest> mockClassificationRequestsType1() {
    List<ClassificationRequest> mock = new ArrayList<>();
    ClassificationRequest mockClassificationRequest1 = new ClassificationRequest(
          "test",
          "attributeId1",
          "hen xui",
          mockProductItemRequest("test1"),
          null
    );
    ClassificationRequest mockClassificationRequest2 = new ClassificationRequest(
          "test2",
          "attributeId1",
          "hen xui",
          mockProductItemRequest("test2"),
          null
    );
    mock.add(mockClassificationRequest1);
    mock.add(mockClassificationRequest2);
    return mock;

  }

  private List<ClassificationResponse> mockClassificationResponsesType1() {
    List<ClassificationResponse> mock = new ArrayList<>();
    ClassificationResponse mockClassificationResponse1 = new ClassificationResponse(
          "test",
          "attributeId3", "attributeId3",
          "hen xui",
          "test1", "test1",
          false
    );
    ClassificationResponse mockClassificationResponse2 = new ClassificationResponse(
          "test2",
          "attributeId4", "attributeId4",
          "hen xui",
          "test2", "test2",
          false
    );
    mock.add(mockClassificationResponse1);
    mock.add(mockClassificationResponse2);
    return mock;

  }


  private List<ClassificationRequest> mockClassificationRequestsType2() {
    List<ClassificationRequest> mockSubClassification = new ArrayList<>();
    List<ClassificationRequest> mockClassificationRequests = new ArrayList<>();

    ClassificationRequest mockSubClassificationRequest1 = new ClassificationRequest(
          "test1",
          "attributeId1",
          "hen xui",
          mockProductItemRequest("test1"),
          null
    );

    ClassificationRequest mockSubClassificationRequest2 = new ClassificationRequest(
          "test2",
          "attributeId1",
          "hen xui",
          mockProductItemRequest("test2"),
          null
    );

    mockSubClassification.add(mockSubClassificationRequest1);
    mockSubClassification.add(mockSubClassificationRequest2);

    ClassificationRequest mockClassificationRequest = new ClassificationRequest(
          "test1",
          "attributeId",
          "lom dom",
          null,
          mockSubClassification
    );

    mockClassificationRequests.add(mockClassificationRequest);

    return mockClassificationRequests;

  }

  private List<ProductWarehouseRequest> mockProductWarehouseRequests() {
    ProductWarehouseRequest mockRequest1 = new ProductWarehouseRequest(
          "testWarehouse1",
          "test1",
          1000.0,
          10000.0,
          200,
          20,
          "VND"

    );

    ProductWarehouseRequest mockRequest2 = new ProductWarehouseRequest(
          "testWarehouse2",
          "test2",
          1000.0,
          10000.0,
          200,
          20,
          "USD"

    );

    List<ProductWarehouseRequest> mock = new ArrayList<>();
    mock.add(mockRequest1);
    mock.add(mockRequest2);

    return mock;
  }

  private List<ProductWarehouseResponse> mockProductWarehouseResponse() {
    ProductWarehouseResponse mockResponse1 = ProductWarehouseResponse.of(
          WarehouseResponse.from("testWarehouse1", "kho1", "0374259818", "hn"),
          mockProductItemResponse("test1", "test1"),
          1000.0,
          10000.0,
          200,
          20,
          "VND"

    );

    ProductWarehouseResponse mockResponse2 = ProductWarehouseResponse.of(
          WarehouseResponse.from("testWarehouse2", "kho2", "0374259818", "hn"),
          mockProductItemResponse("test2", "test2"),
          1000.0,
          10000.0,
          200,
          20,
          "VND"

    );


    List<ProductWarehouseResponse> mock = new ArrayList<>();
    mock.add(mockResponse1);
    mock.add(mockResponse2);

    return mock;
  }


  private ImageResponse images() {
    return new ImageResponse();
  }


  private ProductRequest mockRequest() {
    return new ProductRequest(
          "code", "ao bong da", "cai",
          1, "VN", "mem", "seller", List.of("1", "2"),
          categoriesId(), mockAttributeProductRequest(), null, mockProductWarehouseRequests()

    );
  }

  private ProductAdminResponse mockProductAdminResponse1() {
    return new ProductAdminResponse(
          "1",
          "test1",
          "quan bong da",
          mockCategoryResponse(),
          mockProductWarehouseResponse(),
          null, images(),
          Active.ACTIVE
    );
  }

  private ProductAdminResponse mockProductAdminResponse2() {
    return new ProductAdminResponse(
          "2",
          "test2",
          "ao bong da",
          mockCategoryResponse(),
          mockProductWarehouseResponse(),
          null, images(),
          Active.ACTIVE
    );

  }

  private ProductFilterRequest mockProductFilterRequest1() {
    return new ProductFilterRequest(
          "ao",
          0,
          1000,
          categoriesId(),
          mockAttributeProductRequest()
    );
  }

  private ImageResponse mockImageResponse() {
    return new ImageResponse();
  }

  private ProductResponse mockResponse() {
    return ProductResponse.of(
          "test", "code", "ao bong da", "cai",
          1, "VN", "mem", "seller", "Active", List.of(mockImageResponse()),
          null, null, null, null, null

    );
  }

  private ProductClientResponse mockProductClientResponse1() {
    return new ProductClientResponse(
          "test",
          "ao bong da",
          100L,
          1000.0);
  }

  private ProductClientResponse mockProductClientResponse2() {
    return new ProductClientResponse(
          "test1",
          "quan bong da",
          100L,
          1000.0);
  }


  private ProductAdminFilterRequest mockProductAdminFilterRequest() {
    return new ProductAdminFilterRequest(
          "1", "test",
          100, 1000, 1000, 10000, 10,
          categoriesId(),
          warehouseId(),
          mockAttributeProductRequest(),
          com.ncsgroup.inventory.client.enums.Active.ACTIVE
    );
  }

  @Test
  void testCreate_WhenCategoryIdNotFound_ReturnCategoryNotFoundException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.doThrow(new CategoryNotFoundException()).
          when(categoryService).checkCategoryIdsExist(mockRequest.getCategoryId());

    Assertions.assertThrows(CategoryNotFoundException.class, () -> productFacadeService.createProduct(mockRequest));
  }


  @Test
  void testCreate_WhenAttributeIdNotFound_ReturnAttributeNotFoundException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.doThrow(new AttributeNotFoundException()).when(attributeService).checkAttributeExists(Mockito.anyList());

    Assertions.assertThrows(AttributeNotFoundException.class, () -> productFacadeService.createProduct(mockRequest));
  }

  @Test
  void testCreate_WhenWarehouseIdNotFound_ReturnWarehouseNotFoundException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.doThrow(new WarehouseNotFoundException()).when(warehouseService).checkWarehousesExist(Mockito.anyList());

    Assertions.assertThrows(WarehouseNotFoundException.class, () -> productFacadeService.createProduct(mockRequest));
  }

  @Test
  void testCreate_WhenProductItemCodeType1AlreadyExists_ReturnsProductItemDuplicateCode() {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setClassifications(mockClassificationRequestsType1());

    Mockito.when(productService.create(mockRequest)).thenReturn(mockResponse());
    Mockito.doThrow(new ProductItemDuplicateCodeException())
          .when(classificationFacadeService).createClassifications(
                mockResponse().getId(),
                mockRequest.getClassifications()
          );

    Assertions.assertThrows(
          ProductItemDuplicateCodeException.class,
          () -> productFacadeService.createProduct(mockRequest));
  }

  @Test
  void testCreate_WhenProductItemCodeType2AlreadyExists_ReturnsProductItemDuplicateCode() {
    ProductRequest mockRequest = mockRequest();
    mockRequest.setClassifications(mockClassificationRequestsType2());

    Mockito.when(productService.create(mockRequest)).thenReturn(mockResponse());
    Mockito.doThrow(new ProductItemDuplicateCodeException())
          .when(classificationFacadeService).createClassifications(
                mockResponse().getId(),
                mockRequest.getClassifications()
          );

    Assertions.assertThrows(
          ProductItemDuplicateCodeException.class,
          () -> productFacadeService.createProduct(mockRequest));
  }

  @Test
  void testCreate_WhenProductItemCodeNotFound_ReturnsProductItemCodeNotFoundException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(productService.create(mockRequest)).thenReturn(mockResponse());
    Mockito.doThrow(new ProductItemCodeNotFoundException())
          .when(productWarehouseFacadeService).saveProductWarehouseService(
                mockResponse().getId(),
                mockRequest.getWarehouses(),
                mockRequest.getClassifications()

          );

    Assertions.assertThrows(
          ProductItemCodeNotFoundException.class,
          () -> productFacadeService.createProduct(mockRequest));
  }

  @Test
  void testCreate_WhenCreateProductSuccess_ReturnsResponseBody() {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(productService.create(mockRequest)).thenReturn(mockResponse());
    Mockito.doNothing().when(productCategoryService).save(mockResponse().getId(), mockRequest.getCategoryId());
    Mockito.doNothing().when(productWarehouseService).save(mockResponse().getId(), mockRequest.getWarehouses());
    Mockito.doNothing().when(productAttributeService).save(Mockito.anyList());
    Mockito.doNothing().when(imageService).addImageToProduct(List.of("1", "2"), mockResponse().getId());


    ProductResponse response = productFacadeService.createProduct(mockRequest());

    Assertions.assertEquals(mockResponse().getCode(), response.getCode());
    Assertions.assertEquals(mockResponse().getName(), response.getName());
    Assertions.assertEquals(mockResponse().getUnit(), response.getUnit());
    Assertions.assertEquals(mockResponse().getType(), response.getType());
    Assertions.assertEquals(mockResponse().getOrigin(), response.getOrigin());
    Assertions.assertEquals(mockResponse().getDescription(), response.getDescription());
    Assertions.assertEquals(mockResponse().getSellerId(), response.getSellerId());

  }

  @Test
  void testDetail_WhenProductIdNotFound_ReturnProductNotFoundException() {
    String id = "test";

    Mockito.when(productService.detail(id)).thenThrow(new ProductNotFoundException());

    Assertions.assertThrows(ProductNotFoundException.class, () -> productFacadeService.detailProduct(id));


  }

  @Test
  void testDetail_WhenProductExist_ReturnProductResponse() {
    String id = "test";

    ProductResponse mockResponse = mockResponse();
    mockResponse.setCategoryResponses(mockCategoryResponse());
    mockResponse.setAttributes(mockAttributeProductResponse());
    mockResponse.setClassifications(mockClassificationResponsesType1());
    mockResponse.setProductWarehouseResponses(mockProductWarehouseResponse());

    Mockito.when(productService.detail(id)).thenReturn(mockResponse);
    Mockito.when(productCategoryService.listByProductId(id)).thenReturn(mockCategoryResponse());
    Mockito.when(attributeService.getByProductId(id)).thenReturn(mockAttributeProductResponse());
    Mockito.when(classificationFacadeService.getClassifications(id)).thenReturn(mockClassificationResponsesType1());
    Mockito.when(productWarehouseService.listByProductId(id)).thenReturn(mockProductWarehouseResponse());

    ProductResponse response = productFacadeService.detailProduct(id);
    Assertions.assertEquals(mockResponse.getCode(), response.getCode());
    Assertions.assertEquals(mockResponse.getName(), response.getName());
    Assertions.assertEquals(mockResponse.getUnit(), response.getUnit());
    Assertions.assertEquals(mockResponse.getType(), response.getType());
    Assertions.assertEquals(mockResponse.getOrigin(), response.getOrigin());
    Assertions.assertEquals(mockResponse.getDescription(), response.getDescription());
    Assertions.assertEquals(mockResponse.getSellerId(), response.getSellerId());
    Assertions.assertEquals(mockResponse.getCategoryResponses(), response.getCategoryResponses());
    Assertions.assertEquals(mockResponse.getAttributes(), response.getAttributes());
    Assertions.assertEquals(mockResponse.getClassifications(), response.getClassifications());
    Assertions.assertEquals(mockResponse.getProductWarehouseResponses(), response.getProductWarehouseResponses());
  }

  @Test
  void update_WhenCategoryIdNotFound_ReturnCategoryNotFoundException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.doThrow(new CategoryNotFoundException()).
          when(categoryService).checkCategoryIdsExist(mockRequest.getCategoryId());

    Assertions.assertThrows(CategoryNotFoundException.class, () -> productFacadeService.update("1", mockRequest));
  }

  @Test
  void update_WhenAttributeIdNotFound_ReturnAttributeNotFoundException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.doThrow(new AttributeNotFoundException()).when(attributeService).checkAttributeExists(Mockito.anyList());

    Assertions.assertThrows(AttributeNotFoundException.class, () -> productFacadeService.update("1", mockRequest));
  }

  @Test
  void update_WhenWarehouseIdNotFound_ReturnWarehouseNotFoundException() {
    ProductRequest mockRequest = mockRequest();

    Mockito.doThrow(new WarehouseNotFoundException()).when(warehouseService).checkWarehousesExist(Mockito.anyList());

    Assertions.assertThrows(WarehouseNotFoundException.class, () -> productFacadeService.update("1", mockRequest));
  }

  @Test
  void update_WhenCreateProductSuccess_ReturnsResponseBody() {
    ProductRequest mockRequest = mockRequest();

    Mockito.when(productService.update("1", mockRequest)).thenReturn(mockResponse());
    Mockito.doNothing().when(productCategoryService).save(mockResponse().getId(), mockRequest.getCategoryId());
    Mockito.doNothing().when(productWarehouseService).save(mockResponse().getId(), mockRequest.getWarehouses());
    Mockito.doNothing().when(productAttributeService).save(Mockito.anyList());


    ProductResponse response = productFacadeService.update("1", mockRequest());

    Assertions.assertEquals(mockResponse().getCode(), response.getCode());
    Assertions.assertEquals(mockResponse().getName(), response.getName());
    Assertions.assertEquals(mockResponse().getUnit(), response.getUnit());
    Assertions.assertEquals(mockResponse().getType(), response.getType());
    Assertions.assertEquals(mockResponse().getOrigin(), response.getOrigin());
    Assertions.assertEquals(mockResponse().getDescription(), response.getDescription());
    Assertions.assertEquals(mockResponse().getSellerId(), response.getSellerId());

  }

  @Test
  void testListInClientSide_WhenSuccess_ReturnResponseBody() {
    List<ProductClientResponse> mock = new ArrayList<>();
    mock.add(mockProductClientResponse1());
    mock.add(mockProductClientResponse2());
    Page<ProductClientResponse> mockPage = new PageImpl<>(mock);
    PageResponse<ProductClientResponse> mockPageResponse = PageResponse.of(
          mockPage.getContent(), (int) mockPage.getTotalElements());

    Mockito.when(productService.listInClientSide(0, 10))
          .thenReturn(mockPageResponse);


    PageResponse<ProductClientResponse> mockResponse =
          productFacadeService.listInClientSide(0, 10);

    Assertions.assertEquals(mock.size(), mockResponse.getContent().size());
    Assertions.assertEquals(mockPageResponse.getAmount(), mockResponse.getAmount());
  }

  @Test
  void testFilterInClientSide_WhenSuccess_ReturnResponseBody() {
    List<ProductClientResponse> mock = new ArrayList<>();
    mock.add(mockProductClientResponse1());
    mock.add(mockProductClientResponse2());
    Page<ProductClientResponse> mockPage = new PageImpl<>(mock);
    PageResponse<ProductClientResponse> mockPageResponse = PageResponse.of(
          mockPage.getContent(), (int) mockPage.getTotalElements());

    Mockito.when(productService.filterInClientSide(mockProductFilterRequest1(), 0, 10))
          .thenReturn(mockPageResponse);
    Mockito.when(productWarehouseService.filterInClientSide(mock)).thenReturn(mock);


    PageResponse<ProductClientResponse> mockResponse =
          productFacadeService.filterInClientSide(mockProductFilterRequest1(), 0, 10);

    Assertions.assertEquals(mock.size(), mockResponse.getContent().size());
    Assertions.assertEquals(mockPageResponse.getAmount(), mockResponse.getAmount());
  }

  @Test
  void testListInAdminSide_WhenSuccess_ReturnPageResponseBody() {
    List<ProductAdminResponse> mock = new ArrayList<>();
    mock.add(mockProductAdminResponse1());
    mock.add(mockProductAdminResponse2());
    Page<ProductAdminResponse> mockPage = new PageImpl<>(mock);

    Mockito.when(productService.listInAdminSide(0, 10)).thenReturn(
          PageResponse.of(mockPage.getContent(), (int) mockPage.getTotalElements()));

    PageResponse<ProductAdminResponse> pageResponse = productFacadeService.listInAdminSide(0, 10);
    Assertions.assertEquals(mockPage.getContent().size(), pageResponse.getContent().size());
    Assertions.assertEquals(mockPage.getTotalElements(), pageResponse.getAmount());
  }

  @Test
  void filterInAdminSide_WhenSuccess_ReturnPageResponseBody() {
    ProductAdminFilterRequest request = mockProductAdminFilterRequest();
    List<ProductAdminResponse> mock = new ArrayList<>();
    mock.add(mockProductAdminResponse1());
    mock.add(mockProductAdminResponse2());
    Page<ProductAdminResponse> mockPage = new PageImpl<>(mock);

    Mockito.when(productService.filterInAdminSide(request, 0, 10)).thenReturn(
          PageResponse.of(mockPage.getContent(), (int) mockPage.getTotalElements())
    );

    PageResponse<ProductAdminResponse> pageResponse = productFacadeService.filterInAdminSide(request, 0, 10);
    Assertions.assertEquals(mockPage.getContent().size(), pageResponse.getContent().size());
    Assertions.assertEquals(mockPage.getTotalElements(), pageResponse.getAmount());
  }

  @Test
  void testAddPromotionToProduct_WhenIdProductNotFound_ReturnProductNotFoundException() {
    String productId = "test";
    Mockito.doThrow(new ProductNotFoundException()).when(productService).checkProductExist(productId);

    ProductPromotionRequest mockRequest = mockProductPromotionRequest();

    Assertions.assertThrows(
          ProductNotFoundException.class,
          () -> productFacadeService.addPromotionToProduct(productId, mockRequest));
  }


  @Test
  void testAddPromotionToProduct_WhenIdPromotionNotFound_ReturnPromotionNotFoundException() {
    Mockito.doThrow(new PromotionNotFoundException()).when(promotionService).checkPromotionsExist(promotionId());
    Mockito.doNothing().when(productPromotionService).save("test", promotionId());

    ProductPromotionRequest mockRequest = mockProductPromotionRequest();

    Assertions.assertThrows(
          PromotionNotFoundException.class,
          () -> productFacadeService.addPromotionToProduct("test", mockRequest));
  }


  @Test
  void testAddPromotionToProduct_WhenIdAlreadyExist_ReturnPromotionAlreadyExist() {
    Mockito.doThrow(new PromotionAlreadyExistException())
          .when(productPromotionService).save("test", promotionId());

    ProductPromotionRequest mockRequest = mockProductPromotionRequest();

    Assertions.assertThrows(
          PromotionAlreadyExistException.class,
          () -> productFacadeService.addPromotionToProduct("test", mockRequest));
  }



}
