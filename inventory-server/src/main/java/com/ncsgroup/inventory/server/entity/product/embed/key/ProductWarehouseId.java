package com.ncsgroup.inventory.server.entity.product.embed.key;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Embeddable
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class ProductWarehouseId implements Serializable {
  private String productId;
  private String warehouseId;
  private String productItemCode;
}
