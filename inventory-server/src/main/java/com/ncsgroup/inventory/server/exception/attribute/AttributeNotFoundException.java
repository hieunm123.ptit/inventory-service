package com.ncsgroup.inventory.server.exception.attribute;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class AttributeNotFoundException extends NotFoundException {
  public AttributeNotFoundException() {
    setCode("com.ncsgroup.inventory.server.exception.attribute.AttributeNotFoundException");
  }
}
