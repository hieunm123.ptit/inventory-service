package com.ncsgroup.inventory.server.controller;

import com.ncsgroup.inventory.client.dto.product.ProductCartDeletionRequest;
import com.ncsgroup.inventory.client.dto.product.ProductCartRequest;
import com.ncsgroup.inventory.server.dto.response.ResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.facade.CartRedisFacadeService;
import com.ncsgroup.inventory.server.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.*;
import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.SUCCESS;

@RequestMapping("/api/v1/carts")
@Slf4j
@RestController
@RequiredArgsConstructor
public class CartController {

  private final CartRedisFacadeService cartRedisFacadeService;

  private final MessageService messageService;

  @PostMapping("{customer_id}")
  public ResponseGeneral<Void> addProductToCart(
        @PathVariable("customer_id") String customerId,
        @RequestBody ProductCartRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(addProductToCart) customerId:{}, request:{}", customerId, request);

    cartRedisFacadeService.addProductToCart(customerId, request);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }


  @DeleteMapping("{customer_id}")
  public ResponseGeneral<Void> deleteProductInCart(
        @PathVariable("customer_id") String customerId,
        @RequestBody ProductCartDeletionRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(deleteProductInCart) customerId:{}, request:{}", customerId, request);

    cartRedisFacadeService.deleteProductInCart(customerId, request);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }

  @PutMapping("{customer_id}")
  public ResponseGeneral<Void> updateProductInCart(
        @PathVariable("customer_id") String customerId,
        @RequestBody ProductCartRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(addProductToCart) customerId:{}, request:{}", customerId, request);

    cartRedisFacadeService.updateProductInCart(customerId, request);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }

  @GetMapping("{customer_id}")
  public ResponseGeneral<List<CartItemResponse>> getProductFromCart(
        @PathVariable("customer_id") String customerId,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(getProductFromCart) customerId:{}", customerId);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          cartRedisFacadeService.getProductFromCart(customerId)
    );
  }


}
