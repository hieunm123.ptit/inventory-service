package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.AttributeCreateRequest;
import com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAttributeResponse;
import com.ncsgroup.inventory.server.entity.Attribute;
import com.ncsgroup.inventory.server.service.base.BaseService;

import java.util.List;

public interface AttributeService extends BaseService<Attribute> {
  AttributeResponse create(AttributeCreateRequest request);

  AttributeResponse update(String id, AttributeCreateRequest request);

  PageResponse<AttributeResponse> list(String keyword, int size, int page, boolean isAll, int type);

  void checkAttributeExists(List<String> attributeIds);

  List<ProductAttributeResponse> getByProductId(String productId);

}
