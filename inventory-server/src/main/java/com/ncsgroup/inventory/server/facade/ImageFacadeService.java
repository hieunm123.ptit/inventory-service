package com.ncsgroup.inventory.server.facade;

import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageFacadeService {
  List<ImageResponse> uploadImage(List<MultipartFile> file);
}
