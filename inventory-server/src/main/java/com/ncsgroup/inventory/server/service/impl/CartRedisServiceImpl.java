package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.client.dto.CartItemRequest;
import com.ncsgroup.inventory.client.dto.product.ProductCartDeletionRequest;
import com.ncsgroup.inventory.server.exception.cart_item.FieldKeyNotFoundException;
import com.ncsgroup.inventory.server.service.CartRedisService;
import com.ncsgroup.inventory.server.service.base.impl.BaseRedisServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.*;

import static com.ncsgroup.inventory.server.constanst.Constants.RedisConstants.*;
import static com.ncsgroup.inventory.server.utils.ConversionUtils.convertToIntegerValueMap;

@Slf4j
public class CartRedisServiceImpl extends BaseRedisServiceImpl implements CartRedisService {

  public CartRedisServiceImpl(RedisTemplate<String, Object> redisTemplate) {
    super(redisTemplate);
  }

  @Override
  public void addProductToCart(String customerId, List<CartItemRequest> items) {
    log.info("(addProductToCart) customerId: " + customerId + ", items: " + items);
    String fieldKey;
    Integer updateQuantity;
    for (CartItemRequest item : items) {

      if (Objects.nonNull(item.getProductItemId())) {
        fieldKey = CART_PRODUCT_ITEM_FIELD_PREFIX + item.getProductItemId();
      } else {
        fieldKey = CART_PRODUCT_FIELD_PREFIX + item.getProductId();
      }

      if (this.hashExists(customerId, fieldKey)) {
        updateQuantity = (Integer) this.hashGet(customerId, fieldKey) + item.getQuantity();
      } else {
        updateQuantity = item.getQuantity();
      }

      this.hashSet(customerId, fieldKey, updateQuantity);
    }

    this.setTimeToLive(customerId, CART_TIME_OUT);

  }

  @Override
  public void updateProductInCart(String customerId, List<CartItemRequest> items) {
    log.info("(updateProductInCart) customerId:{} items:{}", customerId, items);

    String fieldKey;
    Set<String> fieldKeys = new HashSet<>();
    for (CartItemRequest item : items) {
      fieldKey = Objects.nonNull(item.getProductItemId()) ?
            CART_PRODUCT_ITEM_FIELD_PREFIX + item.getProductItemId() : CART_PRODUCT_FIELD_PREFIX + item.getProductId();
      fieldKeys.add(fieldKey);

      this.hashSet(customerId, fieldKey, item.getQuantity());
    }

    this.deleteFieldKeys(customerId, fieldKeys);
    this.setTimeToLive(customerId, CART_TIME_OUT);
  }


  @Override
  public void deleteProductInCart(String customerId, ProductCartDeletionRequest request) {
    log.info("(deleteProductInCart) customerId:{}  ProductCartDeletionRequest :{}", customerId, request);

    if (Objects.nonNull(request.getProductItemId())) {

      this.checkFieldKeyExists(customerId, CART_PRODUCT_ITEM_FIELD_PREFIX + request.getProductItemId());
      this.delete(customerId, CART_PRODUCT_ITEM_FIELD_PREFIX + request.getProductItemId());

    } else {

      this.checkFieldKeyExists(customerId, CART_PRODUCT_FIELD_PREFIX + request.getProductId());
      this.delete(customerId, CART_PRODUCT_FIELD_PREFIX + request.getProductId());

    }

  }

  @Override
  public Map<String, Integer> getProductFromCart(String customerId) {
    log.info("(getProductFromCart) customerId :{}", customerId);

    Map<String, Object> product = this.getField(customerId);

    return convertToIntegerValueMap(product);
  }

  private void checkFieldKeyExists(String customerId, String fieldKey) {
    log.debug("(checkFieldKeyExists) customerId:{} fieldKey:{}", customerId, fieldKey);

    if (!this.hashExists(customerId, fieldKey)) {
      log.error("(checkFieldKeyExists) =======> FieldNotFoundException");

      throw new FieldKeyNotFoundException();
    }
  }

  private void deleteFieldKeys(String customerId, Set<String> fieldKeys) {
    log.debug("(deleteFieldKeys) customerId:{}, fieldKeys:{} ", customerId, fieldKeys);

    Set<String> keys = this.getFieldPrefixes(customerId);
    keys.removeAll(fieldKeys);

    this.delete(customerId, keys.stream().toList());
  }


}
