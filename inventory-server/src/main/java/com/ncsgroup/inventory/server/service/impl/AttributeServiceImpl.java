package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.client.dto.AttributeCreateRequest;
import com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAttributeResponse;
import com.ncsgroup.inventory.server.entity.Attribute;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.exception.attribute.AttributeAlreadyExistException;
import com.ncsgroup.inventory.server.exception.attribute.AttributeNotFoundException;
import com.ncsgroup.inventory.server.repository.AttributeRepository;
import com.ncsgroup.inventory.server.service.AttributeService;
import com.ncsgroup.inventory.server.service.base.impl.BaseServiceImpl;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Objects;

import static com.ncsgroup.inventory.server.constanst.Constants.VariableConstants.CLIENT;

@Slf4j
public class AttributeServiceImpl extends BaseServiceImpl<Attribute> implements AttributeService {
  private final AttributeRepository repository;

  public AttributeServiceImpl(AttributeRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  @Transactional
  public AttributeResponse create(AttributeCreateRequest request) {
    log.info("(create) request: {}", request);
    this.checkAlreadyExists(request.getName());

    Attribute attribute = Attribute.from(request.getName(), Active.ACTIVE);

    create(attribute);
    return toAttribute(attribute);
  }

  @Override
  @Transactional
  public AttributeResponse update(String id, AttributeCreateRequest request) {
    log.info("(update) id: {}, request: {}", id, request);
    Attribute attribute = checkNotFound(id);

    if (Objects.nonNull(request.getName()) &&
          !request.getName().equals(attribute.getName()) &&
          repository.existsByName(request.getName())) {
      log.error("(checkAlreadyExists) =======> AttributeAlreadyExistException");
      throw new AttributeAlreadyExistException();
    }
    attribute.setName(request.getName());
    create(attribute);

    return toAttribute(attribute);
  }

  @Override
  public PageResponse<AttributeResponse> list(String keyword, int size, int page, boolean isAll, int type) {
    log.info("(list) keyword: {}, size : {}, page: {}, isAll: {}, type: {}", keyword, size, page, isAll, type);

    Page<AttributeResponse> responsePage = null;
    List<AttributeResponse> attributes;

    if (isAll) {
      attributes = (type == CLIENT) ? repository.getAllClient() : repository.getAll();
    } else {
      responsePage = (type == CLIENT) ?
            repository.searchClient(PageRequest.of(page, size), keyword) :
            repository.search(PageRequest.of(page, size), keyword);

      attributes = responsePage.getContent();
    }

    int totalElements = isAll ? attributes.size() : (int) responsePage.getTotalElements();

    return PageResponse.of(attributes, totalElements);
  }

  @Override
  public void checkAttributeExists(List<String> attributeIds) {
    log.info("(checkAttributeExists) attributeIds: " + attributeIds);

    if (repository.countMatching(attributeIds) != attributeIds.size()) {
      log.error("(checkAttributeExists)========> AttributeNotFoundException ");
      throw new AttributeNotFoundException();
    }
  }

  @Override
  public List<ProductAttributeResponse> getByProductId(String productId) {
    log.info("(getByProductId) getByProductId :{}", productId);

    return repository.getByProduct(productId);
  }

  private Attribute checkNotFound(String id) {
    Attribute attribute = repository.findAttribute(id);
    if (attribute == null) {
      log.error("(checkNotFound) id: {}", id);
      throw new AttributeNotFoundException();
    }
    return attribute;
  }

  private void checkAlreadyExists(String name) {
    log.debug("(checkAlreadyExists) name: {}", name);
    if (repository.existsByName(name)) {
      log.error("(checkAlreadyExists) =======> AttributeAlreadyExistException");
      throw new AttributeAlreadyExistException();
    }

  }

  private AttributeResponse toAttribute(Attribute attribute) {
    log.debug("(toAttribute) attribute: {}", attribute);
    return new AttributeResponse(attribute.getId(), attribute.getName(), attribute.getIsActive());
  }

}
