package com.ncsgroup.inventory.server.entity;

import com.ncsgroup.inventory.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Entity
@Table(name = "category")
public class Category extends BaseEntityWithUpdater {
  private String parentId;
  private String name;
  private String imageId;
  private Boolean isDeleted;
  private Integer isActive;

  public static Category from(String name, String imageId, Integer isActive) {
    return Category.of(null, name, imageId, false, isActive);
  }

  public static Category from(String id, String name, String imageId, Integer isActive) {
    Category category = new Category();
    category.setId(id);
    category.setName(name);
    category.setParentId(null);
    category.setImageId(imageId);
    category.setIsDeleted(false);
    category.setIsActive(isActive);
    return category;
  }

  public static Category from(String name, String imageId, Integer isActive, String parentId) {
    return Category.of(parentId, name, imageId, false, isActive);
  }


}
