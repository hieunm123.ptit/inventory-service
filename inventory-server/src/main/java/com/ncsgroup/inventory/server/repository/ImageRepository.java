package com.ncsgroup.inventory.server.repository;

import com.ncsgroup.inventory.server.entity.Image;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ImageRepository extends BaseRepository<Image> {

  @Modifying
  @Query("update Image i set i.productId = :productId where i.id IN :imageIds AND i.isDeleted = false")
  void updateImage(List<String> imageIds, String productId);

  @Query(value = "select * from images i where i.product_id = ?1 and i.is_deleted = false", nativeQuery = true)
  List<Image> findImageByProductId(String productId);

  @Query("select count(*) from Image i where i.id IN :imageIds AND i.isDeleted = false")
  Integer totalImage(List<String> imageIds);

  @Modifying
  @Query("update Image i set i.isDeleted = true where i.id IN :imageIds")
  void deleteImage(List<String> imageIds);

  @Query(value = "select i.id from images i where i.product_id = ?1 and i.is_deleted = false", nativeQuery = true)
  List<String> findImageIdByProductId(String productId);

  @Query(value = "select * from images i where i.product_id = ?1 and i.is_deleted = false LIMIT 1", nativeQuery = true)
  Image getImageByProductId(String productId);

}
