package com.ncsgroup.inventory.server.exception.warehouse;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class WarehouseNotFoundException extends NotFoundException {
  public WarehouseNotFoundException() {
    setCode("com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException");
  }
}
