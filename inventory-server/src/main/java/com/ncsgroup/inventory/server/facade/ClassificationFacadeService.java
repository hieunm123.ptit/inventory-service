package com.ncsgroup.inventory.server.facade;

import com.ncsgroup.inventory.client.dto.product.ClassificationRequest;
import com.ncsgroup.inventory.server.dto.response.product.ClassificationResponse;

import java.util.List;
import java.util.Set;

public interface ClassificationFacadeService {

  void createClassifications(String productId,List<ClassificationRequest> requests);

  void updateClassifications(String productId, List<ClassificationRequest> requests);

  List<ClassificationResponse> getClassifications(String productId);

  Set<String> getCodeProductItems(List<ClassificationRequest> requests);

}
