package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.product.ProductItemRequest;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductItemResponse;
import com.ncsgroup.inventory.server.entity.product.ProductItem;
import com.ncsgroup.inventory.server.service.base.BaseService;

import java.util.List;


public interface ProductItemService extends BaseService<ProductItem> {
  ProductItemResponse create(ProductItemRequest request);

  void save(List<ProductItem> productItems);

  ProductItem findProductItem(String productItemId);

  void checkProductItemExisted(List<String> productItemIds);

  void checkProductItemExisted(String productItemId);

  List<CartItemResponse> getItems(List<String> itemIds);


}
