package com.ncsgroup.inventory.server.dto.response.warehouse;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class WarehouseResponse {
  private String code;
  private String name;
  private String phoneNumber;
  private String addressId;


  public static WarehouseResponse from(
        String code,
        String name,
        String phoneNumber,
        String addressId

  ) {
    return WarehouseResponse.of(code, name, phoneNumber, addressId);
  }


}
