package com.ncsgroup.inventory.server.facade;


import com.ncsgroup.inventory.client.dto.product.ProductAdminFilterRequest;
import com.ncsgroup.inventory.client.dto.product.ProductFilterRequest;
import com.ncsgroup.inventory.client.dto.product.ProductPromotionRequest;
import com.ncsgroup.inventory.client.dto.product.ProductRequest;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductResponse;


public interface ProductFacadeService {

  ProductResponse createProduct(ProductRequest request);

  ProductResponse detailProduct(String id);

  ProductResponse update(String id, ProductRequest productRequest);

  PageResponse<ProductClientResponse> listInClientSide(int page, int size);

  PageResponse<ProductClientResponse> filterInClientSide(ProductFilterRequest request, int page, int size);

  PageResponse<ProductAdminResponse> listInAdminSide(int page, int size);

  PageResponse<ProductAdminResponse> filterInAdminSide(ProductAdminFilterRequest request, int page, int size);

  void addPromotionToProduct(String productId, ProductPromotionRequest request);

}
