package com.ncsgroup.inventory.server.service.impl;


import com.ncsgroup.inventory.client.dto.product.ProductAdminFilterRequest;


import com.ncsgroup.inventory.client.dto.product.ProductWarehouseRequest;

import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductWarehouseResponse;
import com.ncsgroup.inventory.server.entity.product.embed.ProductWarehouse;
import com.ncsgroup.inventory.server.repository.ProductWarehouseRepository;
import com.ncsgroup.inventory.server.service.ProductWarehouseService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class ProductWarehouseServiceImpl implements ProductWarehouseService {
  private final ProductWarehouseRepository repository;

  @Override
  public void save(
        String productId,
        List<ProductWarehouseRequest> productWarehouseRequests
  ) {
    log.info("(save) productId: {}, requests: {}", productId, productWarehouseRequests);

    repository.saveAll(
          this.buildProductWarehouse(productId, productWarehouseRequests));
  }


  @Override
  @Transactional
  public void update(String productId, List<ProductWarehouseRequest> productWarehouseRequests) {
    log.info("(update) productId: {}, request: {}", productId, productWarehouseRequests);

    if (productWarehouseRequests == null) {
      productWarehouseRequests = new ArrayList<>();
    }

    for (ProductWarehouseRequest request : productWarehouseRequests) {
      repository.update(
            productId,
            request.getWarehouseId(),
            request.getProductItemCode(),
            request.getImportPrice(),
            request.getExportPrice(),
            request.getQuantity(),
            request.getQuantitySold()
      );
    }
  }


  @Override
  public List<ProductWarehouseResponse> listByProductId(String productId) {
    log.info("(listByProductId) productId: {}", productId);

    return repository.getListByProductId(productId);

  }

  @Override
  public List<ProductClientResponse> filterInClientSide(
        List<ProductClientResponse> productClientResponses
  ) {
    log.info("(filterInClientSide) productClientResponses: {}", productClientResponses);

    for (ProductClientResponse response : productClientResponses) {
      response.setQuantitySold(repository.getSumQuantitySold(response.getId()));
    }

    return productClientResponses;

  }

  @Override
  public List<ProductWarehouseResponse> filterInAdminSide(String productId, ProductAdminFilterRequest request) {
    log.info("(filterInAdminSide) productId:{}, request:{}", productId, request);

    return repository.filterProductWarehouse(
          productId,
          request.getWarehouses(),
          request.getQuantityRemaining(),
          request.getQuantitySold(),
          request.getRange()
    );
  }


  private List<ProductWarehouse> buildProductWarehouse(
        String productId,
        List<ProductWarehouseRequest> productWarehouseRequests
  ) {
    log.debug("(buildProductWarehouse) productId: {}, requests:{}", productId, productWarehouseRequests);

    List<ProductWarehouse> productWarehouses = new ArrayList<>(productWarehouseRequests.size());
    for (ProductWarehouseRequest request : productWarehouseRequests) {
      productWarehouses.add(
            ProductWarehouse.from(
                  productId,
                  request.getWarehouseId(),
                  request.getProductItemCode(),
                  request.getImportPrice(),
                  request.getExportPrice(),
                  request.getQuantity(),
                  request.getQuantitySold(),
                  request.getCurrencyCode()
            )
      );
    }

    return productWarehouses;
  }

}
