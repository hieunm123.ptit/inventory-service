package com.ncsgroup.inventory.server.repository;


import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.Warehouse;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface WarehouseRepository extends BaseRepository<Warehouse> {

  boolean existsByName(String name);

  @Query("SELECT CASE WHEN " +
        "(SELECT COUNT(w) FROM Warehouse w where w.id = :id and w.isDeleted =false) > 0 " +
        "THEN true ELSE false " +
        "END ")
  boolean existsById(String id);

  @Query("SELECT new com.ncsgroup.inventory.server.entity.Warehouse(" +
        "w.code, w.name, w.phoneNumber, w.addressId) " +
        "FROM Warehouse w " +
        "WHERE w.name = :name " +
        "AND w.isDeleted = false")
  Warehouse getByName(@Param("name") String name);


  @Query("SELECT " +
        "new com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse(" +
        "        w.code,w.name, w.phoneNumber, w.addressId) " +
        "FROM Warehouse w WHERE (:keyword is null or lower(w.name) LIKE lower(concat('%', :keyword, '%'))" +
        "OR lower(w.code) LIKE lower(concat('%', :keyword, '%')))" +
        "AND w.isDeleted <> true")
  List<WarehouseResponse> search(@Param("keyword") String keyword, Pageable pageable);




  @Query("SELECT COUNT(w) FROM Warehouse w WHERE (:keyword IS NULL OR" +
        " LOWER(w.name) LIKE %:keyword%" +
        " OR LOWER(w.code) LIKE %:keyword%)" +
        " AND w.isDeleted <> true")
  int countSearch(@Param("keyword") String keyword);

  @Query(value = "Update warehouse  SET is_deleted =true Where id = ?1", nativeQuery = true)
  @Modifying
  @Transactional
  void deleteById(String id);


  @Query(value = "SELECT count(*) from Warehouse w where w.id IN :ids")
  Integer totalWarehouse(List<String> ids);

  @Query("SELECT COUNT (DISTINCT w.id) FROM Warehouse w WHERE w.id IN :warehouseIds and w.isDeleted = false ")
  int countMatching(List<String> warehouseIds);


  @Query("""
        SELECT new com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse(
        w.code,w.name, w.phoneNumber, w.addressId)
        From Warehouse w Where w.isDeleted = false
        """
  )
  List<WarehouseResponse> getAll();

  @Query("SELECT CASE WHEN " +
        "(SELECT COUNT(w) FROM Warehouse w where w.phoneNumber = :phoneNumber and w.isDeleted =false) > 0 " +
        "THEN true ELSE false " +
        "END ")
  boolean existsByPhoneNumber(String phoneNumber);



}
