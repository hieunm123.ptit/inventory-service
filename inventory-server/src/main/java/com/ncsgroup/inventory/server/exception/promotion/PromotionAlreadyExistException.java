package com.ncsgroup.inventory.server.exception.promotion;

import com.ncsgroup.inventory.server.exception.base.BadRequestException;

public class PromotionAlreadyExistException extends BadRequestException {
  public PromotionAlreadyExistException() {
    setCode("com.ncsgroup.inventory.server.exception.base.PromotionAlreadyExistException");
  }
}
