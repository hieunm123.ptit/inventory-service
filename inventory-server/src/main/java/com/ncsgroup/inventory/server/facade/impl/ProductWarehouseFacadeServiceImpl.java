package com.ncsgroup.inventory.server.facade.impl;


import com.ncsgroup.inventory.client.dto.product.ClassificationRequest;
import com.ncsgroup.inventory.client.dto.product.ProductWarehouseRequest;
import com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException;

import com.ncsgroup.inventory.server.facade.ClassificationFacadeService;
import com.ncsgroup.inventory.server.facade.ProductWarehouseFacadeService;
import com.ncsgroup.inventory.server.service.ProductWarehouseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@RequiredArgsConstructor
@Slf4j
public class ProductWarehouseFacadeServiceImpl implements ProductWarehouseFacadeService {
  private final ProductWarehouseService productWarehouseService;
  private final ClassificationFacadeService classificationFacadeService;

  @Override
  public void saveProductWarehouseService(
        String productId,
        List<ProductWarehouseRequest> productWarehouseRequests,
        List<ClassificationRequest> classificationRequests
  ) {
    log.info("(saveProductWarehouseService) productId, {},productWarehouseRequests :{}, classificationRequests :{}",
          productId,
          productWarehouseRequests,
          classificationRequests);

    this.checkProductItemCodeExistsInSet(productWarehouseRequests, classificationRequests);

    productWarehouseService.save(productId, productWarehouseRequests);

  }

  private void checkProductItemCodeExistsInSet(
        List<ProductWarehouseRequest> productWarehouseRequests,
        List<ClassificationRequest> classificationRequests
  ) {
    log.debug("(checkProductItemCodeExistsInSet)");
    if (Objects.nonNull(classificationRequests)) {
      Set<String> codes = classificationFacadeService.getCodeProductItems(classificationRequests);

      for (ProductWarehouseRequest productWarehouseRequest : productWarehouseRequests) {

        if (Objects.nonNull(productWarehouseRequest.getProductItemCode())) {

          if (!codes.contains(productWarehouseRequest.getProductItemCode())) {

            log.error("(checkProductItemCodeExistsInSet) =======> ProductItemCodeNotFoundException");
            throw new ProductItemCodeNotFoundException();
          }
        }
      }
    }
  }
}
