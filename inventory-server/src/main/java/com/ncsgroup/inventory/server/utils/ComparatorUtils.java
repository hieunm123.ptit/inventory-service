package com.ncsgroup.inventory.server.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ComparatorUtils {
  private ComparatorUtils(){
  }

  public static <T> List<T> findUniqueElements(List<T> list1, List<T> list2) {
    Set<T> set = new HashSet<>(list2);
    List<T> uniqueElements = new ArrayList<>();

    for (T element : list1) {
      if (!set.contains(element)) {
        uniqueElements.add(element);
      }
    }

    return uniqueElements;
  }

}
