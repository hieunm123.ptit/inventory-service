package com.ncsgroup.inventory.server.service;


import com.ncsgroup.inventory.server.dto.response.product.ProductPromotionResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;

import java.util.List;

public interface ProductPromotionService {
  void save(String productId, List<String> promotionIds);

  List<ProductPromotionResponse> listByProductIdInClientSide(String productId);

  List<ProductPromotionResponse> listByProductIdInAdminSide(String productId);

  List<PromotionResponse> listByProductId(String productId);
}
