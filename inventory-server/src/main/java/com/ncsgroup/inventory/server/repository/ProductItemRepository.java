package com.ncsgroup.inventory.server.repository;

import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.entity.product.ProductItem;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ProductItemRepository extends BaseRepository<ProductItem> {
  @Query("SELECT CASE WHEN " +
        "(SELECT COUNT(p) FROM ProductItem p where p.code = :code and p.isDeleted =false) > 0 " +
        "THEN true ELSE false " +
        "END ")
  boolean existsByCode(String code);

  @Query("SELECT COUNT (DISTINCT p.id) FROM ProductItem p WHERE p.id IN :productItemIds and p.isDeleted = false ")
  int countMatching(List<String> productItemIds);

  @Query("SELECT CASE WHEN " +
        "(SELECT COUNT(p) FROM ProductItem p where p.id = :id and p.isDeleted =false) > 0 " +
        "THEN true ELSE false " +
        "END ")
  boolean existsById(String id);

  @Query("""
        SELECT  new com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse(
        p.id, pi.id, p.name, Min(pw.exportPrice), ppa.id, ppa.attributeValue, pa.id, pa.attributeValue)
        FROM Product p
        LEFT JOIN ProductAttribute pa On p.id = pa.productId
        LEFT JOIN ProductAttribute ppa ON ppa.id = pa.parentId
        LEFT JOIN ProductItem pi on pa.productItemId = pi.id
        LEFT JOIN ProductWarehouse pw ON pi.code = pw.id.productItemCode
        WHERE pi.id IN :itemIds
        AND p.isDeleted =false
        AND pi.isDeleted = false
        GROUP BY p.id, pi.id, pa.id, ppa.id
        """)
  List<CartItemResponse> getItems(List<String> itemIds);

}

