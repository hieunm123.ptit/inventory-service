package com.ncsgroup.inventory.server.exception.product;

import com.ncsgroup.inventory.server.exception.base.BadRequestException;

public class QuantityProductNotEnoughException extends BadRequestException {
  public QuantityProductNotEnoughException() {
    setCode("com.ncsgroup.inventory.server.exception.product.QuantityProductNotEnoughException");
  }

}
