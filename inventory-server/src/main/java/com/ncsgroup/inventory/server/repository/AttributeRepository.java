package com.ncsgroup.inventory.server.repository;

import com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAttributeResponse;
import com.ncsgroup.inventory.server.entity.Attribute;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AttributeRepository extends BaseRepository<Attribute> {
    boolean existsByName(String name);

    @Query(value = "select * from attributes a where a.id  = ?1 and a.is_deleted =false ", nativeQuery = true)
    Attribute findAttribute(String id);

    @Query("""
            select new com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse(a.id, a.name, a.isActive) \s
            from Attribute a where a.isDeleted = false and a.isActive = 0
            """)
    List<AttributeResponse> getAllClient();

    @Query("""
             select new com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse(a.id, a.name, a.isActive) \s
            from Attribute a \s
            where a.isDeleted = false and \s
            a.isActive = 0 and \s
            :keyword is null or \s
            lower(a.name) like %:keyword%
            """)
    Page<AttributeResponse> searchClient(Pageable pageable, String keyword);


    @Query("""
            select new com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse(a.id, a.name, a.isActive) \s
             from Attribute a where a.isDeleted = false
            """)
    List<AttributeResponse> getAll();

    @Query("""
            select new com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse(a.id, a.name, a.isActive) \s
            from Attribute a \s
            where a.isDeleted = false and \s
            :keyword is null or \s
            lower(a.name) like %:keyword%
            """)
    Page<AttributeResponse> search(Pageable pageable, String keyword);

    @Query("SELECT COUNT (DISTINCT a.id) FROM Attribute a WHERE a.id IN :attributeIds and a.isDeleted = false ")
    int countMatching(List<String> attributeIds);

    @Query("""
            select new com.ncsgroup.inventory.server.dto.response.product.ProductAttributeResponse (a.id, a.name, pa.attributeValue, pa.isDetail) \s
            from ProductAttribute pa \s
            left join Attribute a on pa.attributeId = a.id \s
            where pa.productId = :productId and pa.isDetail = true
            """)
    List<ProductAttributeResponse> getByProduct(String productId);

}
