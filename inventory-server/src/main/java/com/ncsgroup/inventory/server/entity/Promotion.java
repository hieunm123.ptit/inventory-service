package com.ncsgroup.inventory.server.entity;

import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Entity
@Table(name = "promotions")
public class Promotion extends BaseEntityWithUpdater {
  private String code;
  private Long startDay;
  private Long endDay;
  private Integer type;
  private Double discountPercent;
  private Integer moneyDeducted;
  private String description;
  private Integer coin;
  private Boolean isDeleted;
  @Enumerated(EnumType.ORDINAL)
  private Active isActive;

  public static Promotion from(
        Long startDay,
        Long endDay,
        Integer type,
        Double discountPercent,
        Integer moneyDeducted,
        String description,
        Integer coin
  ) {
    return Promotion.of(
          null, startDay, endDay, type, discountPercent,
          moneyDeducted, description, coin, false, Active.INACTIVE
    );
  }

  public Promotion(
        String code,
        Long startDay,
        Long endDay,
        Double discountPercent,
        Integer moneyDeducted,
        Integer coin,
        Active isActive) {
    this.code = code;
    this.startDay = startDay;
    this.endDay = endDay;
    this.discountPercent = discountPercent;
    this.moneyDeducted = moneyDeducted;
    this.coin = coin;
    this.isActive = isActive;
  }
}
