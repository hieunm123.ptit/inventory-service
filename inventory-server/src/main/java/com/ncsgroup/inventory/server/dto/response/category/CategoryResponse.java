package com.ncsgroup.inventory.server.dto.response.category;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CategoryResponse {
  private String id;
  private String name;
  private String imageId;
  private Integer isActive;
  private String parenId;
  private List<CategoryResponse> categories;

  public static CategoryResponse from(String id, String name, String imageId, Integer isActive, String parenId){
    return CategoryResponse.builder().id(id).name(name).imageId(imageId).isActive(isActive).parenId(parenId).build();
  }

  public static CategoryResponse from(String id, String name, String imageId, Integer isActive, String parenId, List<CategoryResponse> categories){
    return CategoryResponse.builder().id(id).name(name).imageId(imageId).isActive(isActive).parenId(parenId).categories(categories).build();
  }
}
