package com.ncsgroup.inventory.server.entity.product.embed;

import com.ncsgroup.inventory.server.entity.product.embed.key.ProductWarehouseId;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Entity
public class ProductWarehouse {
  @EmbeddedId
  private ProductWarehouseId id;
  private Double importPrice;
  private Double exportPrice;
  private Integer quantity;
  private Integer quantitySold;
  private String currencyCode;

  public static ProductWarehouse from(
        String productId,
        String warehouseId,
        String productItemCode,
        Double importPrice,
        Double exportPrice,
        Integer quantity,
        Integer quantitySold,
        String currencyCode
  ) {
    return ProductWarehouse.of(
          ProductWarehouseId.of(productId, warehouseId, productItemCode),
          importPrice,
          exportPrice,
          quantity,
          quantitySold,
          currencyCode
    );
  }

}
