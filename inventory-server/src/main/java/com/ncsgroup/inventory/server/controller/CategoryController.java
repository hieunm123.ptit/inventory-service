package com.ncsgroup.inventory.server.controller;

import com.ncsgroup.inventory.client.dto.CategoryCreateRequest;
import com.ncsgroup.inventory.client.dto.CategoryUpdateRequest;
import com.ncsgroup.inventory.server.dto.response.ResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.category.CategoryResponse;
import com.ncsgroup.inventory.server.service.CategoryService;
import com.ncsgroup.inventory.server.service.MessageService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.*;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/categories")
public class CategoryController {
  private final CategoryService categoryService;
  private final MessageService messageService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseGeneral<CategoryResponse> create(
        @RequestBody @Valid CategoryCreateRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(create) request: {}", request);
    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_CATEGORY_SUCCESS, language),
          categoryService.create(request)
    );
  }

  @GetMapping("{id}")
  public ResponseGeneral<CategoryResponse> detail(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(detail) id: {}", id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(DETAIL_CATEGORY_SUCCESS, language),
          categoryService.detail(id)
    );
  }


  @PutMapping("{id}")
  public ResponseGeneral<CategoryResponse> update(
        @PathVariable String id,
        @RequestBody @Valid CategoryUpdateRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(update) id: {}, request: {}", id, request);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(UPDATE_CATEGORY_SUCCESS, language),
          categoryService.update(id, request)
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(delete) id: {}", id);
    categoryService.deleteById(id);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(DELETE_CATEGORY_SUCCESS, language)

    );
  }

  @GetMapping
  public ResponseGeneral<List<CategoryResponse>> list(
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(list)");
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          categoryService.getAll()
    );
  }
}
