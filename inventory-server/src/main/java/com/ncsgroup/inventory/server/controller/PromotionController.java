package com.ncsgroup.inventory.server.controller;

import com.ncsgroup.inventory.client.dto.PromotionRequest;


import com.ncsgroup.inventory.client.dto.PromotionSearchRequest;
import com.ncsgroup.inventory.client.dto.product.ProductFilterRequest;
import com.ncsgroup.inventory.server.dto.response.ResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;


import com.ncsgroup.inventory.server.service.MessageService;
import com.ncsgroup.inventory.server.service.PromotionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.*;

import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.CREATE_PROMOTION_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.UPDATE_PROMOTION_SUCCESS;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/promotions")
@Slf4j
public class PromotionController {
  private final PromotionService promotionService;
  private final MessageService messageService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseGeneral<PromotionResponse> create(
        @Valid @RequestBody PromotionRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(create) request: {}", request);

    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_PROMOTION_SUCCESS, language),
          promotionService.create(request)
    );
  }


  @PutMapping("{id}")
  public ResponseGeneral<PromotionResponse> update(
        @PathVariable String id,
        @Valid @RequestBody PromotionRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(update) id:{}, request:{}", id, request);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(UPDATE_PROMOTION_SUCCESS, language),
          promotionService.update(id, request));
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(delete) id:{}", id);

    promotionService.deleteById(id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)

    );
  }

  @PostMapping("{id}")
  public ResponseGeneral<Void> changeActive(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(changeActive) id:{}", id);

    promotionService.changeActive(id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)

    );
  }

  @PostMapping("/list")
  public ResponseGeneral<PageResponse<PromotionResponse>> list(
        @RequestBody PromotionSearchRequest request,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "all", defaultValue = "false", required = false) boolean isAll,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(list) request:{}, size:{}, page:{}, isAll:{}", request, size, page, isAll);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          promotionService.list(request, page, size, isAll)
    );
  }


}
