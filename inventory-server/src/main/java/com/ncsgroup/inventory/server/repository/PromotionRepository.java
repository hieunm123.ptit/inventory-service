package com.ncsgroup.inventory.server.repository;


import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;

import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.Promotion;


import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PromotionRepository extends BaseRepository<Promotion> {

  @Query("""
        SELECT new com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse(
        p.code, p.startDay, p.endDay, p.discountPercent, p.moneyDeducted, p.description, p.coin, p.isActive)
        FROM Promotion p
        Where p.id =:id
        """)
  PromotionResponse getResponseById(String id);


  @Query("""
        SELECT p From Promotion p Where p.id = :id and p.isDeleted = false
        """
  )
  Promotion find(String id);

  @Query("""
        SELECT new com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse(
        pr.code, pr.startDay, pr.endDay, pr.discountPercent, pr.moneyDeducted,pr.description, pr.coin, pr.isActive)
        FROM Promotion pr
        WHERE ((:code IS NULL OR lower(pr.code) LIKE lower(concat('%', :code , '%')))
        AND ( (:fromStartDate IS NULL) OR (pr.startDay >= :fromStartDate))
        AND ( (:toStartDate IS NULL) OR (pr.startDay <= :toStartDate))
        AND ( (:fromEndDate IS NULL) OR (pr.endDay >= :fromEndDate))
        AND ( (:toEndDate IS NULL) OR (pr.endDay <= :toEndDate))
        AND ( (:type IS NULL) OR (pr.type = :type))
        AND ( (:discountPercent IS NULL) OR (pr.discountPercent = :discountPercent))
        AND ( (:moneyDeducted IS NULL) OR (pr.moneyDeducted = :moneyDeducted))
        AND ( (:active is null) or (pr.isActive = :active)))
        """
  )
  List<PromotionResponse> getAll(
        String code,
        Long fromStartDate,
        Long toStartDate,
        Long fromEndDate,
        Long toEndDate,
        Integer type,
        Double discountPercent,
        Integer moneyDeducted,
        Active active
  );

  @Query("""
        SELECT new com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse(
        pr.code, pr.startDay, pr.endDay, pr.discountPercent, pr.moneyDeducted,pr.description, pr.coin, pr.isActive)
        FROM Promotion pr
        Where ((:code IS NULL OR lower(pr.code) LIKE lower(concat('%', :code , '%')))
        AND ( (:fromStartDate IS NULL) OR (pr.startDay >= :fromStartDate))
        AND ( (:toStartDate IS NULL) OR (pr.startDay <= :toStartDate))
        AND ( (:fromEndDate IS NULL) OR (pr.endDay >= :fromEndDate))
        AND ( (:toEndDate IS NULL) OR (pr.endDay <= :toEndDate))
        AND ( (:type IS NULL) OR (pr.type = :type))
        AND ( (:discountPercent IS NULL) OR (pr.discountPercent = :discountPercent))
        AND ( (:moneyDeducted IS NULL) OR (pr.moneyDeducted = :moneyDeducted))
        AND ( (:active is null) or (pr.isActive = :active)))
        """
  )
  Page<PromotionResponse> filter(
        String code,
        Long fromStartDate,
        Long toStartDate,
        Long fromEndDate,
        Long toEndDate,
        Integer type,
        Double discountPercent,
        Integer moneyDeducted,
        Active active,
        Pageable pageable);

  @Query("""
        SELECT CASE WHEN
        (SELECT COUNT(p) FROM Promotion p where p.id = :id and p.isDeleted =false) > 0
        THEN true ELSE false
        END
        """)
  boolean existsById(String id);

  @Query(value = "Update promotions  SET is_deleted =true Where id = ?1", nativeQuery = true)
  @Modifying
  @Transactional
  void deleteById(String id);

  @Query(value = "Update promotions SET is_active = Case When is_active = 0 Then 1 else 0 END Where id = ?1", nativeQuery = true)
  @Modifying
  @Transactional
  void changeActive(String id);

  @Query("SELECT COUNT (DISTINCT p.id) FROM Promotion p WHERE p.id IN :promotionIds and p.isDeleted = false ")
  int countMatching(List<String> promotionIds);


}
