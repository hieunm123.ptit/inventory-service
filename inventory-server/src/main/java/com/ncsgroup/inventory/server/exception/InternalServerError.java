package com.ncsgroup.inventory.server.exception;

import com.ncsgroup.inventory.server.exception.base.BaseException;

import static com.ncsgroup.inventory.server.constanst.Constants.StatusExceptions.INTERNAL_SERVER_ERROR;

public class InternalServerError extends BaseException {
  public InternalServerError() {
    setCode("com.ncsgroup.inventory.server.exception.InternalServerError");
    setStatus(INTERNAL_SERVER_ERROR);
  }
}
