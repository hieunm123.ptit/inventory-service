package com.ncsgroup.inventory.server.exception.attribute;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class AttributeDuplicateNameException extends ConflictException {
  public AttributeDuplicateNameException(){
    setCode("com.ncsgroup.inventory.server.exception.attribute.AttributeDuplicateNameException");
  }
}
