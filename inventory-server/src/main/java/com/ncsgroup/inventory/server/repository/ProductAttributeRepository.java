package com.ncsgroup.inventory.server.repository;

import com.ncsgroup.inventory.server.dto.response.product.ClassificationResponse;
import com.ncsgroup.inventory.server.entity.product.ProductAttribute;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductAttributeRepository extends BaseRepository<ProductAttribute> {


  @Transactional
  @Modifying
  @Query("delete from ProductAttribute pa where pa.productId = :productId and pa.productItemId = null and pa.isDetail = true")
  void deleteByProductId(String productId);

  @Query(value = "select count(*) from ProductAttribute pa where pa.id IN :ids")
  int countProductAttribute(List<String> ids);

  @Query(value = "select pa from ProductAttribute pa where pa.id = :id and pa.attributeId = :attributeId")
  ProductAttribute findProductAttribute(String id, String attributeId);

  @Query("SELECT new com.ncsgroup.inventory.server.dto.response.product.ClassificationResponse(" +
        "pa.id, a.name, pa.attributeId,pa.attributeValue, pi.id, pi.code,pa.isDetail ) " +
        "From ProductAttribute pa " +
        "Join Attribute a on  pa.attributeId = a.id " +
        "left JOIN ProductItem pi on  pa.productItemId = pi.id " +
        "Where pa.productId = :productId " +
        "and pa.isDetail = false " +
        "and pa.parentId = null " +
        "and (pa.productItemId = null or pi.isDeleted = false )")
  List<ClassificationResponse> listByProductId(String productId);

  @Query("SELECT new com.ncsgroup.inventory.server.dto.response.product.ClassificationResponse(" +
        "pa.id, a.name, pa.attributeId,pa.attributeValue, pi.id, pi.code,pa.isDetail ) " +
        "From ProductAttribute pa " +
        "Join Attribute a on  pa.attributeId = a.id " +
        "left JOIN ProductItem pi on  pa.productItemId = pi.id " +
        "Where pa.parentId = :parentId " +
        "and pa.isDetail = false "
       )
  List<ClassificationResponse> listByParentId(String parentId);


}
