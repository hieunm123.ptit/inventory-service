package com.ncsgroup.inventory.server.dto.response.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductResponse {
  private String id;
  private String code;
  private String name;
  private String unit;
  private Integer type;
  private String origin;
  private String description;
  private String sellerId;
  private String status;
  private List<ImageResponse> imageResponses;
  private List<CategoryProductResponse> categoryResponses;
  private List<ProductAttributeResponse> attributes;
  private List<ClassificationResponse> classifications;
  private List<ProductWarehouseResponse> productWarehouseResponses;
  private List<PromotionResponse> promotions;

  public static ProductResponse from(
        String id,
        String code,
        String name,
        String unit,
        Integer type,
        String origin,
        String description,
        String sellerId

  ) {

    return new ProductResponse(
          id, code, name, unit, type,
          origin, description, sellerId
    );
  }

  public ProductResponse(
        String id, String code, String name, String unit, Integer type,
        String origin, String description, String sellerId
  ) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.unit = unit;
    this.type = type;
    this.origin = origin;
    this.description = description;
    this.sellerId = sellerId;

  }
}
