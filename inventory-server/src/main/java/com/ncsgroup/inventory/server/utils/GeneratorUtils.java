package com.ncsgroup.inventory.server.utils;


import java.util.UUID;

public class GeneratorUtils {

  private GeneratorUtils() {
  }
  public static String generateId() {
    return UUID.randomUUID().toString();
  }
}
