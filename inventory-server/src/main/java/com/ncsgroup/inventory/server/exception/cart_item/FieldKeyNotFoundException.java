package com.ncsgroup.inventory.server.exception.cart_item;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class FieldKeyNotFoundException extends NotFoundException {

  public FieldKeyNotFoundException(){
    setCode("com.ncsgroup.inventory.server.exception.CartItem.FieldKeyNotFoundException");
  }
}
