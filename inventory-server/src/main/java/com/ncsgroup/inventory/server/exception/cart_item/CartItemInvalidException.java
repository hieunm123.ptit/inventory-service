package com.ncsgroup.inventory.server.exception.cart_item;

import com.ncsgroup.inventory.server.exception.base.BadRequestException;

public class CartItemInvalidException extends BadRequestException {
  public CartItemInvalidException() {
    setCode("com.ncsgroup.inventory.server.exception.CartItem.CartItemInvalidException");
  }
}
