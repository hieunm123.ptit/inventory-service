package com.ncsgroup.inventory.server.repository;

import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.entity.product.embed.ProductCategory;
import com.ncsgroup.inventory.server.entity.product.embed.key.ProductCategoryId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, ProductCategoryId> {

  @Modifying
  @Query(value = "delete from product_category pc where pc.product_id = ?1", nativeQuery = true)
  void deleteByProductId(String productId);

  @Query("SELECT new com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse(" +
        "c.name, c.imageId) " +
        "from ProductCategory pc " +
        "join Category c on pc.id.categoryId = c.id " +
        "where pc.id.productId = :productId")
  List<CategoryProductResponse> listByProductId(String productId);
}
