package com.ncsgroup.inventory.server.dto.response.category;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CategoryProductResponse {
  private String name;
  private String imageId;

  public CategoryProductResponse(String name, String imageId) {
    this.name = name;
    this.imageId = imageId;
  }

}
