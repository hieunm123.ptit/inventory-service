package com.ncsgroup.inventory.server.facade.impl;

import com.ncsgroup.inventory.client.dto.File;
import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.entity.Image;
import com.ncsgroup.inventory.server.facade.ImageFacadeService;
import com.ncsgroup.inventory.server.service.AmazonService;
import com.ncsgroup.inventory.server.service.ImageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@Slf4j
@RequiredArgsConstructor
public class ImageFacadeServiceImpl implements ImageFacadeService {
    private final ImageService imageService;
    private final AmazonService amazonService;

    @Override
    public List<ImageResponse> uploadImage(List<MultipartFile> multipartFile) {
        log.info("(uploadImage)");

        List<File> files = amazonService.uploadFile(multipartFile);

        List<Image> images = files.stream()
              .map(this::convertToImage)
              .toList();

        return imageService.save(images);
    }

    private Image convertToImage(File file) {
        log.debug("(convertToImage) file: {}", file);

        return Image.from(
                file.getUrl(),
                file.getContentType(),
                file.getName(),
                file.getBucketName());

    }
}
