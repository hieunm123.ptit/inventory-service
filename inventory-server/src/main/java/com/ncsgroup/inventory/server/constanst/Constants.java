package com.ncsgroup.inventory.server.constanst;

public class Constants {
  private Constants() {
  }

  public static class Validate {
    private Validate() {
    }
    public static final String INVALID_NAME = "Invalid name";

  }

  public static class AuditorConstant {
    private AuditorConstant() {
    }

    public static final String SYSTEM = "SYSTEM";
  }

  public static class StatusExceptions {
    private StatusExceptions() {
    }

    public static final Integer NOT_FOUND = 404;
    public static final Integer CONFLICT = 409;
    public static final Integer BAD_REQUEST = 400;
    public static final Integer INTERNAL_SERVER_ERROR = 500;
  }

  public static class AuthConstant {
    private AuthConstant(){

    }

    public static final String TYPE_TOKEN = "Bear ";
    public static final String AUTHORIZATION = "Authorization";
  }

  public static class MessageController {

    private MessageController(){
    }
    public static final String CREATE_CATEGORY_SUCCESS = "com.ncsgroup.inventory.server.controller.CategoryController.create";

    public static final String DETAIL_CATEGORY_SUCCESS = "com.ncsgroup.inventory.server.controller.CategoryController.detail";

    public static final String DELETE_CATEGORY_SUCCESS = "com.ncsgroup.inventory.server.controller.CategoryController.delete";

    public static final String UPDATE_CATEGORY_SUCCESS = "com.ncsgroup.inventory.server.controller.CategoryController.update";

    public static final String CREATE_ATTRIBUTE_SUCCESS = "com.ncsgroup.inventory.server.controller.AttributeController.create";

    public static final String UPDATE_ATTRIBUTE_SUCCESS = "com.ncsgroup.inventory.server.controller.AttributeController.update";

    public static final String UPDATE_PRODUCT_SUCCESS = "com.ncsgroup.inventory.server.controller.ProductController.update";
    public static final String CREATE_PROMOTION_SUCCESS = "com.ncsgroup.inventory.server.controller.PromotionController.create";

    public static final String UPDATE_PROMOTION_SUCCESS = "com.ncsgroup.inventory.server.controller.PromotionController.update";

  }

  public static class CommonConstants {
    private CommonConstants(){
    }
    public static final String ENCODING_UTF_8 = "UTF-8";
    public static final String LANGUAGE = "Accept-Language";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String BASE_NAME_MESSAGE = "classpath:i18n/messages";
    public static final String SUCCESS = "com.ncsgroup.inventory.server.constanst.Constants.CommonConstants";

  }


  public static class MessageCode {
    private MessageCode(){}

    public static final String CREATE_WAREHOUSE_SUCCESS = "com.ncsgroup.inventory.server.controller.WarehouseController.create";
    public static final String UPDATE_WAREHOUSE_SUCCESS = "com.ncsgroup.inventory.server.controller.WarehouseController.update";

    public static final String CREATE_PRODUCT_SUCCESS = "com.ncsgroup.inventory.server.controller.ProductController.create";

  }

  public static class VariableConstants {
    private VariableConstants(){}
    public static final String TYPE_CLIENT = "0";
    public static final Integer CLIENT = 0;
    public static final String SIZE_DEFAULT = "10";
    public static final String PAGE_DEFAULT = "0";
    public static final String IS_ALL_DEFAULT = "0";

    public static final Integer PROMO_TYPE_DISCOUNT = 0;
  }

  public static class RedisConstants {
    private RedisConstants(){}
    public static final String CART_PRODUCT_FIELD_PREFIX = "product_";
    public static final String CART_PRODUCT_ITEM_FIELD_PREFIX = "product_item_";
    public static final int CART_TIME_OUT = 30;
  }

}
