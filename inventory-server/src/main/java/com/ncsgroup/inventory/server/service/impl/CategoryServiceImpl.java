package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.client.dto.CategoryCreateRequest;
import com.ncsgroup.inventory.client.dto.CategoryUpdateRequest;
import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.dto.response.category.CategoryResponse;
import com.ncsgroup.inventory.server.entity.Category;
import com.ncsgroup.inventory.server.exception.category.CategoryAlreadyExistException;
import com.ncsgroup.inventory.server.exception.category.CategoryDuplicateNameException;
import com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException;
import com.ncsgroup.inventory.server.repository.CategoryRepository;
import com.ncsgroup.inventory.server.service.CategoryService;
import com.ncsgroup.inventory.server.service.base.impl.BaseServiceImpl;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class CategoryServiceImpl extends BaseServiceImpl<Category> implements CategoryService {
    private final CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @Transactional
    public CategoryResponse create(CategoryCreateRequest request) {
        log.info("(create) request: {}", request);
        if (request.getCategoryRequests() == null) {
            request.setCategoryRequests(new ArrayList<>());
        }

        this.checkAlreadyExist(request);

        List<Category> categories = new ArrayList<>();
        String id = UUID.randomUUID().toString();
        Category category = Category.from(
                id,
                request.getName(),
                request.getImageId(),
                request.getIsActive()
        );

        categories.add(category);

        if (!request.getCategoryRequests().isEmpty()) {

            List<Category> categoryList = request.getCategoryRequests().stream().map(categoryCreateRequest -> Category.from(
                            categoryCreateRequest.getName(),
                            categoryCreateRequest.getImageId(),
                            categoryCreateRequest.getIsActive(),
                            id))
                    .toList();

            categories.addAll(categoryList);

        }

        repository.saveAll(categories);
        return this.categoryResponse(categories, id);
    }

    @Override
    public CategoryResponse detail(String id) {
        log.info("(detail) id: {}", id);
        Category category = checkNotFound(id);

        CategoryResponse categoryResponse = this.toCategory(category);

        if (category.getParentId() != null) {
            CategoryResponse response = toCategory(repository.getCategory(category.getParentId()));

            List<CategoryResponse> categoryResponses = new ArrayList<>();
            categoryResponses.add(categoryResponse);

            response.setCategories(categoryResponses);

            return response;
        }

        List<CategoryResponse> categories = this.toCategoryList(repository.getCategoryByParentId(id));

        categoryResponse.setCategories(categories);
        return categoryResponse;
    }

    // request co nhung id trong
    @Override
    @Transactional
    public CategoryResponse update(String id, CategoryUpdateRequest request) {
        log.info("(update) id: {}, request: {}", id, request);

        List<Category> categories = new ArrayList<>();
        List<Category> categoryParents = new ArrayList<>();

        this.checkAlreadyExistUpdate(request);

        Category category = checkNotFound(id);

        this.checkAlreadyExistUpdate(request, category);
        this.updateParentCategory(request, category);

        categories.add(category);

        List<CategoryUpdateRequest> categoryUpdateRequests = request.getCategoryRequests();

        for (CategoryUpdateRequest categoryUpdateRequest : categoryUpdateRequests) {

            if (categoryUpdateRequest.getId() == null) {

                this.checkAlreadyExistUpdate(categoryUpdateRequest.getName());
                Category category1 = Category.from(
                        categoryUpdateRequest.getName(),
                        categoryUpdateRequest.getImageId(),
                        categoryUpdateRequest.getIsActive(),
                        id
                );
                categories.add(category1);
                categoryParents.add(category1);

            } else {

                Category category1 = checkNotFound(categoryUpdateRequest.getId());
                this.checkAlreadyExistUpdate(categoryUpdateRequest, category1);
                this.updateParentCategory(categoryUpdateRequest, category1);
                categories.add(category1);
                categoryParents.add(category1);
            }
        }

        List<String> listId = categoryUpdateRequests.stream()
                .map(CategoryUpdateRequest::getId)
                .filter(getId -> getId != null && !getId.isEmpty())
                .toList();

        repository.deleteSoft(findDuplicateElement(listId, repository.getCategoryIdByParentId(id)));
        repository.saveAll(categories);

        CategoryResponse categoryResponse = toCategory(category);
        categoryResponse.setCategories(toCategoryList(categoryParents));

        return categoryResponse;
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        log.info("(deleteById) id: {}", id);

        Category category = checkNotFound(id);

        if (category.getParentId() == null) {
            List<Category> categories = repository.getCategoryByParentId(category.getId());
            categories.add(category);

            List<String> categoryIds = categories.stream().map(Category::getId).toList();
            repository.deleteSoft(categoryIds);

        } else {
            repository.deleteById(category.getId());
        }
    }

    @Override
    public List<CategoryResponse> getAll() {
        log.info("(getAll)");

        List<CategoryResponse> categoryResponses = new ArrayList<>();
        List<Category> categories = repository.getAllCategoryParent();

        for (Category category : categories) {
            List<Category> categoryList = repository.getCategoryByParentId(category.getId());
            CategoryResponse categoryResponse = toCategory(category);

            categoryResponse.setCategories(toCategoryList(categoryList));
            categoryResponses.add(categoryResponse);
        }
        return categoryResponses;
    }

    @Override
    public void checkCategoryIdsExist(List<String> categoryIds) {
        log.debug("(checkCategoryIdsExist) categoryIds:{}", categoryIds);

        if (repository.countMatching(categoryIds) != categoryIds.size()) {
            log.error("(checkCategoryIdsExist)=====> CategoryNotFoundException");
            throw new CategoryNotFoundException();
        }

    }

    @Override
    public List<CategoryProductResponse> getListById(List<String> categoryIds) {
        log.info("(getListById) categoryIds:{}", categoryIds);

        return repository.find(categoryIds);
    }

    private void checkAlreadyExistUpdate(String name) {
        log.error("(checkAlreadyExistUpdate) =====> CategoryAlreadyExistException()");
        if (Boolean.TRUE.equals(repository.existsByName(name)))
            throw new CategoryAlreadyExistException();
    }

    private void checkAlreadyExistUpdate(CategoryUpdateRequest request, Category category) {

        if (Objects.nonNull(request.getName())
                && !request.getName().equals(category.getName())
                && Boolean.TRUE.equals(repository.existsByName(request.getName()))) {
            log.error("(checkAlreadyExistUpdate) ======> CategoryAlreadyExistException");
            throw new CategoryAlreadyExistException();
        }
    }

    private List<String> findDuplicateElement(List<String> idRequests, List<String> ids) {
        log.debug("(findDuplicateElement) idRequests: {}, ids: {}", idRequests, ids);
        List<String> differentElements = new ArrayList<>();

        for (String element : idRequests) {
            if (!ids.contains(element)) {
                differentElements.add(element);
            }
        }

        for (String element : ids) {
            if (!idRequests.contains(element)) {
                differentElements.add(element);
            }
        }

        return differentElements;

    }

    private Category checkNotFound(String id) {
        Category category = repository.findById(id).orElseThrow(CategoryNotFoundException::new);

        if (Boolean.TRUE.equals(category.getIsDeleted())) {
            log.error("(CategoryNotFoundException) =====>");
            throw new CategoryNotFoundException();
        }
        return category;
    }

    private void updateParentCategory(CategoryUpdateRequest request, Category category) {
        category.setName(request.getName());
        category.setImageId(request.getImageId());
        category.setIsActive(request.getIsActive());
    }

    private void checkAlreadyExistUpdate(CategoryUpdateRequest request) {
        List<String> listName = new ArrayList<>();
        listName.add(request.getName());

        if (!request.getCategoryRequests().isEmpty()) {
            for (CategoryUpdateRequest categoryUpdateRequest : request.getCategoryRequests()) {
                listName.add(categoryUpdateRequest.getName());
            }
        }

        this.checkDuplicate(listName);
    }


    private void checkAlreadyExist(CategoryCreateRequest request) {
        List<String> listName = new ArrayList<>();
        listName.add(request.getName());

        if (!request.getCategoryRequests().isEmpty()) {
            for (CategoryCreateRequest categoryCreateRequest : request.getCategoryRequests()) {
                listName.add(categoryCreateRequest.getName());
            }
        }

        this.checkDuplicate(listName);

        this.checkException(listName);

    }

    private void checkDuplicate(List<String> list) {
        Set<String> set = new HashSet<>();

        if (list.stream().anyMatch(e -> !set.add(e))) {
            log.error("(checkDuplicate)");

            throw new CategoryDuplicateNameException();
        }

    }

    private void checkException(List<String> list) {

        if (repository.totalCategory(list) > 0) {
            log.error("(checkAlreadyExist)");
            throw new CategoryAlreadyExistException();
        }

    }


    private CategoryResponse toCategory(Category category) {
        return CategoryResponse.from(category.getId(), category.getName(), category.getImageId(), category.getIsActive(), category.getParentId());
    }

    private List<CategoryResponse> toCategoryList(List<Category> categories) {
        return categories.stream().map(category -> CategoryResponse.from(
                category.getId(),
                category.getName(),
                category.getImageId(),
                category.getIsActive(),
                category.getParentId()
        )).toList();

    }

    private CategoryResponse categoryResponse(List<Category> categories, String id) {
        log.debug("(categoryResponse) categories: {}", categories);

        CategoryResponse response = new CategoryResponse();
        List<CategoryResponse> categoryResponses = new ArrayList<>();

        for (Category category : categories) {
            if (category.getId().equals(id)) {

                response.setId(id);
                response.setName(category.getName());
                response.setImageId(category.getImageId());
                response.setIsActive(category.getIsActive());
                response.setParenId(category.getParentId());
            } else {

                CategoryResponse categoryResponse = this.toCategory(category);
                categoryResponses.add(categoryResponse);
            }
        }

        response.setCategories(categoryResponses);
        return response;
    }
}
