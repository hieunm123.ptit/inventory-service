package com.ncsgroup.inventory.server.exception.product;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class ProductAlreadyExistException extends ConflictException {
  public ProductAlreadyExistException(){
    setCode("com.ncsgroup.inventory.server.exception.product.ProductAlreadyExistException");
  }
}
