package com.ncsgroup.inventory.server.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
        basePackages = "com.ncsgroup.inventory.server.repository"
)
public class JpaRepositoryConfiguration {
}
