package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.PromotionRequest;
import com.ncsgroup.inventory.client.dto.PromotionSearchRequest;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;
import com.ncsgroup.inventory.server.entity.Promotion;

import com.ncsgroup.inventory.server.service.base.BaseService;

import java.util.List;

public interface PromotionService extends BaseService<Promotion> {
  PromotionResponse create(PromotionRequest request);

  PromotionResponse update(String id, PromotionRequest request);

  void deleteById(String id);

  void changeActive(String id);


  void checkPromotionsExist(List<String> promotionIds);

  PageResponse<PromotionResponse> list(PromotionSearchRequest request, int page, int size, boolean isAll);



}
