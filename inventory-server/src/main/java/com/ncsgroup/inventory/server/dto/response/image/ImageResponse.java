package com.ncsgroup.inventory.server.dto.response.image;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class ImageResponse {
  private String id;
  private String url;
  private String contentType;
  private String name;
  private String bucketName;
  private String productId;

}
