package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.CartItemRequest;

import com.ncsgroup.inventory.client.dto.product.ProductCartDeletionRequest;

import com.ncsgroup.inventory.server.service.base.BaseRedisService;

import java.util.List;
import java.util.Map;

public interface CartRedisService extends BaseRedisService {
  void addProductToCart(String customerId, List<CartItemRequest> items);

  void updateProductInCart(String customerId, List<CartItemRequest> items);

  void deleteProductInCart(String customerId, ProductCartDeletionRequest request);

  Map<String, Integer> getProductFromCart(String customerId);

}
