package com.ncsgroup.inventory.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/test")
public class MainController {

    @GetMapping
    public String test() {
        return "Hello from spring CI/CD with gitlab";
    }
}
