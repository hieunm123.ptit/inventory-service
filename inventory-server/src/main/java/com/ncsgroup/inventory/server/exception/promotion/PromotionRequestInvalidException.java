package com.ncsgroup.inventory.server.exception.promotion;

import com.ncsgroup.inventory.server.exception.base.BadRequestException;

public class PromotionRequestInvalidException extends BadRequestException {
  public PromotionRequestInvalidException(){
    setCode("com.ncsgroup.inventory.server.exception.promotion.PromotionRequestInvalidException");
  }
}
