package com.ncsgroup.inventory.server.configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.BASE_NAME_MESSAGE;
import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.ENCODING_UTF_8;

@Configuration
public class MessageSourceConfiguration {

  @Bean
  public MessageSource messageSource() {
    var messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename(BASE_NAME_MESSAGE);
    messageSource.setDefaultEncoding(ENCODING_UTF_8);
    return messageSource;
  }

}
