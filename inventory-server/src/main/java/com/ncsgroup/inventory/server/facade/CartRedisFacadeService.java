package com.ncsgroup.inventory.server.facade;


import com.ncsgroup.inventory.client.dto.product.ProductCartDeletionRequest;
import com.ncsgroup.inventory.client.dto.product.ProductCartRequest;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;

import java.util.List;


public interface CartRedisFacadeService {
  void addProductToCart(String customerId, ProductCartRequest request);

  void updateProductInCart(String customerId, ProductCartRequest request);

  void deleteProductInCart(String customerId, ProductCartDeletionRequest request);

  List<CartItemResponse> getProductFromCart(String customerId);
}






