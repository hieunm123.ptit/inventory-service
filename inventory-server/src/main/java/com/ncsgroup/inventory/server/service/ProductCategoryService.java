package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;

import java.util.List;

public interface ProductCategoryService {
  void save(String productId, List<String>  categoryId);
  void update(String productId, List<String>  categoryId);
  List<CategoryProductResponse> listByProductId(String productId);

}
