package com.ncsgroup.inventory.server.entity.product.embed;

import com.ncsgroup.inventory.server.entity.product.embed.key.ProductCategoryId;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "product_category")
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class ProductCategory {
  @EmbeddedId
  private ProductCategoryId id;

  public static ProductCategory from(
        String productId,
        String categoryId
  ) {
    return ProductCategory.of(
          ProductCategoryId.of(
                productId,
                categoryId
          )
    );
  }
}
