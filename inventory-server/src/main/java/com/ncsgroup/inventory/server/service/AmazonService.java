package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.File;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AmazonService {
  List<File> uploadFile(List<MultipartFile> file);

  String generatePreSignedUrl(String fileName);
}
