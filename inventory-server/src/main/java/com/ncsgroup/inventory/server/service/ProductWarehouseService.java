package com.ncsgroup.inventory.server.service;


import com.ncsgroup.inventory.client.dto.product.ProductAdminFilterRequest;

import com.ncsgroup.inventory.client.dto.product.ProductWarehouseRequest;


import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductWarehouseResponse;




import java.util.List;

public interface ProductWarehouseService {
  void save(String productId, List<ProductWarehouseRequest> productWarehouseRequests);


  void update(String productId, List<ProductWarehouseRequest> productWarehouseRequests);

  List<ProductWarehouseResponse> listByProductId(String productId);


  List<ProductClientResponse> filterInClientSide(List<ProductClientResponse> productClientResponses);

  List<ProductWarehouseResponse> filterInAdminSide(String productId, ProductAdminFilterRequest request);



}
