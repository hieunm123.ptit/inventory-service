package com.ncsgroup.inventory.server.facade.impl;

import com.ncsgroup.inventory.client.dto.product.*;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductResponse;
import com.ncsgroup.inventory.server.entity.Image;
import com.ncsgroup.inventory.server.entity.product.ProductAttribute;
import com.ncsgroup.inventory.server.facade.ClassificationFacadeService;
import com.ncsgroup.inventory.server.facade.ProductFacadeService;
import com.ncsgroup.inventory.server.facade.ProductWarehouseFacadeService;
import com.ncsgroup.inventory.server.service.*;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


import java.util.*;

@Slf4j
@RequiredArgsConstructor
public class ProductFacadeServiceImpl implements ProductFacadeService {
  private final ProductService productService;
  private final CategoryService categoryService;
  private final WarehouseService warehouseService;
  private final AttributeService attributeService;
  private final PromotionService promotionService;
  private final ProductCategoryService productCategoryService;
  private final ProductAttributeService productAttributeService;
  private final ProductWarehouseService productWarehouseService;
  private final ClassificationFacadeService classificationFacadeService;
  private final ProductWarehouseFacadeService productWarehouseFacadeService;
  private final ProductPromotionService productPromotionService;
  private final ImageService imageService;
  private final AmazonService amazonService;

  @Override
  @Transactional
  public ProductResponse createProduct(ProductRequest request) {
    log.info("(createProduct) request:{}", request);

    this.checkComponentExists(request);
    ProductResponse response = productService.create(request);

    this.addRelationship(response.getId(), request);
    this.addProperties(response, request);

    if (Objects.nonNull(request.getImageIds()) && !request.getImageIds().isEmpty()) {
      imageService.addImageToProduct(request.getImageIds(), response.getId());
      List<Image> images = imageService.getAllByProduct(response.getId());

      response.setImageResponses(this.convertToImage(images));
    }

    return response;
  }

  @Override
  public ProductResponse detailProduct(String id) {
    log.info("(detailProduct) id:{}", id);

    ProductResponse response = productService.detail(id);
    response.setCategoryResponses(productCategoryService.listByProductId(id));
    response.setAttributes(attributeService.getByProductId(id));
    response.setClassifications(classificationFacadeService.getClassifications(id));
    response.setProductWarehouseResponses(productWarehouseService.listByProductId(id));
    response.setPromotions(productPromotionService.listByProductId(id));
    response.setImageResponses(this.convertToImage(imageService.getAllByProduct(id)));

    return response;
  }

  @Override
  @Transactional
  public ProductResponse update(String id, ProductRequest request) {
    log.info("(update) id: {}, request: {}", id, request);

    ProductResponse response = productService.update(id, request);
    this.checkComponentExists(request);

    productCategoryService.update(id, request.getCategoryId());
    productWarehouseService.update(id, request.getWarehouses());
    productAttributeService.update(id, request.getAttributes());
    classificationFacadeService.updateClassifications(id, request.getClassifications());

    imageService.updateImage(
          imageService.findId(id),
          request.getImageIds()
    );

    imageService.addImageToProduct(request.getImageIds(), id);
    List<Image> images = imageService.getAllByProduct(response.getId());
    response.setImageResponses(convertToImage(images));

    this.addProperties(response, request);
    return response;
  }

  @Override
  public PageResponse<ProductClientResponse> listInClientSide(int page, int size) {
    log.info("(listInClientSide) page:{}, size:{}", page, size);

    PageResponse<ProductClientResponse> pageResponse = productService.listInClientSide(page, size);
    this.addPropertiesInClientSide(pageResponse.getContent());

    return pageResponse;
  }

  public PageResponse<ProductClientResponse> filterInClientSide(ProductFilterRequest request, int page, int size) {
    log.info("(filterInClientSide) request :{}, page :{}, size :{}", request, page, size);

    PageResponse<ProductClientResponse> pageResponse = productService.filterInClientSide(request, page, size);
    List<ProductClientResponse> productClientResponses = productWarehouseService.filterInClientSide(pageResponse.getContent());
    this.addPropertiesInClientSide(productClientResponses);

    return PageResponse.of(productClientResponses, pageResponse.getAmount());
  }

  @Override
  public PageResponse<ProductAdminResponse> listInAdminSide(int page, int size) {
    log.info("(listInAdminSide) page:{}, size:{}", page, size);

    PageResponse<ProductAdminResponse> response = productService.listInAdminSide(page, size);
    this.addProperties(response.getContent());

    return response;
  }

  @Override
  public PageResponse<ProductAdminResponse> filterInAdminSide(ProductAdminFilterRequest request, int page, int size) {
    log.info("(filterInAdminSide) request:{}, page:{}, size:{}", request, page, size);

    PageResponse<ProductAdminResponse> response = productService.filterInAdminSide(request, page, size);
    this.addProperties(response.getContent(), request);

    return response;
  }

  @Override
  public void addPromotionToProduct(String productId, ProductPromotionRequest request) {
    log.info("(addPromotionToProduct) productId:{}, request:{}", productId, request);

    productService.checkProductExist(productId);
    promotionService.checkPromotionsExist(request.getPromotionIds());
    productPromotionService.save(productId, request.getPromotionIds());

  }


  private void checkComponentExists(ProductRequest request) {
    log.debug("(checkComponentExists) request:{}", request);

    if (Objects.nonNull(request.getCategoryId())) {
      categoryService.checkCategoryIdsExist(request.getCategoryId());
    }

    if (Objects.nonNull(request.getWarehouses())) {
      warehouseService.checkWarehousesExist(this.getWarehouseIds(request));
    }

    if (Objects.nonNull(request.getAttributes())) {
      attributeService.checkAttributeExists(this.getAttributesIds(request));
    }
  }


  private List<String> getWarehouseIds(ProductRequest request) {
    log.debug("(getWarehouseIds) request: " + request);

    Set<String> ids = new HashSet<>();
    for (ProductWarehouseRequest productWarehouse : request.getWarehouses()) {
      ids.add(productWarehouse.getWarehouseId());
    }
    return ids.stream().toList();

  }

  private List<String> getAttributesIds(ProductRequest request) {
    log.debug("(getAttributesIds) request:{}", request);

    Set<String> ids = new HashSet<>();
    for (AttributeProductRequest attribute : request.getAttributes()) {
      ids.add(attribute.getAttributeId());
    }
    if (Objects.nonNull(request.getClassifications())) {
      for (ClassificationRequest classification : request.getClassifications()) {
        ids.add(classification.getAttributeId());
        if (Objects.nonNull(classification.getSubClassifications())) {
          for (ClassificationRequest subClassification : classification.getSubClassifications()) {
            ids.add(subClassification.getAttributeId());
          }
        }
      }
    }
    return ids.stream().toList();

  }

  private void addRelationship(String productId, ProductRequest request) {
    log.info("(addRelationship) productId:{}, request:{}", productId, request);

    if (Objects.nonNull(request.getCategoryId())) {
      productCategoryService.save(productId, request.getCategoryId());
    }

    if (Objects.nonNull(request.getAttributes())) {
      List<ProductAttribute> productAttributes = new ArrayList<>();
      for (AttributeProductRequest attributeProduct : request.getAttributes()) {
        productAttributes.add(ProductAttribute.from(
              productId,
              attributeProduct.getAttributeId(),
              attributeProduct.getValue()));
      }
      productAttributeService.save(productAttributes);
    }

    if (Objects.nonNull(request.getClassifications())) {
      classificationFacadeService.createClassifications(
            productId,
            request.getClassifications()
      );
    }
    if (Objects.nonNull(request.getWarehouses())) {
      productWarehouseFacadeService.saveProductWarehouseService(
            productId,
            request.getWarehouses(),
            request.getClassifications());
    }
  }


  private void addProperties(ProductResponse response, ProductRequest request) {
    log.info("(addProperties) response:{}, request:{}", response, request);

    response.setCategoryResponses(
          categoryService.getListById(request.getCategoryId()));

    response.setAttributes(
          attributeService.getByProductId(response.getId())
    );

    response.setClassifications(
          classificationFacadeService.getClassifications(response.getId())
    );

    response.setProductWarehouseResponses(
          productWarehouseService.listByProductId(response.getId())
    );
  }

  private void addProperties(List<ProductAdminResponse> productAdminResponses) {
    log.info("(addProperties) productAdminResponses:{}", productAdminResponses);

    for (ProductAdminResponse productAdminResponse : productAdminResponses) {
      productAdminResponse.setCategories(productCategoryService.listByProductId(productAdminResponse.getId()));
      productAdminResponse.setWarehouses(productWarehouseService.listByProductId(productAdminResponse.getId()));
      productAdminResponse.setPromotions(productPromotionService.listByProductIdInAdminSide(productAdminResponse.getId()));
      if (Objects.nonNull(imageService.getImage(productAdminResponse.getId()))) {
        productAdminResponse.setImages(this.convertToImageResponse(imageService.getImage(productAdminResponse.getId())));
      }
    }
  }

  private void addProperties(List<ProductAdminResponse> productAdminResponses, ProductAdminFilterRequest request) {
    log.info("(addProperties) productAdminResponses:{}, request:{}", productAdminResponses, request);

    for (ProductAdminResponse productAdminResponse : productAdminResponses) {
      productAdminResponse.setCategories(productCategoryService.listByProductId(productAdminResponse.getId()));
      productAdminResponse.setWarehouses(productWarehouseService.filterInAdminSide(
            productAdminResponse.getId(),
            request
      ));

      if (Objects.nonNull(imageService.getImage(productAdminResponse.getId()))) {
        productAdminResponse.setImages(this.convertToImageResponse(imageService.getImage(productAdminResponse.getId())));
      }

      productAdminResponse.setPromotions(productPromotionService.listByProductIdInAdminSide(productAdminResponse.getId()));
    }
  }


  private List<ImageResponse> convertToImage(List<Image> images) {
    log.debug("(convertToImage) images: {}", images);

    return images.stream()
          .map(image -> ImageResponse.of(
                image.getId(),
                amazonService.generatePreSignedUrl(image.getName()),
                image.getContentType(),
                image.getName(),
                image.getBucketName(),
                image.getProductId()
          )).toList();
  }

  private ImageResponse convertToImageResponse(Image image) {
    return ImageResponse.of(
          image.getId(),
          image.getUrl(),
          image.getContentType(),
          image.getName(),
          image.getBucketName(),
          image.getProductId()
    );
  }

  private void addPropertiesInClientSide(List<ProductClientResponse> productClientResponses) {
    log.debug("(addPropertiesInClientSide) productClientResponses:{}", productClientResponses);

    for (ProductClientResponse response : productClientResponses) {
      response.setPromotions(productPromotionService.listByProductIdInClientSide(response.getId()));
      if (Objects.nonNull(imageService.getImage(response.getId()))) {
        response.setImages(convertToImageResponse(imageService.getImage(response.getId())));
      }
    }
  }


}
