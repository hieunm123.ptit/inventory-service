package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.client.dto.PromotionRequest;
import com.ncsgroup.inventory.client.dto.PromotionSearchRequest;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.Promotion;
import com.ncsgroup.inventory.server.exception.promotion.PromotionNotFoundException;
import com.ncsgroup.inventory.server.exception.promotion.PromotionRequestInvalidException;
import com.ncsgroup.inventory.server.repository.PromotionRepository;
import com.ncsgroup.inventory.server.service.PromotionService;
import com.ncsgroup.inventory.server.service.base.impl.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Objects;

import static com.ncsgroup.inventory.server.constanst.Constants.VariableConstants.PROMO_TYPE_DISCOUNT;
import static com.ncsgroup.inventory.server.utils.DateUtils.convertToDateString;
import static com.ncsgroup.inventory.server.utils.DateUtils.convertToTimestamp;


@Slf4j
public class PromotionServiceImpl extends BaseServiceImpl<Promotion> implements PromotionService {
  private final PromotionRepository repository;

  public PromotionServiceImpl(PromotionRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public PromotionResponse create(PromotionRequest request) {
    log.info("(create) request:{}", request);

    this.checkValidRequest(request);
    Promotion promotion = migrateToEntity(request);
    repository.saveAndFlush(promotion);

    return repository.getResponseById(promotion.getId());
  }

  @Override
  public PromotionResponse update(String id, PromotionRequest request) {
    log.info("(update) id:{}, request:{}", id, request);

    this.checkValidRequest(request);
    Promotion promotion = find(id);
    this.updateProperties(promotion, request);
    update(promotion);

    return migrateToResponse(promotion);
  }

  @Override
  public void deleteById(String id) {
    log.info("(delete) id:{}", id);

    this.checkPromotionExists(id);
    repository.deleteById(id);

  }

  @Override
  public void changeActive(String id) {
    log.info("(changeActive) id:{}", id);

    this.checkPromotionExists(id);
    repository.changeActive(id);
  }

  @Override

  public void checkPromotionsExist(List<String> promotionIds) {
    log.info("(changePromotionsExist) promotionIds:{}", promotionIds);

    if (repository.countMatching(promotionIds) != promotionIds.size()) {
      log.error("(checkPromotionsExist) ========> PromotionNotFoundException");
      throw new PromotionNotFoundException();
    }
  }

  public PageResponse<PromotionResponse> list(PromotionSearchRequest request, int page, int size, boolean isAll) {
    log.info("(list) request:{}, page:{}, size:{}, isAll:{}", request, page, size, isAll);
    Page<PromotionResponse> pageResponse = null;
    List<PromotionResponse> promotionResponse;
    if (isAll) {
      promotionResponse = repository.getAll(
            request.getCode(),
            convertToTimestamp(request.getFromStartDate()),
            convertToTimestamp(request.getToStartDate()),
            convertToTimestamp(request.getFromEndDate()),
            convertToTimestamp(request.getToEndDate()),
            request.getType(),
            request.getDiscountPercent(),
            request.getMoneyDeducted(),
            Active.valueOf(request.getIsActive())
      );
    } else {
      pageResponse = repository.filter(
            request.getCode(),
            convertToTimestamp(request.getFromStartDate()),
            convertToTimestamp(request.getToStartDate()),
            convertToTimestamp(request.getFromEndDate()),
            convertToTimestamp(request.getToEndDate()),
            request.getType(),
            request.getDiscountPercent(),
            request.getMoneyDeducted(),
            Active.valueOf(request.getIsActive()),
            PageRequest.of(page, size));
      promotionResponse = pageResponse.getContent();
    }

    return PageResponse.of(promotionResponse, isAll ? promotionResponse.size() : (int) pageResponse.getTotalElements());

  }


  private Promotion migrateToEntity(PromotionRequest request) {
    log.debug("(migrateToEntity) request:{}", request);

    return Promotion.from(
          convertToTimestamp(request.getStartDay()),
          convertToTimestamp(request.getEndDay()),
          request.getType(),
          request.getDiscountPercent(),
          request.getMoneyDeducted(),
          request.getDescription(),
          request.getCoin()
    );
  }

  private void updateProperties(Promotion promotion, PromotionRequest request) {
    log.debug("(updateProperties) promotion:{}, request:{}", promotion, request);

    promotion.setStartDay(convertToTimestamp(request.getStartDay()));
    promotion.setEndDay(convertToTimestamp(request.getEndDay()));
    promotion.setDiscountPercent(request.getDiscountPercent());
    promotion.setMoneyDeducted(request.getMoneyDeducted());
    promotion.setCoin(request.getCoin());

  }

  private PromotionResponse migrateToResponse(Promotion promotion) {
    log.debug("(migrateToEntity) promotion:{}", promotion);

    return PromotionResponse.from(
          promotion.getCode(),
          convertToDateString(promotion.getStartDay()),
          convertToDateString(promotion.getEndDay()),
          promotion.getType(),
          promotion.getDiscountPercent(),
          promotion.getMoneyDeducted(),
          promotion.getDescription(),
          promotion.getCoin(),
          promotion.getIsActive()
    );

  }

  private void checkPromotionExists(String id) {
    log.debug("(checkPromotionExists) id:{}", id);

    if (!repository.existsById(id)) {
      log.debug("(checkPromotionExists)========>PromotionNotFoundException");

      throw new PromotionNotFoundException();
    }
  }

  private Promotion find(String id) {
    log.debug("(find) id:{}", id);

    Promotion promotion = repository.find(id);
    if (!Objects.nonNull(promotion)) {
      throw new PromotionNotFoundException();
    }

    return promotion;
  }

  private void checkValidRequest(PromotionRequest request) {
    log.debug("(checkValidRequest)  request:{} ", request);
    if (request.getType().equals(PROMO_TYPE_DISCOUNT)) {

      if (Objects.isNull(request.getDiscountPercent()) && Objects.isNull(request.getMoneyDeducted())) {
        log.error("(checkValidRequest) =======> PromotionRequestInvalidException");

        throw new PromotionRequestInvalidException();
      }

    } else {

      if (Objects.isNull(request.getDescription())) {
        log.error("(checkValidRequest) =======> PromotionRequestInvalidException");

        throw new PromotionRequestInvalidException();

      }
    }
  }

}

