package com.ncsgroup.inventory.server.exception.product;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class ProductDuplicateCodeException extends ConflictException {
  public ProductDuplicateCodeException() {
    setCode("com.ncsgroup.inventory.server.exception.product.ProductDuplicateCodeException");
  }
}
