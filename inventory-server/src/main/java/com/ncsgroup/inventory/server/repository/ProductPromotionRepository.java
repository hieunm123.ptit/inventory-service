package com.ncsgroup.inventory.server.repository;


import com.ncsgroup.inventory.server.dto.response.product.ProductPromotionResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;
import com.ncsgroup.inventory.server.entity.product.embed.ProductPromotion;
import com.ncsgroup.inventory.server.entity.product.embed.key.ProductPromotionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductPromotionRepository extends JpaRepository<ProductPromotion, ProductPromotionId> {

  @Query("""
        SELECT EXISTS(SELECT 1 FROM ProductPromotion p
        WHERE p.id.productId =:productId AND p.id.promotionId IN :promotionId)
        """)
  boolean existsByKey(String productId, List<String> promotionId);

  @Query("""
        SELECT NEW com.ncsgroup.inventory.server.dto.response.product.ProductPromotionResponse(
        pr.discountPercent, pr.moneyDeducted, pr.description, pr.isActive)
        From ProductPromotion pp
        Left JOIN Promotion pr ON pr.id = pp.id.promotionId
        WHERE pp.id.productId = :productId
        AND pr.isDeleted = false
        AND pr.isActive = 0
                
        """)
  List<ProductPromotionResponse> listByProductIdInClientSide(String productId);

  @Query("""
        SELECT NEW com.ncsgroup.inventory.server.dto.response.product.ProductPromotionResponse(
        pr.discountPercent, pr.moneyDeducted, pr.description, pr.isActive)
        From ProductPromotion pp
        Left JOIN Promotion pr ON pr.id = pp.id.promotionId
        WHERE pp.id.productId = :productId
        AND pr.isDeleted = false
            
        """)
  List<ProductPromotionResponse> listByProductIdInAdminSide(String productId);

  @Query("""
        SELECT NEW com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse(
        pr.code, pr.startDay, pr.endDay, pr.discountPercent, pr.moneyDeducted,pr.description, pr.coin, pr.isActive)
        From ProductPromotion pp
        Left JOIN Promotion pr ON pr.id = pp.id.promotionId
        WHERE pp.id.productId = :productId
        AND pr.isDeleted = false
        AND pr.isActive = 0
                
        """)
  List<PromotionResponse> listByProductId(String productId);

}
