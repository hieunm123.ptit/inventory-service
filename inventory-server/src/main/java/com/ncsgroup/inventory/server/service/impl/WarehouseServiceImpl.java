package com.ncsgroup.inventory.server.service.impl;


import com.ncsgroup.inventory.client.dto.WarehouseRequest;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehousePageResponse;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.Warehouse;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseAlreadyExistException;
import com.ncsgroup.inventory.server.exception.warehouse.WarehouseNotFoundException;
import com.ncsgroup.inventory.server.repository.WarehouseRepository;
import com.ncsgroup.inventory.server.service.WarehouseService;
import com.ncsgroup.inventory.server.service.base.impl.BaseServiceImpl;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.List;


@Slf4j
public class WarehouseServiceImpl extends BaseServiceImpl<Warehouse> implements WarehouseService {
  private final WarehouseRepository repository;

  public WarehouseServiceImpl(WarehouseRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  @Transactional
  public WarehouseResponse create(WarehouseRequest request) {
    log.info("(create) request: {}", request);

    this.checkWarehouseAlreadyExists(request.getName());
    this.checkWarehouseAlreadyExistsByPhoneNumber(request.getPhoneNumber());
    Warehouse warehouse = Warehouse.from(
          request.getName(),
          request.getPhoneNumber(),
          request.getAddressId()
    );
    repository.saveAndFlush(warehouse);
    warehouse = repository.getByName(request.getName());

    return WarehouseResponse.of(
          warehouse.getCode(),
          warehouse.getName(),
          warehouse.getPhoneNumber(),
          warehouse.getAddressId()
    );

  }

  @Override
  @Transactional
  public WarehouseResponse update(String id, WarehouseRequest request) {
    log.info("(update) request: {}", request);

    Warehouse warehouse = find(id);
    this.checkWarehouseAlreadyExistsByName(warehouse, request);
    this.checkWarehouseAlreadyExistsByPhoneNumberWhenUpdate(warehouse, request);
    updateFieldWarehouse(warehouse, request);
    warehouse = update(warehouse);

    return WarehouseResponse.of(
          warehouse.getCode(),
          warehouse.getName(),
          warehouse.getPhoneNumber(),
          warehouse.getAddressId()
    );
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    log.info("(deleteById) id :{}", id);

    this.checkWarehouseExistsById(id);
    repository.deleteById(id);

  }

  @Override
  public WarehousePageResponse list(String keyword, int size, int page, boolean isAll) {
    log.info("(list) keyword: {}, size : {}, page: {}, isAll: {}", keyword, size, page, isAll);

    Pageable pageable = PageRequest.of(page, size);

    List<WarehouseResponse> warehouses = isAll ?
          repository.getAll() : repository.search(keyword, pageable);


    return WarehousePageResponse.of(warehouses, isAll ? warehouses.size() : repository.countSearch(keyword));
  }

  @Override
  public void checkExistWarehouse(List<String> ids) {
    log.info("(checkExistWarehouse) ids: {}", ids);

    if (repository.totalWarehouse(ids) < ids.size()) {
      log.error("(WarehouseNotFoundException)");
      throw new WarehouseNotFoundException();
    }
  }


  public void checkWarehousesExist(List<String> warehouseIds) {
    log.info("(checkWarehousesExist) warehouseIds: " + warehouseIds);

    if (repository.countMatching(warehouseIds) != warehouseIds.size()) {
      log.error("(checkWarehousesExist) ==========> WarehouseNotFoundException");
      throw new WarehouseNotFoundException();
    }
  }


  private void checkWarehouseAlreadyExists(String name) {
    log.debug("checkWarehouseAlreadyExists :{}", name);

    if (repository.existsByName(name) && repository.getByName(name) != null) {
      log.error("(checkWarehousesExist) ==========> WarehouseAlreadyExist");

      throw new WarehouseAlreadyExistException();
    }
  }


  private Warehouse find(String id) {
    log.debug("(find) id:{}", id);

    Warehouse warehouse = repository.findById(id).orElseThrow(WarehouseNotFoundException::new);
    if (warehouse.isDeleted()) {
      throw new WarehouseNotFoundException();
    }

    return warehouse;
  }


  private void updateFieldWarehouse(
        Warehouse warehouse,
        WarehouseRequest request
  ) {

    warehouse.setName(request.getName());
    warehouse.setPhoneNumber(request.getPhoneNumber());
    warehouse.setAddressId(request.getAddressId());

  }

  private void checkWarehouseAlreadyExistsByName(Warehouse warehouse, WarehouseRequest request) {
    log.debug("(checkWarehouseAlreadyExistsForUpdate) warehouse:{}, request:{}", warehouse, request);

    if (!warehouse.getName().equals(request.getName())) {
      checkWarehouseAlreadyExists(request.getName());

    }
  }

  private void checkWarehouseExistsById(String id) {
    log.debug("(checkWarehouseExistsById) id :{}", id);

    if (!repository.existsById(id)) {
      log.error("(checkWarehousesExist) ==========> WarehouseNotFoundException");

      throw new WarehouseNotFoundException();
    }
  }

  private void checkWarehouseAlreadyExistsByPhoneNumber(String phoneNumber) {
    log.debug("(checkWarehouseAlreadyExistsByPhoneNumber) phoneNumber:{}", phoneNumber);

    if (repository.existsByPhoneNumber(phoneNumber)) {
      log.error("(checkWarehousesExistByPhoneNumber) ==========> WarehouseAlreadyExist");
      throw new WarehouseAlreadyExistException();
    }
  }

  private void checkWarehouseAlreadyExistsByPhoneNumberWhenUpdate(Warehouse warehouse, WarehouseRequest request) {
    log.debug("(checkWarehouseAlreadyExistsByPhoneNumberWhenUpdate) warehouse:{}, request:{}", warehouse, request);

    if (!warehouse.getPhoneNumber().equals(request.getPhoneNumber())) {
      checkWarehouseAlreadyExistsByPhoneNumber(request.getPhoneNumber());
    }
  }
}
