package com.ncsgroup.inventory.server.exception.product;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class ProductItemCodeNotFoundException extends NotFoundException {
  public ProductItemCodeNotFoundException(){
    setCode("com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException");
  }
}
