package com.ncsgroup.inventory.server.exception.category;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class CategoryDuplicateNameException extends ConflictException {
  public CategoryDuplicateNameException() {
    setCode("com.ncsgroup.inventory.server.exception.category.CategoryDuplicateNameException");
  }
}
