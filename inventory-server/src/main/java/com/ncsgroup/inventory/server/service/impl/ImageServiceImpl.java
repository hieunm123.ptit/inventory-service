package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.entity.Image;
import com.ncsgroup.inventory.server.exception.image.ImageNotFoundException;
import com.ncsgroup.inventory.server.repository.ImageRepository;
import com.ncsgroup.inventory.server.service.ImageService;
import com.ncsgroup.inventory.server.utils.ComparatorUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {
  private final ImageRepository repository;

  @Override
  @Transactional
  public List<ImageResponse> save(List<Image> images) {
    log.info("(save) image: {}", images);

    return convertToImage(repository.saveAll(images));
  }


  @Override
  @Transactional
  public void addImageToProduct(List<String> imageIds, String productId) {
    log.info("(addImageToProduct) images: {}, productId: {}", imageIds, productId);

    if (repository.totalImage(imageIds) < imageIds.size()) {
      throw new ImageNotFoundException();
    }

    repository.updateImage(imageIds, productId);
  }

  @Override
  public List<Image> getAllByProduct(String productId) {
    log.info("(getAllByProduct) productId: {}", productId);

    return repository.findImageByProductId(productId);
  }

  @Override
  public void updateImage(List<String> imageProductIds, List<String> imageRequestIds) {
    log.info("(updateImage) imageProductIds: {}, imageRequestIds: {}", imageProductIds, imageRequestIds);

    repository.deleteImage(
          ComparatorUtils.findUniqueElements(imageProductIds, imageRequestIds)
    );
  }

  @Override
  public List<String> findId(String productId) {
    log.info("(findId) productId: {}", productId);

    return repository.findImageIdByProductId(productId);
  }

  @Override
  public Image getImage(String productId) {
    log.info("(getImage) productId: {}", productId);

    return repository.getImageByProductId(productId);
  }


  private List<ImageResponse> convertToImage(List<Image> images) {
    log.debug("(convertToImage) images: {}", images);

    return images.stream()
          .map(image -> ImageResponse.of(
                image.getId(),
                image.getUrl(),
                image.getContentType(),
                image.getName(),
                image.getBucketName(),
                image.getProductId()
          )).toList();
  }

}
