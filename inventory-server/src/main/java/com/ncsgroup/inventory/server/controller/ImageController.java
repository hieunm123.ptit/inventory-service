package com.ncsgroup.inventory.server.controller;

import com.ncsgroup.inventory.server.dto.response.ResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.facade.ImageFacadeService;
import com.ncsgroup.inventory.server.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/images")
public class ImageController {
  private final MessageService messageService;
  private final ImageFacadeService imageFacadeService;

  @PostMapping
  public ResponseGeneral<List<ImageResponse>> upload(
        @RequestParam("files") List<MultipartFile> files,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(upload)");
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          imageFacadeService.uploadImage(files)
    );
  }
}
