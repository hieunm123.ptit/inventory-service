package com.ncsgroup.inventory.server.dto.response.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductAdminResponse {
  private String id;
  private String code;
  private String name;
  private List<CategoryProductResponse> categories;
  private List<ProductWarehouseResponse> warehouses;
  private List<ProductPromotionResponse> promotions;
  private ImageResponse images;
  private Active isActive;

  public ProductAdminResponse(
        String id,
        String code,
        String name,
        Active isActive
  ) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.isActive = isActive;
  }
}
