package com.ncsgroup.inventory.server.dto.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClassificationResponse {
  private String id;
  private String attributeId;
  private String attributeName;

  private String value;
  private boolean isDetail;
  private ProductItemResponse productItem;
  private List<ClassificationResponse> subClassifications;

  public ClassificationResponse(String id, String attributeName, String attributeId, String value,
                                String productItemId, String code, boolean isDetail
  ) {
    this.id = id;
    this.attributeName = attributeName;
    this.attributeId = attributeId;
    this.value = value;
    if (Objects.nonNull(productItemId)) {
      this.productItem = ProductItemResponse.of(productItemId, code);
    } else {
      this.productItem = null;
    }
    this.isDetail = isDetail;
  }
}
