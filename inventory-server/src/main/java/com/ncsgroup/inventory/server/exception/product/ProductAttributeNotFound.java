package com.ncsgroup.inventory.server.exception.product;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class ProductAttributeNotFound extends NotFoundException {
  public ProductAttributeNotFound(){
    setCode("com.ncsgroup.inventory.server.exception.product.ProductAttributeNotFound");
  }
}
