package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.client.dto.product.ProductItemRequest;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductItemResponse;
import com.ncsgroup.inventory.server.entity.product.ProductItem;
import com.ncsgroup.inventory.server.exception.product.ProductItemCodeNotFoundException;
import com.ncsgroup.inventory.server.exception.product.ProductItemDuplicateCodeException;
import com.ncsgroup.inventory.server.repository.ProductItemRepository;
import com.ncsgroup.inventory.server.service.ProductItemService;
import com.ncsgroup.inventory.server.service.base.impl.BaseServiceImpl;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class ProductItemServiceImpl extends BaseServiceImpl<ProductItem> implements ProductItemService {
  private final ProductItemRepository repository;

  public ProductItemServiceImpl(ProductItemRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public ProductItemResponse create(ProductItemRequest request) {
    log.info("(create) request: {}", request);

    this.checkProductItemExists(request.getCode());
    ProductItem productItem = ProductItem.from(request.getCode());
    productItem = create(productItem);

    return ProductItemResponse.of(
          productItem.getId(),
          productItem.getCode()
    );
  }

  @Override
  public void save(List<ProductItem> productItems) {
    log.info("(save) productItems :{}", productItems);
    this.checkProductItemExists(productItems);

    repository.saveAll(productItems);

  }

  @Override
  public ProductItem findProductItem(String productItemId) {
    log.info("(findProductItem) productItemId :{}", productItemId);

    return repository.findById(productItemId).orElse(null);
  }

  @Override
  public void checkProductItemExisted(List<String> productItemIds) {
    log.info("(checkProductItemExisted) productItemIds :{}", productItemIds);

    if (repository.countMatching(productItemIds) != productItemIds.size()) {
      log.error("(checkProductItemExist) =====>ProductItemCodeNotFoundException");
      throw new ProductItemCodeNotFoundException();
    }
  }

  @Override
  public void checkProductItemExisted(String productItemId) {
    log.info("(checkProductItemExisted) productItemIds :{}", productItemId);

    if (!repository.existsById(productItemId)) {
      log.error("(checkProductItemExisted) =======> ProductItemCodeNotFoundException");

      throw new ProductItemCodeNotFoundException();
    }
  }

  @Override
  public List<CartItemResponse> getItems(List<String> itemIds) {
    log.info("(getItems) itemIds: " + itemIds);

    return repository.getItems(itemIds);
  }


  private void checkProductItemExists(List<ProductItem> productItems) {
    log.debug("(checkProductItemExists) productItems: {}", productItems);
    for (ProductItem productItem : productItems) {
      if (repository.existsByCode(productItem.getCode())) {
        throw new ProductItemDuplicateCodeException();
      }
    }
  }

  private void checkProductItemExists(String code) {
    log.debug("(checkProductItemExists) code: {}", code);

    if (repository.existsByCode(code)) {
      throw new ProductItemDuplicateCodeException();
    }
  }
}
