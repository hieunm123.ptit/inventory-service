package com.ncsgroup.inventory.server.exception.cart_item;

import com.ncsgroup.inventory.server.exception.base.BadRequestException;

public class CartDeletionInvalidException extends BadRequestException {
  public CartDeletionInvalidException(){
    setCode("com.ncsgroup.inventory.server.exception.CartItem.CartDeletionInvalidException");
  }
}
