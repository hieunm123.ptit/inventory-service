package com.ncsgroup.inventory.server.exception.category;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class CategoryAlreadyExistException extends ConflictException {
  public CategoryAlreadyExistException() {
    setCode("com.ncsgroup.inventory.server.exception.category.CategoryAlreadyExistException");
  }
}
