package com.ncsgroup.inventory.server.dto.response.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductClientResponse {
  private String id;
  private String name;
  private Long quantitySold;
  private Double exportPrice;
  private List<ProductPromotionResponse> promotions;
  private ImageResponse images;

  public ProductClientResponse(String id, String name) {
    this.id = id;
    this.name = name;
    this.exportPrice = null;
    this.quantitySold = null;

  }

  public ProductClientResponse(String id, String name, Long quantitySold, Double exportPrice) {
    this.id = id;
    this.name = name;
    this.quantitySold = quantitySold;
    this.exportPrice = exportPrice;
  }

}
