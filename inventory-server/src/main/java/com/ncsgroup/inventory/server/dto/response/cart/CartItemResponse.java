package com.ncsgroup.inventory.server.dto.response.cart;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartItemResponse {
  private String productId;
  private String productItemId;
  private String name;
  private Integer amount;
  private Double price;
  private String classificationName;
  private String classificationId;
  private String subClassificationName;
  private String subClassificationId;

  public CartItemResponse(String productId, String name, Double price) {
    this.productId = productId;
    this.name = name;
    this.price = price;
  }

  public CartItemResponse(
        String productId,
        String productItemId,
        String name,
        Double price,
        String classificationId,
        String classificationName,
        String subClassificationId,
        String subClassificationName
  ) {
    this.productId = productId;
    this.productItemId = productItemId;
    this.name = name;
    this.price = price;
    this.classificationId = classificationId;
    this.classificationName = classificationName;
    this.subClassificationId = subClassificationId;
    this.subClassificationName = subClassificationName;
  }
}
