package com.ncsgroup.inventory.server.entity.product.embed.key;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class ProductPromotionId {
  private String productId;
  private String promotionId;
}
