package com.ncsgroup.inventory.server.utils;

import com.ncsgroup.inventory.server.exception.InternalServerError;

import java.util.HashMap;
import java.util.Map;

public class ConversionUtils {
  private ConversionUtils() {
  }

  public static Map<String, Integer> convertToIntegerValueMap(Map<String, Object> fieldMap) {
    Map<String, Integer> products = new HashMap<>();

    for (Map.Entry<String, Object> entry : fieldMap.entrySet()) {
      products.put(entry.getKey(), convertValueToInteger(entry.getValue()));
    }

    return products;
  }

  private static Integer convertValueToInteger(Object value) {
    if (value instanceof Integer integer) {
      return integer;
    } else if (value instanceof String string) {
      try {
        return Integer.parseInt(string);
      } catch (NumberFormatException e) {
        throw new InternalServerError();
      }
    } else {
      throw new InternalServerError();
    }
  }
}

