package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.entity.product.embed.ProductCategory;
import com.ncsgroup.inventory.server.repository.ProductCategoryRepository;
import com.ncsgroup.inventory.server.service.ProductCategoryService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class ProductCategoryServiceImpl implements ProductCategoryService {

  private final ProductCategoryRepository repository;

  @Override
  @Transactional
  public void save(
        String productId,
        List<String> categoryId
  ) {
    log.info("(save) productId :{}, categoryId :{}", productId, categoryId);

    repository.saveAll(
          this.buildProductCategory(productId, categoryId)
    );

  }

  @Override
  @Transactional
  public void update(String productId, List<String> categoryIds) {
    log.info("(update) productId :{}, categoryIds :{}", productId, categoryIds);

    repository.deleteByProductId(productId);

    repository.saveAll(
          this.buildProductCategory(productId, categoryIds)
    );
  }


  @Override
  public List<CategoryProductResponse> listByProductId(String productId) {
    log.info("(listByProductId) productId:{}", productId);

    return repository.listByProductId(productId);
  }

  private List<ProductCategory> buildProductCategory(
        String productId,
        List<String> categoryIds
  ) {
    log.debug("(buildProductCategory) productId :{}, categoryIds :{}", productId, categoryIds);

    if (categoryIds == null) {
      categoryIds = new ArrayList<>();
    }
    List<ProductCategory> productCategories = new ArrayList<>(categoryIds.size());
    for (String categoryId : categoryIds) {
      productCategories.add(
            ProductCategory.from(
                  productId,
                  categoryId
            )
      );
    }
    return productCategories;
  }

}
