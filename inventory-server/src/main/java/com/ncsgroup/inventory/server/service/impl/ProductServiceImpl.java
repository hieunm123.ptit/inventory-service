package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.client.dto.product.*;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductResponse;
import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.product.Product;
import com.ncsgroup.inventory.server.exception.product.ProductAlreadyExistException;
import com.ncsgroup.inventory.server.exception.product.ProductDuplicateCodeException;
import com.ncsgroup.inventory.server.exception.product.ProductNotFoundException;
import com.ncsgroup.inventory.server.exception.product.QuantityProductNotEnoughException;
import com.ncsgroup.inventory.server.repository.ProductRepository;
import com.ncsgroup.inventory.server.service.ProductService;
import com.ncsgroup.inventory.server.service.base.impl.BaseServiceImpl;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Slf4j
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {

  private final ProductRepository repository;

  public ProductServiceImpl(ProductRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public ProductResponse create(ProductRequest request) {
    log.info("(create) request: {}", request);

    this.checkProductExists(request.getCode());
    Product product = repository.saveAndFlush(this.buildProduct(request));

    return ProductResponse.from(
          product.getId(),
          product.getCode(),
          product.getName(),
          product.getUnit(),
          product.getType(),
          product.getOrigin(),
          product.getDescription(),
          product.getSellerId()
    );
  }

  @Override
  public ProductResponse detail(String id) {
    log.info("(detail) id :{}", id);
    this.checkProductExist(id);

    Product product = repository.find(id);
    return ProductResponse.from(
          product.getId(),
          product.getCode(),
          product.getName(),
          product.getUnit(),
          product.getType(),
          product.getOrigin(),
          product.getDescription(),
          product.getSellerId()

    );
  }

  @Override
  public ProductResponse update(String id, ProductRequest request) {
    log.info("(update) id: {}, request: {}", id, request);

    Product product = this.checkNotFound(id);
    this.checkExist(product, request);

    product.setCode(request.getCode());
    product.setName(request.getName());
    product.setUnit(request.getUnit());
    product.setType(request.getType());
    product.setOrigin(request.getOrigin());
    product.setDescription(request.getDescription());
    product.setSellerId(request.getSellerId());

    create(product);

    return this.migrateProduct(product);
  }

  @Override
  public ProductResponse updateActive(String id) {
    log.info("(updateActive) id: {}", id);

    Product product = this.checkNotFound(id);

    if (product.getIsActive().equals(Active.ACTIVE)) {
      product.setIsActive(Active.INACTIVE);

    } else {
      product.setIsActive(Active.ACTIVE);
    }

    repository.save(product);

    return this.migrateProduct(product);
  }

  private ProductResponse migrateProduct(Product product) {
    log.debug("(migrateProduct) request: {}", product);
    return ProductResponse.of(
          product.getId(),
          product.getCode(),
          product.getName(),
          product.getUnit(),
          product.getType(),
          product.getOrigin(),
          product.getDescription(),
          product.getSellerId(),
          String.valueOf(product.getIsActive().getValue()),
          null,
          null,
          null,
          null,
          null,
          null
    );
  }

  @Override
  public PageResponse<ProductClientResponse> listInClientSide(int page, int size) {
    log.info("(listInClientSide) page:{} ,size :{}", page, size);

    Page<ProductClientResponse> pageResponse = repository.listInClientSide(PageRequest.of(page, size));

    return PageResponse.of(pageResponse.getContent(), (int) pageResponse.getTotalElements());
  }


  @Override

  public PageResponse<ProductClientResponse> filterInClientSide(ProductFilterRequest request, int page, int size) {
    log.info("(filterInClientSide) request:{}, page:{},size:{} ", request, page, size);

    List<String> attributesId = new ArrayList<>();
    List<String> values = new ArrayList<>();
    this.getListIdAndListValueAttributes(request, attributesId, values);

    Page<ProductClientResponse> pageResponse = repository.listFilterClient(
          request.getProductName(),
          request.getMinPrice(),
          request.getMaxPrice(),
          request.getCategoryId(),
          (!attributesId.isEmpty()) ? attributesId : null,
          (!values.isEmpty()) ? values : null,
          PageRequest.of(page, size));

    return PageResponse.of(pageResponse.getContent(), (int) pageResponse.getTotalElements());

  }

  @Override
  public PageResponse<ProductAdminResponse> listInAdminSide(int page, int size) {
    log.info("(listInAdminSide) page:{}, size:{}", page, size);

    Page<ProductAdminResponse> pageResponse = repository.listInAdminSide(PageRequest.of(page, size));

    return PageResponse.of(pageResponse.getContent(), (int) pageResponse.getTotalElements());
  }

  @Override
  public PageResponse<ProductAdminResponse> filterInAdminSide(ProductAdminFilterRequest request, int page, int size) {
    log.info("(listFilterAdmin) request:{}, page:{}, size:{}", request, page, size);

    List<String> attributesId = new ArrayList<>();
    List<String> values = new ArrayList<>();
    this.getListIdAndListValueAttributes(request, attributesId, values);

    Page<ProductAdminResponse> pageResponse = repository.filterInAdminSide(
          request.getCode(),
          request.getProductName(),
          request.getMinPrice(),
          request.getMaxPrice(),
          request.getQuantityRemaining(),
          request.getQuantitySold(),
          request.getRange(),
          request.getCategories(),
          (!attributesId.isEmpty()) ? attributesId : null,
          (!values.isEmpty()) ? values : null,
          request.getWarehouses(),
          request.getActive(),
          PageRequest.of(page, size)
    );
    return PageResponse.of(pageResponse.getContent(), (int) pageResponse.getTotalElements());
  }

  @Override
  public List<CartItemResponse> getItems(List<String> itemIds) {
    log.info("(getItems) itemIds: " + itemIds);

    return repository.getItems(itemIds);
  }

  @Override
  @Transactional
  public void deductProduct(List<ProductOrderRequest> request, String warehouseId) {
    log.info("(deductProduct) request:{}, warehouseId:{} ", request, warehouseId);

    for (ProductOrderRequest productOrder : request) {

      if (Objects.nonNull(productOrder.getProductItemId())) {

        checkQuantityProductItem(productOrder, warehouseId);
        repository.updateQuantityProductItem(productOrder.getProductItemId(), productOrder.getQuantity(), warehouseId);

      } else {

        checkQuantityProduct(productOrder, warehouseId);
        repository.updateQuantityProduct(productOrder.getProductId(), productOrder.getQuantity(), warehouseId);

      }

    }
  }


  @Override
  @Transactional
  public void deleteById(String id) {
    log.info("(deleteById) id:{}", id);

    checkProductExist(id);
    repository.deleteById(id);
  }

  @Override
  public void checkProductExist(String id) {
    log.debug("(checkProductExist) id: {}", id);

    if (!repository.existsById(id)) {
      log.error("(checkProductExist) =====>ProductNotFoundException");
      throw new ProductNotFoundException();
    }
  }

  @Override
  public void checkProductExist(List<String> id) {
    log.debug("(checkProductExist) id: {}", id);

    if (repository.countMatching(id) != id.size()) {
      log.error("(checkProductExist) =====>ProductNotFoundException");
      throw new ProductNotFoundException();
    }
  }


  private void checkProductExists(String code) {
    log.debug("(checkProductExists) code: {}", code);

    if (repository.existsByCode(code)) {
      throw new ProductDuplicateCodeException();
    }
  }

  private void checkExist(Product product, ProductRequest request) {
    if (Objects.nonNull(request.getCode())
          && !request.getCode().equals(product.getCode())
          && repository.existsByCode(request.getCode())) {
      log.error("(ProductAlreadyExistException) =====> ProductAlreadyExistException");
      throw new ProductAlreadyExistException();
    }
  }

  private Product checkNotFound(String id) {
    Product product = repository.findProductById(id);
    if (product == null) {
      log.error("(ProductNotFoundException) =====> ProductNotFoundException");
      throw new ProductNotFoundException();
    }

    return product;
  }


  private Product buildProduct(ProductRequest request) {
    log.debug("(buildProduct) request: {}", request);

    return Product.from(
          request.getCode(),
          request.getName(),
          request.getUnit(),
          request.getType(),
          request.getOrigin(),
          request.getDescription(),
          request.getSellerId()
    );
  }

  private void getListIdAndListValueAttributes(
        ProductFilterRequest request,
        List<String> attributesId,
        List<String> values
  ) {

    log.debug("(getListIdAndListValueAttributes)request:{}, attributesId:{}, values:{}", request, attributesId, values);
    if (Objects.nonNull(request.getAttributes())) {
      for (AttributeProductRequest attributeProductRequest : request.getAttributes()) {
        attributesId.add(attributeProductRequest.getAttributeId());
        values.add(attributeProductRequest.getValue());
      }
    }
  }

  private void getListIdAndListValueAttributes(
        ProductAdminFilterRequest request,
        List<String> attributesId,
        List<String> values
  ) {

    log.debug("(getListIdAndListValueAttributes)request:{}, attributesId:{}, values:{}", request, attributesId, values);
    if (Objects.nonNull(request.getAttributes())) {
      for (AttributeProductRequest attributeProductRequest : request.getAttributes()) {
        attributesId.add(attributeProductRequest.getAttributeId());
        values.add(attributeProductRequest.getValue());
      }
    }
  }


  private void checkQuantityProductItem(ProductOrderRequest productOrder, String warehouseId) {
    log.debug("(checkQuantityProductItem) productOrder:{}", productOrder);

    if (!repository.checkQuantityProductItem(
          productOrder.getProductItemId(),
          productOrder.getQuantity(),
          warehouseId)
    ) {

      log.error("(checkQuantityProductItem) =======> QuantityProductNotEnoughException");
      throw new QuantityProductNotEnoughException();

    }
  }

  private void checkQuantityProduct(ProductOrderRequest productOrder, String warehouseId) {
    log.debug("(checkQuantityProductItem) productOrder:{}", productOrder);

    if (!repository.checkQuantityProduct(
          productOrder.getProductId(),
          productOrder.getQuantity(),
          warehouseId)
    ) {

      log.error("(checkQuantityProductItem) =======> QuantityProductNotEnoughException");
      throw new QuantityProductNotEnoughException();

    }
  }


}
