package com.ncsgroup.inventory.server.controller;


import com.ncsgroup.inventory.server.dto.response.PageResponseGeneral;
import com.ncsgroup.inventory.client.dto.WarehouseRequest;
import com.ncsgroup.inventory.server.dto.response.ResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehousePageResponse;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.service.MessageService;
import com.ncsgroup.inventory.server.service.WarehouseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.*;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageCode.CREATE_WAREHOUSE_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageCode.UPDATE_WAREHOUSE_SUCCESS;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/warehouses")
@Slf4j
public class WarehouseController {
  private final WarehouseService warehouseService;
  private final MessageService messageService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseGeneral<WarehouseResponse> create(
        @Valid @RequestBody WarehouseRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(create) request: {}", request);

    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_WAREHOUSE_SUCCESS, language),
          warehouseService.create(request)
    );
  }

  @PutMapping("{id}")
  public ResponseGeneral<WarehouseResponse> update(
        @PathVariable String id,
        @Valid @RequestBody WarehouseRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(update) id:{}, request:{}", id, request);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(UPDATE_WAREHOUSE_SUCCESS, language),
          warehouseService.update(id, request)
    );
  }


  @GetMapping
  public PageResponseGeneral<WarehousePageResponse> list(
        @RequestParam(name = "keyword", required = false) String keyword,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "all", defaultValue = "false", required = false) boolean isAll,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(list) keyword: {}, size : {}, page: {}, isAll: {}", keyword, size, page, isAll);

    return PageResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          warehouseService.list(keyword, size, page, isAll)
    );
  }

  @DeleteMapping("{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(delete) id: {}", id);

    warehouseService.deleteById(id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );

  }

}
