package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.server.dto.response.warehouse.WarehousePageResponse;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import com.ncsgroup.inventory.server.entity.Warehouse;
import com.ncsgroup.inventory.server.service.base.BaseService;
import com.ncsgroup.inventory.client.dto.WarehouseRequest;

import java.util.List;

public interface WarehouseService extends BaseService<Warehouse> {
  WarehouseResponse create(WarehouseRequest request);

  WarehouseResponse update(String id, WarehouseRequest request);

  void deleteById(String id);

  WarehousePageResponse list(String keyword, int size, int page, boolean isAll);


  void checkExistWarehouse(List<String> ids);

  void checkWarehousesExist(List<String> warehouseIds);


}
