package com.ncsgroup.inventory.server.entity.product.embed;

import com.ncsgroup.inventory.server.entity.product.embed.key.ProductPromotionId;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "product_promotion")
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class ProductPromotion {
  @EmbeddedId
  private ProductPromotionId id;

  public static ProductPromotion from(String productId, String promotionId) {
    return ProductPromotion.of(
          ProductPromotionId.of(
                productId,
                promotionId
          ));
  }
}
