package com.ncsgroup.inventory.server.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;
@Slf4j
public class DateUtils {

  private DateUtils() {
  }

  public static String getCurrentDateString() {
    return LocalDate.now().toString();
  }

  public static Long currentTimeMillis() {
    return System.currentTimeMillis();
  }

  public static Long convertToTimestamp(String dateString) {

    log.debug("(convertToTimestamp) dateString: {}", dateString);
    if (Objects.isNull(dateString)) return null;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    try {
      Date date = dateFormat.parse(dateString);
      return date.getTime();
    } catch (ParseException e) {
      log.error(e.toString());
    }
    return null;
  }

  public static String convertToDateString(Long timestamp) {
    log.debug("(convertToTimestamp) timestamp: {}", timestamp);
    Date date = new Date(timestamp);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    return dateFormat.format(date);
  }
}
