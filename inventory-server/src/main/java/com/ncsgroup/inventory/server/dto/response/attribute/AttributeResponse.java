package com.ncsgroup.inventory.server.dto.response.attribute;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.server.entity.enums.Active;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AttributeResponse {
  private String id;
  private String name;
  private Active isActive;

  public AttributeResponse(String id, String name, Active isActive) {
    this.id = id;
    this.name = name;
    this.isActive = isActive;
  }
}
