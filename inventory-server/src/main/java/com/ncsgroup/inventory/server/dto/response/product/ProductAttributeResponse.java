package com.ncsgroup.inventory.server.dto.response.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductAttributeResponse {
  private String attributeId;
  private String attributeName;
  private String attributeValue;
  private boolean isDetail;

  public ProductAttributeResponse (String attributeId, String attributeName, String attributeValue, boolean isDetail){
    this.attributeId = attributeId;
    this.attributeName = attributeName;
    this.attributeValue = attributeValue;
    this.isDetail = isDetail;
  }
}
