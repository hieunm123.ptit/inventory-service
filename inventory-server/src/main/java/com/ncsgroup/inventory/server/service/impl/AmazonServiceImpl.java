package com.ncsgroup.inventory.server.service.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.ncsgroup.inventory.client.dto.File;
import com.ncsgroup.inventory.server.service.AmazonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class AmazonServiceImpl implements AmazonService {
  @Value("${application.bucket.name}")
  private String bucketName;

  private final AmazonS3 s3Client;

  @Override
  public List<File> uploadFile(List<MultipartFile> files) {
    try {
      List<File> fileList = new ArrayList<>();

      for (MultipartFile file : files){
        String fileName = file.getOriginalFilename();
        String contentType = file.getContentType();
        s3Client.putObject(new PutObjectRequest(bucketName, fileName, file.getInputStream(), new ObjectMetadata()));

        String fileUrl = generatePreSignedUrl(fileName);
        fileList.add(File.of(fileName, bucketName, contentType, fileUrl));
      }

      return fileList;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String generatePreSignedUrl(String fileName) {

    try {
      java.util.Date expiration = new java.util.Date();
      long expTimeMillis = expiration.getTime();
      expTimeMillis += 60 * 60 * 1000;
      expiration.setTime(expTimeMillis);

      GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName)
            .withMethod(HttpMethod.GET)
            .withExpiration(expiration);

      URL presignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

      return presignedUrl.toString();
    } catch (AmazonServiceException e) {
      throw new RuntimeException(e);
    }
  }
}
