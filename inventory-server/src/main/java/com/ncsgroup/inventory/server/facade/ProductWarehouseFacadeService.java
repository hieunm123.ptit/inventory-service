package com.ncsgroup.inventory.server.facade;

import com.ncsgroup.inventory.client.dto.product.ClassificationRequest;
import com.ncsgroup.inventory.client.dto.product.ProductWarehouseRequest;

import java.util.List;

public interface ProductWarehouseFacadeService {
  void saveProductWarehouseService(
        String productId,
        List<ProductWarehouseRequest> productWarehouseRequests,
        List<ClassificationRequest> classificationRequests);
}
