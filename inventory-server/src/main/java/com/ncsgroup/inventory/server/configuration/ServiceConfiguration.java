package com.ncsgroup.inventory.server.configuration;


import com.amazonaws.services.s3.AmazonS3;
import com.ncsgroup.inventory.server.facade.*;
import com.ncsgroup.inventory.server.facade.impl.*;
import com.ncsgroup.inventory.server.repository.*;
import com.ncsgroup.inventory.server.service.*;
import com.ncsgroup.inventory.server.service.impl.*;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class ServiceConfiguration {
  @Bean
  public MessageService messageService(MessageSource messageSource) {
    return new MessageServiceImpl(messageSource);
  }

  @Bean
  public WarehouseService warehouseService(WarehouseRepository repository) {
    return new WarehouseServiceImpl(repository);
  }

  @Bean
  public CategoryService categoryService(CategoryRepository repository) {
    return new CategoryServiceImpl(repository);
  }

  @Bean
  public ProductService productService(ProductRepository repository) {
    return new ProductServiceImpl(repository);
  }

  @Bean
  public ProductCategoryService productCategoryService(ProductCategoryRepository repository) {
    return new ProductCategoryServiceImpl(repository);
  }

  @Bean
  public ProductWarehouseService productWarehouseService(ProductWarehouseRepository repository) {
    return new ProductWarehouseServiceImpl(repository);
  }

  @Bean
  public AttributeService attributeService(AttributeRepository repository) {
    return new AttributeServiceImpl(repository);

  }

  @Bean
  public ProductItemService productItemService(ProductItemRepository repository) {
    return new ProductItemServiceImpl(repository);
  }

  @Bean
  public AmazonService amazonService(AmazonS3 amazonS3) {
    return new AmazonServiceImpl(amazonS3);
  }

  @Bean
  public ProductAttributeService productAttributeService(ProductAttributeRepository repository) {
    return new ProductAttributeServiceImpl(repository);
  }

  @Bean
  public ImageService imageService(ImageRepository repository) {
    return new ImageServiceImpl(repository);
  }

  @Bean
  ImageFacadeService imageFacadeService(ImageService imageService, AmazonService amazonService) {
    return new ImageFacadeServiceImpl(imageService, amazonService);
  }

  @Bean
  public ClassificationFacadeService classificationFacadeService(
        ProductAttributeService productAttributeService,
        ProductItemService productItemService
  ) {
    return new ClassificationFacadeServiceImpl(productAttributeService, productItemService);
  }

  @Bean
  public ProductAttributeFacadeService productAttributeFacadeService(
        ProductAttributeService productAttributeService,
        AttributeService attributeService
  ) {
    return new ProductAttributeFacadeServiceImpl(
          productAttributeService,
          attributeService
    );
  }


  @Bean
  ProductWarehouseFacadeService productWarehouseFacadeService(
        ProductWarehouseService productWarehouseService,
        ClassificationFacadeService classificationFacadeService
  ) {
    return new ProductWarehouseFacadeServiceImpl(
          productWarehouseService,
          classificationFacadeService
    );
  }

  @Bean
  public ProductFacadeService productFacadeService(
        ProductService productService,
        AttributeService attributeService,
        WarehouseService warehouseService,
        CategoryService categoryService,
        PromotionService promotionService,
        ProductCategoryService productCategoryService,
        ProductAttributeService productAttributeService,
        ClassificationFacadeService classificationFacadeService,
        ProductWarehouseService productWarehouseService,
        ProductWarehouseFacadeService productWarehouseFacadeService,
        ProductPromotionService promotionPromotionService,
        ImageService imageService,
        AmazonService amazonService
  ) {
    return new ProductFacadeServiceImpl(
          productService,
          categoryService,
          warehouseService,
          attributeService,
          promotionService,
          productCategoryService,
          productAttributeService,
          productWarehouseService,
          classificationFacadeService,
          productWarehouseFacadeService,
          promotionPromotionService,
          imageService,
          amazonService
    );

  }

  @Bean
  public PromotionService promotionService(PromotionRepository repository) {
    return new PromotionServiceImpl(repository);
  }

  @Bean
  public ProductPromotionService productPromotionService(ProductPromotionRepository repository) {
    return new ProductPromotionServiceImpl(repository);
  }

  @Bean
  public CartRedisService cartService(RedisTemplate<String, Object> redisTemplate) {
    return new CartRedisServiceImpl(redisTemplate);
  }

  @Bean
  public CartRedisFacadeService cartFacadeService(
        CartRedisService cartRedisService,
        ProductService productService,
        ProductItemService productItemService
  ) {
    return  new CartRedisFacadeServiceImpl(
          cartRedisService,
          productService,
          productItemService
    );
  }

}
