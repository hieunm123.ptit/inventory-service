package com.ncsgroup.inventory.server.entity.product;

import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Entity
@Table(name = "Products")
public class Product extends BaseEntityWithUpdater {
  private String code;
  private String name;
  private String unit;
  private Integer type;
  private String origin;
  private String description;
  private String sellerId;
  private double rating;
  private boolean isDeleted;
  @Enumerated(EnumType.ORDINAL)
  private Active isActive;

  public static Product from(
        String code,
        String name,
        String unit,
        Integer type,
        String origin,
        String description,
        String sellerId
  ) {
    return Product.of(code, name, unit, type, origin, description, sellerId, 0, false, Active.ACTIVE);
  }
}
