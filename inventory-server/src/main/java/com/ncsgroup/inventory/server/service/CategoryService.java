package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.CategoryCreateRequest;
import com.ncsgroup.inventory.client.dto.CategoryUpdateRequest;
import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.dto.response.category.CategoryResponse;
import com.ncsgroup.inventory.server.entity.Category;
import com.ncsgroup.inventory.server.service.base.BaseService;

import java.util.List;

public interface CategoryService extends BaseService<Category> {
  CategoryResponse create(CategoryCreateRequest request);

  CategoryResponse detail(String id);

  CategoryResponse update(String id, CategoryUpdateRequest request);

  void deleteById(String id);

  List<CategoryResponse> getAll();

  void checkCategoryIdsExist(List<String> categoryIds);

  List<CategoryProductResponse> getListById(List<String> categoryIds);
}
