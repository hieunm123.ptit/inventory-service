package com.ncsgroup.inventory.server.controller;


import com.ncsgroup.inventory.client.dto.product.*;
import com.ncsgroup.inventory.server.dto.response.PageResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.ResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductResponse;
import com.ncsgroup.inventory.server.facade.ProductFacadeService;
import com.ncsgroup.inventory.server.service.MessageService;
import com.ncsgroup.inventory.server.service.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.*;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageCode.CREATE_PRODUCT_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.UPDATE_PRODUCT_SUCCESS;


@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1/products")
public class ProductController {
  private final MessageService messageService;
  private final ProductFacadeService productFacadeService;
  private final ProductService productService;


  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseGeneral<ProductResponse> create(
        @Valid @RequestBody ProductRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(create) request: {}", request);

    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_PRODUCT_SUCCESS, language),
          productFacadeService.createProduct(request)
    );
  }

  @GetMapping("{id}")
  public ResponseGeneral<ProductResponse> details(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(details) id:{}", id);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          productFacadeService.detailProduct(id)
    );
  }


  @PutMapping("{id}")
  public ResponseGeneral<ProductResponse> update(
        @PathVariable String id,
        @Valid @RequestBody ProductRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(update) id: {}, request: {}", id, request);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(UPDATE_PRODUCT_SUCCESS, language),
          productFacadeService.update(id, request)
    );
  }

  @GetMapping("client")
  public PageResponseGeneral<PageResponse<ProductClientResponse>> listInClientSide(
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(listInClientSide) size:{}, page:{}", size, page);

    return PageResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          productFacadeService.listInClientSide(page, size)
    );
  }

  @PostMapping("filter/client")
  public PageResponseGeneral<PageResponse<ProductClientResponse>> filterInClientSide(
        @RequestBody ProductFilterRequest request,
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(filterInClientSide) request:{}, size:{}, page:{}", request, size, page);

    return PageResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          productFacadeService.filterInClientSide(request, page, size)
    );
  }

  @GetMapping("admin")
  public PageResponseGeneral<PageResponse<ProductAdminResponse>> listInAdminSide(
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(listInClientSide) size:{}, page:{}", size, page);

    return PageResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          productFacadeService.listInAdminSide(page, size)
    );
  }

  @PostMapping("filter/admin")
  public PageResponseGeneral<PageResponse<ProductAdminResponse>> filterInAdminSide(
        @RequestBody ProductAdminFilterRequest request,
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "size", defaultValue = "10") int size,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(filterInClientSide) request:{},size:{}, page:{}", request, size, page);

    return PageResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          productFacadeService.filterInAdminSide(request, page, size)
    );
  }

  @DeleteMapping("{id}")
  public ResponseGeneral<Void> delete(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(delete) id:{}", id);

    productService.deleteById(id);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }

  @PostMapping("{id}/promotions")
  public ResponseGeneral<Void> addPromotionToProduct(
        @PathVariable String id,
        @RequestBody ProductPromotionRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {

    log.info("(addPromotionToProduct) id:{}, promotionIds:{}", id, request);

    productFacadeService.addPromotionToProduct(id, request);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }


  @PutMapping("{id}/update-status")
  public ResponseGeneral<ProductResponse> updateStatus(
        @PathVariable String id,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(updateStatus) id:{}", id);

    return ResponseGeneral.ofSuccess(messageService.getMessage(SUCCESS, language),
          productService.updateActive(id));
  }

  @PostMapping("/deduct")
  public ResponseGeneral<Void> deductProduct(
        @RequestBody ProductOrderCreateRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language

  ) {
    log.info("(deductProduct) request:{}", request);

    productService.deductProduct(request.getProductOder(), request.getWarehouseId());

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }

  @PostMapping("/revert")
  public ResponseGeneral<Void> revert(
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(revert)");

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language)
    );
  }


}
