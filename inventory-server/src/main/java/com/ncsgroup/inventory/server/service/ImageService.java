package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.server.dto.response.image.ImageResponse;
import com.ncsgroup.inventory.server.entity.Image;

import java.util.List;

public interface ImageService {
  List<ImageResponse> save(List<Image> images);

  void addImageToProduct(List<String> imageIds, String productId);

  List<Image> getAllByProduct(String productId);

  void updateImage(List<String> imageProductIds, List<String> imageRequestIds);

  List<String> findId(String productId);

  Image getImage(String productId);

}
