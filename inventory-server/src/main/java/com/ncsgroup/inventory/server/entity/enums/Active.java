package com.ncsgroup.inventory.server.entity.enums;

public enum Active {

  ACTIVE(0),

  INACTIVE(1);
  private int value;

  private Active(int value) {
    this.value = value;
  }

  public static Active valueOf(int value) {
    for (Active active : values()) {
      if (active.getValue() == value) {
        return active;
      }
    }
    throw new IllegalArgumentException("Invalid ThresholdTime value: " + value);
  }

  public int getValue() {
    return value;
  }
}
