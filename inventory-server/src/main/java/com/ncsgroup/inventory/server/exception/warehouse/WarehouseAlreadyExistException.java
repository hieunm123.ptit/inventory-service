package com.ncsgroup.inventory.server.exception.warehouse;

import com.ncsgroup.inventory.server.exception.base.BadRequestException;

public class WarehouseAlreadyExistException extends BadRequestException {
  public WarehouseAlreadyExistException(){
       setCode("com.ncsgroup.inventory.server.exception.warehouse.WarehouseAlreadyExistException");
  }
}
