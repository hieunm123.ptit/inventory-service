package com.ncsgroup.inventory.server.facade.impl;

import com.ncsgroup.inventory.client.dto.CartItemRequest;

import com.ncsgroup.inventory.client.dto.product.ProductCartDeletionRequest;


import com.ncsgroup.inventory.client.dto.product.ProductCartRequest;

import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.exception.cart_item.CartDeletionInvalidException;
import com.ncsgroup.inventory.server.exception.cart_item.CartItemInvalidException;
import com.ncsgroup.inventory.server.facade.CartRedisFacadeService;
import com.ncsgroup.inventory.server.service.CartRedisService;
import com.ncsgroup.inventory.server.service.ProductItemService;
import com.ncsgroup.inventory.server.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

import static com.ncsgroup.inventory.server.constanst.Constants.RedisConstants.CART_PRODUCT_FIELD_PREFIX;
import static com.ncsgroup.inventory.server.constanst.Constants.RedisConstants.CART_PRODUCT_ITEM_FIELD_PREFIX;

@RequiredArgsConstructor
@Slf4j
public class CartRedisFacadeServiceImpl implements CartRedisFacadeService {

  private final CartRedisService cartRedisService;
  private final ProductService productService;
  private final ProductItemService productItemService;

  @Override
  public void addProductToCart(String customerId, ProductCartRequest request) {
    log.info("(addProductToCart) customerId: " + customerId + ", request: " + request);

    this.checkComponentExists(request.getItems());
    cartRedisService.addProductToCart(
          customerId,
          request.getItems()
    );


  }

  @Override
  public void updateProductInCart(String customerId, ProductCartRequest request) {
    log.info("(updateProductInCart) customerId:{} request:{}", customerId, request);

    this.checkComponentExists(request.getItems());
    cartRedisService.updateProductInCart(
          customerId,
          request.getItems()
    );
  }


  @Override
  public void deleteProductInCart(String customerId, ProductCartDeletionRequest request) {
    log.info("(deleteProductInCart) customerId:{}  ProductCartDeletionRequest :{}", customerId, request);

    this.checkComponentExists(request);
    cartRedisService.deleteProductInCart(customerId, request);

  }

  @Override
  public List<CartItemResponse> getProductFromCart(String customerId) {
    log.info("(getProductFromCart) customerId:{}", customerId);

    Map<String, Integer> products = cartRedisService.getProductFromCart(customerId);
    List<String> productIds = new LinkedList<>();
    List<String> productItemIds = new LinkedList<>();
    List<CartItemResponse> itemResponses = new LinkedList<>();

    if (products.isEmpty()) return Collections.emptyList();

    this.getIds(productIds, productItemIds, products);

    if (!productIds.isEmpty()) {
      itemResponses.addAll(productService.getItems(productIds));
    }

    if (!productItemIds.isEmpty()) {
      itemResponses.addAll(productItemService.getItems(productItemIds));
    }

    for (CartItemResponse response : itemResponses) {

      if (Objects.nonNull(response.getProductItemId())) {

        response.setAmount(products.get(CART_PRODUCT_ITEM_FIELD_PREFIX + response.getProductItemId()));
      } else {

        response.setAmount(products.get(CART_PRODUCT_FIELD_PREFIX + response.getProductId()));

      }
    }


    return itemResponses;
  }


  private void checkComponentExists(List<CartItemRequest> items) {
    log.debug("(checkProductExists) items: " + items);
    Set<String> productIds = new HashSet<>();
    Set<String> productItemIds = new HashSet<>();
    for (CartItemRequest item : items) {

      if (!Objects.nonNull(item.getProductItemId()) && !Objects.nonNull(item.getProductId())) {
        log.error("(checkComponentExists) ========> CartItemInvalidException");
        throw new CartItemInvalidException();
      }

      if (Objects.nonNull(item.getProductId())) {
        productIds.add(item.getProductId());
      }

      if (Objects.nonNull(item.getProductItemId())) {
        productItemIds.add(item.getProductItemId());
      }

    }

    if (!productIds.isEmpty()) {
      productService.checkProductExist(productIds.stream().toList());
    }

    if (!productItemIds.isEmpty()) {
      productItemService.checkProductItemExisted(productItemIds.stream().toList());
    }
  }

  private void checkComponentExists(ProductCartDeletionRequest request) {
    log.debug("(checkComponentExists) request:{} ", request);

    if (!Objects.nonNull(request.getProductItemId()) && !Objects.nonNull(request.getProductId())) {
      log.error("(checkComponentExists) =========> CartDeletionInvalid");

      throw new CartDeletionInvalidException();
    }

  }

  private void getIds(List<String> productIds, List<String> productItemIds, Map<String, Integer> products) {
    log.debug("(getIds) productIds:{} productItemIds:{} products:{}", productIds, productItemIds, products);

    for (String key : products.keySet()) {
      if (key.startsWith(CART_PRODUCT_ITEM_FIELD_PREFIX)) {

        productItemIds.add(key.substring(CART_PRODUCT_ITEM_FIELD_PREFIX.length()));

      } else {

        productIds.add(key.substring(CART_PRODUCT_FIELD_PREFIX.length()));
      }

    }

  }

}
