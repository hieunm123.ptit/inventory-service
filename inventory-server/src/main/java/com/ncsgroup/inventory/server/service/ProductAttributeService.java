package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.product.AttributeProductRequest;
import com.ncsgroup.inventory.server.dto.response.product.ClassificationResponse;
import com.ncsgroup.inventory.server.entity.product.ProductAttribute;
import com.ncsgroup.inventory.server.service.base.BaseService;

import java.util.List;

public interface ProductAttributeService extends BaseService<ProductAttribute> {
  void save(List<ProductAttribute> productAttributes);

  void update(String productId, List<AttributeProductRequest> requests);

  void checkExist(List<String> ids);

  ProductAttribute findProductAttribute(String productAttributeId, String attributeId);

  List<ClassificationResponse> listByProductId(String productId);

  List<ClassificationResponse> listByParentId(String parentId);
}
