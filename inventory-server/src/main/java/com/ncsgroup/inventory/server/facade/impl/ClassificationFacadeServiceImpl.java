package com.ncsgroup.inventory.server.facade.impl;

import com.ncsgroup.inventory.client.dto.product.ClassificationRequest;
import com.ncsgroup.inventory.server.dto.response.product.ClassificationResponse;
import com.ncsgroup.inventory.server.entity.product.ProductAttribute;
import com.ncsgroup.inventory.server.entity.product.ProductItem;
import com.ncsgroup.inventory.server.facade.ClassificationFacadeService;
import com.ncsgroup.inventory.server.service.ProductAttributeService;
import com.ncsgroup.inventory.server.service.ProductItemService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Stream;

import static com.ncsgroup.inventory.server.utils.GeneratorUtils.generateId;

@Slf4j
@RequiredArgsConstructor
public class ClassificationFacadeServiceImpl implements ClassificationFacadeService {
  private final ProductAttributeService productAttributeService;
  private final ProductItemService productItemService;

  @Override
  @Transactional
  public void createClassifications(
        String productId,
        List<ClassificationRequest> requests
  ) {
    log.info("(createClassifications) requests: {}", requests);

    productAttributeService.save(
          this.buildProductAttributes(productId, requests));

  }

  @Override
  @Transactional
  public void updateClassifications(String productId, List<ClassificationRequest> requests) {
    log.info("(createClassifications) productId: {}, requests: {}", productId, requests);

    productAttributeService.checkExist(this.getAllIds(requests));
    List<ProductItem> productItems = new ArrayList<>();
    List<ProductAttribute> productAttributes = new ArrayList<>();

    for (ClassificationRequest request : requests) {
      if (request.getId() == null ) {

        productAttributeService.save(
              this.buildProductAttributes(productId, requests));

      } else {
        ProductAttribute productAttribute = productAttributeService.findProductAttribute(request.getId(), request.getAttributeId());
        productAttribute.setAttributeValue(request.getValue());

        productAttributes.add(productAttribute);

        if (request.getSubClassifications() == null) {
          ProductItem productItem = productItemService.findProductItem(productAttribute.getProductItemId());

          productItem.setCode(request.getProductItem().getCode());

          productItems.add(productItem);

        } else {
          for (ClassificationRequest classificationRequest : request.getSubClassifications()) {

            ProductAttribute productAttribute1 = productAttributeService.findProductAttribute(
                  classificationRequest.getId(),
                  request.getAttributeId()
            );
            productAttribute1.setAttributeValue(classificationRequest.getValue());
            productAttributes.add(productAttribute1);

            ProductItem productItem = productItemService.findProductItem(productAttribute1.getProductItemId());
            productItem.setCode(request.getProductItem().getCode());

            productItems.add(productItem);
          }
        }
      }
    }

    productItemService.save(productItems);
    productAttributeService.save(productAttributes);
  }

  @Override
  public List<ClassificationResponse> getClassifications(String productId) {
    log.info("(getClassifications) productId:{} ", productId);

    List<ClassificationResponse> classifications = productAttributeService.listByProductId(productId);
    for (ClassificationResponse classification : classifications) {
      if (!Objects.nonNull(classification.getProductItem())) {
        classification.setSubClassifications(productAttributeService.listByParentId(classification.getId()));
      }
    }
    return classifications;
  }

  @Override
  public Set<String> getCodeProductItems(List<ClassificationRequest> requests) {
    log.info("(getCodeProductItems) requests:{}", requests);
    Set<String> codes = new HashSet<>();
    for (ClassificationRequest request : requests) {
      if (Objects.nonNull(request.getProductItem())) {
        codes.add(request.getProductItem().getCode());
      } else {
        for (ClassificationRequest subClassification : request.getSubClassifications()) {
          codes.add(subClassification.getProductItem().getCode());
        }
      }
    }
    return codes;
  }

  private List<ProductAttribute> buildProductAttributes(
        String productId,
        List<ClassificationRequest> requests
  ) {
    log.debug("(getProductAttributes) productId:{}, requests: {}", productId, requests);

    List<ProductAttribute> productAttributes = new ArrayList<>();
    List<ProductItem> productItems = new ArrayList<>();
    String id;
    String parentId;
    for (ClassificationRequest request : requests) {
      id = parentId = generateId();
      if (!Objects.nonNull(request.getSubClassifications())) {
        ProductItem productItem = ProductItem.from(
              request.getProductItem().getCode()
        );
        productItems.add(productItem);
        productAttributes.add(
              ProductAttribute.from(
                    id,
                    productId,
                    request.getAttributeId(),
                    productItem.getId(),
                    request.getValue(),
                    null
              )
        );

      } else {
        productAttributes.add(
              ProductAttribute.from(
                    id,
                    productId,
                    request.getAttributeId(),
                    request.getValue(),
                    null
              )
        );

        for (ClassificationRequest subClassification : request.getSubClassifications()) {
          id = generateId();
          ProductItem productItem = ProductItem.from(
                subClassification.getProductItem().getCode()
          );
          productItems.add(productItem);
          productAttributes.add(
                ProductAttribute.from(
                      id,
                      productId,
                      subClassification.getAttributeId(),
                      productItem.getId(),
                      subClassification.getValue(),
                      parentId
                )
          );
        }
      }
    }
    productItemService.save(productItems);

    return productAttributes;
  }

  private List<String> getAllIds(List<ClassificationRequest> classificationRequests) {
    log.debug("(getAllIds) request: {}", classificationRequests);

    return classificationRequests.stream()
          .filter(request -> request.getId() != null && !request.getId().isEmpty())
          .flatMap(request -> Stream.concat(
                Stream.of(request.getId()),
                getAllIds(request.getSubClassifications()).stream()
          ))
          .toList();
  }
}
