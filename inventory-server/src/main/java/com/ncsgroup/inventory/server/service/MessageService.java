package com.ncsgroup.inventory.server.service;

public interface MessageService {
  String getMessage(String code, String language);
}
