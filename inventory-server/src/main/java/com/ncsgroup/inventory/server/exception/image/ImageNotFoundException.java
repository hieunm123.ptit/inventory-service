package com.ncsgroup.inventory.server.exception.image;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class ImageNotFoundException extends NotFoundException {
  public ImageNotFoundException(){
    setCode("(com.ncsgroup.inventory.server.exception.image.ImageNotFoundException)");
  }
}
