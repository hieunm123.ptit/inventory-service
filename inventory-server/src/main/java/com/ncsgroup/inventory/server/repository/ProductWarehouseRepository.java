package com.ncsgroup.inventory.server.repository;


import com.ncsgroup.inventory.server.dto.response.product.ProductWarehouseResponse;
import com.ncsgroup.inventory.server.entity.product.embed.ProductWarehouse;
import com.ncsgroup.inventory.server.entity.product.embed.key.ProductWarehouseId;
import jakarta.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductWarehouseRepository extends JpaRepository<ProductWarehouse, ProductWarehouseId> {
  @Query("SELECT new " +
        "com.ncsgroup.inventory.server.dto.response.product.ProductWarehouseResponse(" +
        "w.code, w.name, w.phoneNumber, w.addressId, " +
        "pi.id, pi.code, " +
        "p.importPrice, p.exportPrice,p.quantity,p.quantitySold,p.currencyCode) " +
        "from ProductWarehouse p " +
        "Join Warehouse w on p.id.warehouseId = w.id " +
        "left join ProductItem pi on p.id.productItemCode = pi.code " +
        "where p.id.productId = :productId")
  List<ProductWarehouseResponse> getListByProductId(String productId);


  @Transactional
  @Modifying
  @Query("UPDATE ProductWarehouse pw SET pw.exportPrice = :exportPrice, pw.importPrice = :importPrice, " +
        "pw.quantity = :quantity, pw.quantitySold = :quantitySold where pw.id.productId = :productId and" +
        " pw.id.warehouseId = :warehouseId and pw.id.productItemCode = :productItemCode ")
  void update(
        String productId,
        String warehouseId,
        String productItemCode,
        Double importPrice,
        Double exportPrice,
        Integer quantity,
        Integer quantitySold
  );

  @Query("""
        SELECT SUM(pw.quantitySold) from ProductWarehouse pw
        Where pw.id.productId = :productId
        """
  )
  Long getSumQuantitySold(String productId);


  @Query(
        """
              SELECT NEW com.ncsgroup.inventory.server.dto.response.product.ProductWarehouseResponse(
              w.code, w.name, w.phoneNumber, w.addressId,
              pi.id, pi.code,
              p.importPrice, p.exportPrice,p.quantity,p.quantitySold,p.currencyCode)
              from ProductWarehouse p
              Join Warehouse w on p.id.warehouseId = w.id
              left join ProductItem pi on p.id.productItemCode = pi.code
              where p.id.productId = :productId
              AND (coalesce(:warehouseId) IS NULL OR  p.id.warehouseId IN (:warehouseId))
              And (:quantityRemaining IS NULL OR (p.quantity - p.quantitySold <= :quantityRemaining +:range  ))
              And (:quantityRemaining IS NULL OR (p.quantity - p.quantitySold >= :quantityRemaining -:range ))
              And (:quantitySold IS NULL OR p.quantitySold <= :quantitySold +:range)
              And (:quantitySold IS NULL OR p.quantitySold >= :quantitySold -:range)
              """
  )
  List<ProductWarehouseResponse> filterProductWarehouse(
        String productId,
        List<String> warehouseId,
        Integer quantityRemaining,
        Integer quantitySold,
        Integer range
  );




}
