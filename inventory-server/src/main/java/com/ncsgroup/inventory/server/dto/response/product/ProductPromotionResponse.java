package com.ncsgroup.inventory.server.dto.response.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ncsgroup.inventory.server.entity.enums.Active;
@JsonInclude(JsonInclude.Include.NON_NULL)
public record ProductPromotionResponse(
      Double discountPercent,
      Integer moneyDeducted,
      String description,
      Active active) {
}
