package com.ncsgroup.inventory.server.service.impl;

import com.ncsgroup.inventory.client.dto.product.AttributeProductRequest;
import com.ncsgroup.inventory.server.dto.response.product.ClassificationResponse;
import com.ncsgroup.inventory.server.entity.product.ProductAttribute;
import com.ncsgroup.inventory.server.exception.product.ProductAttributeNotFound;
import com.ncsgroup.inventory.server.repository.ProductAttributeRepository;
import com.ncsgroup.inventory.server.service.ProductAttributeService;
import com.ncsgroup.inventory.server.service.base.impl.BaseServiceImpl;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class ProductAttributeServiceImpl extends BaseServiceImpl<ProductAttribute> implements ProductAttributeService {
  private final ProductAttributeRepository repository;

  public ProductAttributeServiceImpl(ProductAttributeRepository repository) {
    super(repository);
    this.repository = repository;
  }

  @Override
  public void save(List<ProductAttribute> productAttributes) {
    log.info("(save) productAttributes :{}", productAttributes);

    repository.saveAllAndFlush(productAttributes);
  }

  @Override
  @Transactional
  public void update(String productId, List<AttributeProductRequest> requests) {
    log.info("(update) productId: {}, requests: {}", productId, requests);

    repository.deleteByProductId(productId);

    repository.saveAllAndFlush(
          this.buildProductAttributes(productId, requests)
    );

  }

  @Override
  public void checkExist(List<String> ids) {
    log.info("(checkExist) request: {}", ids);

    if (repository.countProductAttribute(ids) < ids.size())
      throw new ProductAttributeNotFound();
  }

  @Override
  public ProductAttribute findProductAttribute(String productAttributeId, String attributeId) {
    log.info("(findProductAttribute) id: {}, attribute: {}", productAttributeId, attributeId);

    return repository.findProductAttribute(productAttributeId, attributeId);
  }

  private List<ProductAttribute> buildProductAttributes(
        String productId, List<AttributeProductRequest> attributes
  ) {
    log.debug("(buildProductAttributes)productId: {}, attributes: {}", productId, attributes);

    return attributes.stream()
          .map(attribute -> ProductAttribute.from(
                productId,
                attribute.getAttributeId(),
                attribute.getValue()))
          .toList();
  }

  @Override
  public List<ClassificationResponse> listByProductId(String productId) {
    log.info("(listByProductId) productId :{}", productId);

    return repository.listByProductId(productId);
  }

  @Override
  public List<ClassificationResponse> listByParentId(String parentId) {
    log.info("(listByParentId) parentId :{}", parentId);

    return repository.listByParentId(parentId);
  }
}
