package com.ncsgroup.inventory.server.service.impl;


import com.ncsgroup.inventory.server.dto.response.product.ProductPromotionResponse;
import com.ncsgroup.inventory.server.dto.response.promotion.PromotionResponse;
import com.ncsgroup.inventory.server.entity.product.embed.ProductPromotion;
import com.ncsgroup.inventory.server.exception.promotion.PromotionAlreadyExistException;

import com.ncsgroup.inventory.server.repository.ProductPromotionRepository;
import com.ncsgroup.inventory.server.service.ProductPromotionService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class ProductPromotionServiceImpl implements ProductPromotionService {

  private final ProductPromotionRepository repository;

  @Override
  @Transactional
  public void save(String productId, List<String> promotionIds) {
    log.info("(save) productId:{}, promotionIds:{} ", productId, promotionIds);

    this.checkExist(productId, promotionIds);
    repository.saveAll(
          this.buildProductPromotion(productId, promotionIds));

  }

  @Override
  public List<ProductPromotionResponse> listByProductIdInClientSide(String productId) {
    log.info("(listByProductIdInClientSide) productId: " + productId);

    return repository.listByProductIdInClientSide(productId);
  }

  @Override
  public List<ProductPromotionResponse> listByProductIdInAdminSide(String productId) {
    log.info("(listByProductIdInAdminSide) productId:{}", productId);

    return repository.listByProductIdInAdminSide(productId);
  }

  @Override
  public List<PromotionResponse> listByProductId(String productId) {
    log.info("(listByProductId) productId:{}", productId);

    return repository.listByProductId(productId);
  }

  private List<ProductPromotion> buildProductPromotion(String productId, List<String> promotionIds) {
    log.debug("(buildProductPromotion) productId:{}, promotionIds:{} ", productId, promotionIds);
    List<ProductPromotion> productPromotions = new ArrayList<>();
    for (String promotionId : promotionIds) {
      productPromotions.add(
            ProductPromotion.from(
                  productId,
                  promotionId
            ));
    }

    return productPromotions;
  }

  private void checkExist(String productId, List<String> promotionIds) {
    log.debug("(checkExist) productId:{}, promotionIds:{} ", productId, promotionIds);

    if (repository.existsByKey(productId, promotionIds)) {
      log.error("(checkExist)===========>PromotionAlreadyExistsException");

      throw new PromotionAlreadyExistException();
    }
  }
}
