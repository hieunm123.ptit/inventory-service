package com.ncsgroup.inventory.server.repository;

import com.ncsgroup.inventory.client.enums.Active;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.entity.product.Product;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface ProductRepository extends BaseRepository<Product> {

  @Query(value = "select p from Product p where p.id = :id and p.isDeleted = false")
  Product findProductById(String id);

  @Query("SELECT CASE WHEN " +
        "(SELECT COUNT(p) FROM Product p where p.code = :code and p.isDeleted =false) > 0 " +
        "THEN true ELSE false " +
        "END ")
  boolean existsByCode(String code);

  @Query("SELECT CASE WHEN " +
        "(SELECT COUNT(p) FROM Product p where p.id = :id and p.isDeleted =false) > 0 " +
        "THEN true ELSE false " +
        "END ")
  boolean existsById(String id);

  @Query("SELECT p from Product p where p.id =:id and p.isDeleted = false")
  Product find(String id);

  @Query("SELECT DISTINCT new com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse(" +
        "p.id, p.name, sum(pw.quantitySold), min(pw.exportPrice )) " +
        "From Product p " +
        "Left Join ProductWarehouse pw on  p.id = pw.id.productId " +
        "Where p.isDeleted = false and p.isActive = 0 " +
        "Group by p.id, p.name")
  Page<ProductClientResponse> listInClientSide(Pageable pageable);


  @Query("""
        SELECT DISTINCT new com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse(
        p.id, p.name, Sum(pw.quantitySold) ,  Min(pw.exportPrice) )
        FROM Product p
        LEFT JOIN ProductWarehouse pw ON p.id = pw.id.productId
        LEFT JOIN ProductCategory pc ON p.id = pc.id.productId
        LEFT JOIN ProductAttribute pa ON p.id = pa.productId
        WHERE (:productName IS NULL OR lower(p.name) LIKE lower(concat('%', :productName , '%')))
        AND (:minPrice IS NULL OR pw.exportPrice >= :minPrice)
        AND (:maxPrice IS NULL OR pw.exportPrice <= :maxPrice)
        AND ( coalesce(:categoriesId) IS NULL OR  pc.id.categoryId IN (:categoriesId))
        AND (
            (coalesce(:attributesId) IS NULL OR pa.attributeId IN :attributesId)
            AND (coalesce(:values) IS NULL OR pa.attributeValue IN :values)
        )
        AND p.isDeleted = false AND p.isActive = 0
        GROUP BY p.id, p.name
                
        """)
  Page<ProductClientResponse> listFilterClient(
        String productName,
        Integer minPrice,
        Integer maxPrice,
        List<String> categoriesId,
        List<String> attributesId,
        List<String> values,
        Pageable pageable
  );


  @Query("""
        SELECT DISTINCT p.id
        FROM Product p
        LEFT JOIN ProductWarehouse pw ON p.id = pw.id.productId
        LEFT JOIN ProductCategory pc ON p.id = pc.id.productId
        LEFT JOIN ProductAttribute pa ON p.id = pa.productId
        WHERE (:productName IS NULL OR lower(p.name) LIKE lower(concat('%', :productName , '%')))
        AND (:minPrice IS NULL OR pw.exportPrice >= :minPrice)
        AND (:maxPrice IS NULL OR pw.exportPrice <= :maxPrice)
        AND ( coalesce(:categoriesId) IS NULL OR  pc.id.categoryId IN (:categoriesId))
        AND (
            (coalesce(:attributesId) IS NULL OR pa.attributeId IN :attributesId)
            AND (coalesce(:values) IS NULL OR pa.attributeValue IN :values)
        )
        AND p.isDeleted = false AND p.isActive = 0
        GROUP BY p.id, p.name
                
        """)
  List<String> getIdProductFilter(
        String productName,
        Integer minPrice,
        Integer maxPrice,
        List<String> categoriesId,
        List<String> attributesId,
        List<String> values,
        Pageable pageable
  );

  @Query(
        """
              SELECT new com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse(
              p.id, p.code, p.name, p.isActive)
              from Product p
              WHERE p.isDeleted = false
              """)
  Page<ProductAdminResponse> listInAdminSide(Pageable pageable);

  @Query(
        """
              SELECT new com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse( 
              p.id, p.code, p.name, p.isActive) 
              from Product p 
              LEFT JOIN ProductWarehouse pw ON p.id = pw.id.productId 
              LEFT JOIN ProductCategory pc ON p.id = pc.id.productId 
              LEFT JOIN ProductAttribute pa ON p.id = pa.productId 
              Where (:productName IS NULL OR lower(p.name) LIKE lower(concat('%', :productName , '%'))) 
              And (:code IS NULL OR lower(p.code) LIKE lower(concat('%', :code , '%'))) 
              AND (:minPrice IS NULL OR pw.exportPrice >= :minPrice) 
              AND (:maxPrice IS NULL OR pw.exportPrice <= :maxPrice) 
              AND ((:quantityRemaining IS NULL) OR ((pw.quantity - pw.quantitySold) <= :quantityRemaining + COALESCE(:range, 0)))
              AND ((:quantityRemaining IS NULL) OR ((pw.quantity - pw.quantitySold) >= :quantityRemaining - COALESCE(:range, 0)))
              AND ((:quantitySold IS NULL) OR pw.quantitySold <= :quantitySold + COALESCE(:range, 0))
              AND ((:quantitySold IS NULL) OR pw.quantitySold >= :quantitySold - COALESCE(:range, 0))
              AND (COALESCE(:categoriesId) IS NULL OR pc.id.categoryId IN (:categoriesId))
              AND ( (COALESCE(:attributesId) IS NULL OR pa.attributeId IN (:attributesId))
                    AND (COALESCE(:values) IS NULL OR pa.attributeValue IN (:values))                                                                                                                                                                               
                    )
              AND (COALESCE(:warehouseIds) IS NULL OR pw.id.warehouseId IN (:warehouseIds))                                                                                                                                                                                                                                                
              AND ((:active is null) or (p.isActive = :active))
              AND (p.isDeleted = false)  
              GROUP BY p.id 
              """
  )
  Page<ProductAdminResponse> filterInAdminSide(
        String code,
        String productName,
        Integer minPrice,
        Integer maxPrice,
        Integer quantityRemaining,
        Integer quantitySold,
        Integer range,
        List<String> categoriesId,
        List<String> attributesId,
        List<String> values,
        List<String> warehouseIds,
        Active active,
        Pageable pageable
  );

  @Transactional
  @Modifying
  @Query(value = "UPDATE products Set is_deleted = true where id = ?1", nativeQuery = true)
  void deleteById(String id);

  @Query("SELECT COUNT (DISTINCT p.id) FROM Product p WHERE p.id IN :productIds and p.isDeleted = false and p.isActive=0 ")
  int countMatching(List<String> productIds);

  @Query("""
        SELECT new com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse(
        p.id, p.name, pw.exportPrice)
        FROM Product p
        JOIN ProductWarehouse pw on pw.id.productId = p.id
        WHERE p.id IN :itemIds
        AND p.isDeleted = false
        """)
  List<CartItemResponse> getItems(List<String> itemIds);

  @Query("""
        SELECT CASE WHEN
        (SELECT COUNT(pw) FROM ProductWarehouse pw
         where pw.id.productId = :productId
         AND pw.id.warehouseId = :warehouseId
         AND (pw.quantity - pw.quantitySold) > :quantity ) > 0
        THEN true ELSE false
        END
        """)
  boolean checkQuantityProduct(String productId, Integer quantity, String warehouseId);

  @Transactional
  @Modifying
  @Query("""
        Update ProductWarehouse pw Set pw.quantitySold = pw.quantitySold + :quantity
        Where pw.id.warehouseId = :warehouseId
        AND pw.id.productId = :productId
        """)
  void updateQuantityProduct(String productId, Integer quantity, String warehouseId);

  @Query("""
        SELECT CASE WHEN
        (SELECT COUNT(pw) FROM ProductWarehouse pw
         JOIN ProductItem pi ON pi.code = pw.id.productItemCode
         where pi.id = :productItemId
         AND pw.id.warehouseId = :warehouseId
         AND (pw.quantity - pw.quantitySold) > :quantity ) > 0
        THEN true ELSE false
        END
        """)
  boolean checkQuantityProductItem(String productItemId, Integer quantity, String warehouseId);

  @Transactional
  @Modifying
  @Query(value = """
        UPDATE product_warehouse
        SET quantity_sold = quantity_sold +?2
        FROM product_items
        WHERE warehouse_id = ?3
        AND id = ?1
        """, nativeQuery = true)
  void updateQuantityProductItem(String productItemId, Integer quantity, String warehouseId);

}
