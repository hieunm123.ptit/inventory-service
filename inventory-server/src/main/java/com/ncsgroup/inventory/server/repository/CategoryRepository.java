package com.ncsgroup.inventory.server.repository;

import com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse;
import com.ncsgroup.inventory.server.entity.Category;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CategoryRepository extends BaseRepository<Category> {

  Boolean existsByName(String name);

  @Query(value = "select count(*) from Category c where c.name IN :names")
  int totalCategory(List<String> names);

  @Query(value = "select * from category c where c.parent_id = ?1 and c.is_deleted = false ", nativeQuery = true)
  List<Category> getCategoryByParentId(String parentId);

  @Query(value = "select c.id from category c where c.parent_id = ?1 and c.is_deleted = false ", nativeQuery = true)
  List<String> getCategoryIdByParentId(String parentId);

  @Query(value = "select * from category c where c.id = ?1", nativeQuery = true)
  Category getCategory(String id);

  @Query(value = "UPDATE Category SET isDeleted = true where id IN :categoryIds")
  @Modifying
  @Transactional
  void deleteSoft(List<String> categoryIds);

  @Query(value = "UPDATE Category SET isDeleted = true where id = :id")
  @Modifying
  @Transactional
  void deleteById(String id);


  @Query(value = "select * from category c where c.id = ?1 and c.is_deleted = false", nativeQuery = true)
  Category findCategory(String id);

  @Query(value = "select * from category c where c.is_deleted = false and c.parent_id is null", nativeQuery = true)
  List<Category> getAllCategoryParent();

  @Query("SELECT COUNT (DISTINCT c.id) FROM Category c WHERE c.id IN :categoryIds and c.isDeleted = false ")
  int countMatching(List<String> categoryIds);

  @Query(value = "SELECT new com.ncsgroup.inventory.server.dto.response.category.CategoryProductResponse(" +
        "c.name, c.imageId) " +
        "FROM Category c where c.id in :categoryIds "
  )
  List<CategoryProductResponse> find(List<String> categoryIds);


}
