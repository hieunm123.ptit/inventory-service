package com.ncsgroup.inventory.server.exception.product;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class ProductNotFoundException extends NotFoundException {
  public ProductNotFoundException(){
    setCode("com.ncsgroup.inventory.server.exception.product.ProductNotFoundException");
  }
}
