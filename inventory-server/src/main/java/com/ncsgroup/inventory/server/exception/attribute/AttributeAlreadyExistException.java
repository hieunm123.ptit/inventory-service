package com.ncsgroup.inventory.server.exception.attribute;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class AttributeAlreadyExistException extends ConflictException {
  public AttributeAlreadyExistException(){
    setCode("com.ncsgroup.inventory.server.exception.attribute.AttributeAlreadyExistException");
  }
}
