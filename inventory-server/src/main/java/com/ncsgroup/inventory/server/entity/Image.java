package com.ncsgroup.inventory.server.entity;

import com.ncsgroup.inventory.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "images")
public class Image extends BaseEntityWithUpdater {
  private String url;
  private String contentType;
  private String name;
  private String bucketName;
  private String productId;
  private boolean isDeleted;

  public static Image from(String url, String contentType, String name, String bucketName){
    Image image = new Image();
    image.setUrl(url);
    image.setName(name);
    image.setContentType(contentType);
    image.setBucketName(bucketName);
    return image;
  }

}
