package com.ncsgroup.inventory.server.entity;

import com.ncsgroup.inventory.server.entity.base.BaseEntityWithUpdater;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
@Entity
@Table(name = "warehouse")

public class Warehouse extends BaseEntityWithUpdater {
  @Column(name = "code")
  private String code;
  @Column(name = "name")
  private String name;
  @Column(name = "is_deleted")
  private boolean isDeleted;
  @Column(name = "phone_number")
  private String phoneNumber;
  @Column(name = "address_id")
  private String addressId;



  public static Warehouse from(
        String name,
        String phoneNumber,
        String addressId

  ) {
    return Warehouse.of(null, name, false, phoneNumber, addressId);
  }

  public Warehouse(
        String code,
        String name,
        String phoneNumber,
        String addressId
  ) {
    this.code = code;
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.addressId = addressId;
  }
}
