package com.ncsgroup.inventory.server.controller;

import com.ncsgroup.inventory.client.dto.AttributeCreateRequest;
import com.ncsgroup.inventory.client.dto.product.ClassificationRequest;
import com.ncsgroup.inventory.server.dto.response.ResponseGeneral;
import com.ncsgroup.inventory.server.dto.response.attribute.AttributeResponse;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.facade.ClassificationFacadeService;
import com.ncsgroup.inventory.server.service.AttributeService;
import com.ncsgroup.inventory.server.service.MessageService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ncsgroup.inventory.server.constanst.Constants.CommonConstants.*;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.CREATE_ATTRIBUTE_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.MessageController.UPDATE_ATTRIBUTE_SUCCESS;
import static com.ncsgroup.inventory.server.constanst.Constants.VariableConstants.*;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/attributes")
public class AttributeController {

  private final AttributeService attributeService;
  private final MessageService messageService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseGeneral<AttributeResponse> create(
        @RequestBody @Valid AttributeCreateRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(create) request: {}", request);
    return ResponseGeneral.ofCreated(
          messageService.getMessage(CREATE_ATTRIBUTE_SUCCESS, language),
          attributeService.create(request)
    );
  }


  @PutMapping("{id}")
  public ResponseGeneral<AttributeResponse> update(
        @PathVariable String id,
        @RequestBody @Valid AttributeCreateRequest request,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language) {

    log.info("(update) id: {}, request: {}", id, request);
    return ResponseGeneral.ofSuccess(
          messageService.getMessage(UPDATE_ATTRIBUTE_SUCCESS, language),
          attributeService.update(id, request)
    );
  }

  @GetMapping
  public ResponseGeneral<PageResponse<AttributeResponse>> list(
        @RequestParam(name = "keyword", required = false) String keyword,
        @RequestParam(name = "size", defaultValue = SIZE_DEFAULT) int size,
        @RequestParam(name = "page", defaultValue = PAGE_DEFAULT) int page,
        @RequestParam(name = "all", defaultValue = IS_ALL_DEFAULT, required = false) boolean isAll,
        @RequestParam(name = "type", defaultValue = TYPE_CLIENT) int type,
        @RequestHeader(name = LANGUAGE, defaultValue = DEFAULT_LANGUAGE) String language
  ) {
    log.info("(list)keyword: {}, size: {}, page: {}, isAll: {}, type: {}", keyword, page, size, isAll, type);

    return ResponseGeneral.ofSuccess(
          messageService.getMessage(SUCCESS, language),
          attributeService.list(keyword, size, page, isAll, type)
    );
  }

}
