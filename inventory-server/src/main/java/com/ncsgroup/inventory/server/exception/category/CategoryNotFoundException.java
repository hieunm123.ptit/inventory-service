package com.ncsgroup.inventory.server.exception.category;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class CategoryNotFoundException extends NotFoundException {
  public CategoryNotFoundException(){
    setCode("com.ncsgroup.inventory.server.exception.category.CategoryNotFoundException");
  }
}
