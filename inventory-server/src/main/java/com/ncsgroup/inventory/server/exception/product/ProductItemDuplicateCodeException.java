package com.ncsgroup.inventory.server.exception.product;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class ProductItemDuplicateCodeException extends ConflictException {
  public ProductItemDuplicateCodeException(){
    setCode("com.ncsgroup.inventory.server.exception.product.ProductItemDuplicateCodeException");
  }

}
