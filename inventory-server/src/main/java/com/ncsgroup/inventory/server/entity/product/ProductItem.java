package com.ncsgroup.inventory.server.entity.product;

import com.ncsgroup.inventory.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.ncsgroup.inventory.server.utils.GeneratorUtils.generateId;

@Entity
@Data
@Table(name = "product_items")
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class ProductItem extends BaseEntityWithUpdater {
  private String code;
  private boolean isDeleted;

  public static ProductItem from(String code) {
    ProductItem productItem = new ProductItem();
    productItem.setId(generateId());
    productItem.setCode(code);
    productItem.setDeleted(false);
    return productItem;
  }
}