package com.ncsgroup.inventory.server.dto.response.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ncsgroup.inventory.server.dto.response.warehouse.WarehouseResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ProductWarehouseResponse {
  private WarehouseResponse warehouseResponse;
  private ProductItemResponse productItemResponse;
  private Double importPrice;
  private Double exportPrice;
  private Integer quantity;
  private Integer quantitySold;
  private String currencyCode;

  public ProductWarehouseResponse(String warehouseCode, String warehouseName, String phoneNumber, String addressId,
                                  String productItemId, String productItemCode,
                                  Double importPrice, Double exportPrice, Integer quantity, Integer quantitySold,
                                  String currencyCode) {
    this.warehouseResponse = WarehouseResponse.from(
          warehouseCode,
          warehouseName,
          phoneNumber,
          addressId
    );
    if (Objects.nonNull(productItemId)) {
      this.productItemResponse = ProductItemResponse.of(productItemId, productItemCode);
    } else {
      this.productItemResponse = null;
    }
    this.importPrice = importPrice;
    this.exportPrice = exportPrice;
    this.quantity = quantity;
    this.quantitySold = quantitySold;
    this.currencyCode = currencyCode;
  }
}
