package com.ncsgroup.inventory.server.facade.impl;

import com.ncsgroup.inventory.server.facade.ProductAttributeFacadeService;
import com.ncsgroup.inventory.server.service.AttributeService;
import com.ncsgroup.inventory.server.service.ProductAttributeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class ProductAttributeFacadeServiceImpl implements ProductAttributeFacadeService {
  private final ProductAttributeService productAttributeService;
  private final AttributeService attributeService;
}
