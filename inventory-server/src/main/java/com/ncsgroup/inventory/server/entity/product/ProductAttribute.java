package com.ncsgroup.inventory.server.entity.product;

import io.micrometer.common.util.StringUtils;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Table(name = "product_attribute")
public class ProductAttribute {
  @Id
  private String id;
  private String productId;
  private String attributeId;
  private String productItemId;
  private String attributeValue;
  private String parentId;
  private boolean isDetail;

  public static ProductAttribute of(
        String productId, String attributeId, String productItemId, String value, String parentId
  ) {
    return of(
          UUID.randomUUID().toString(),
          productId,
          attributeId,
          productItemId,
          value,
          parentId,
          false
    );
  }

  public static ProductAttribute from(
        String productId,
        String attributeId,
        String value
  ) {

    ProductAttribute productAttribute = new ProductAttribute();
    productAttribute.setProductId(productId);
    productAttribute.setAttributeId(attributeId);
    productAttribute.setAttributeValue(value);
    productAttribute.setDetail(true);
    return productAttribute;
  }

  public static ProductAttribute from(
        String id,
        String productId,
        String attributeId,
        String productItemId,
        String value,
        String parentId
  ) {
    ProductAttribute productAttribute = new ProductAttribute();
    productAttribute.setId(id);
    productAttribute.setProductId(productId);
    productAttribute.setAttributeId(attributeId);
    productAttribute.setProductItemId(productItemId);
    productAttribute.setAttributeValue(value);
    productAttribute.setParentId(parentId);
    productAttribute.setDetail(false);

    return productAttribute;
  }

  public static ProductAttribute from(
        String id,
        String productId,
        String attributeId,
        String value,
        String parentId
  ) {
    ProductAttribute productAttribute = new ProductAttribute();
    productAttribute.setId(id);
    productAttribute.setProductId(productId);
    productAttribute.setAttributeId(attributeId);
    productAttribute.setProductItemId(null);
    productAttribute.setAttributeValue(value);
    productAttribute.setParentId(parentId);
    productAttribute.setDetail(false);

    return productAttribute;
  }

  @PrePersist
  public void ensureId() {
    this.id = StringUtils.isBlank(this.id) ? UUID.randomUUID().toString() : this.id;
  }
}
