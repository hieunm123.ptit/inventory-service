package com.ncsgroup.inventory.server.entity;

import com.ncsgroup.inventory.server.entity.enums.Active;
import com.ncsgroup.inventory.server.entity.base.BaseEntityWithUpdater;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Entity
@Table(name = "attributes")
public class Attribute extends BaseEntityWithUpdater {
  private String name;
  private Boolean isDeleted;
  @Enumerated(EnumType.ORDINAL)
  private Active isActive;

  public static Attribute from(String name, Active isActive) {
    return Attribute.of(name, false, isActive);
  }

}
