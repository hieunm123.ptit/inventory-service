package com.ncsgroup.inventory.server.exception.promotion;

import com.ncsgroup.inventory.server.exception.base.NotFoundException;

public class PromotionNotFoundException extends NotFoundException {
  public PromotionNotFoundException(){
    setCode("com.ncsgroup.inventory.server.exception.base.PromotionNotFoundException");
  }
}
