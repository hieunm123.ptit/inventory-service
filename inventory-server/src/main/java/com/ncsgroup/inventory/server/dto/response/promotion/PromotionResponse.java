package com.ncsgroup.inventory.server.dto.response.promotion;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import com.ncsgroup.inventory.server.entity.enums.Active;

import com.ncsgroup.inventory.server.utils.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionResponse {
  private String code;
  private String startDay;
  private String endDay;
  private Integer type;
  private Double discountPercent;
  private Integer moneyDeducted;
  private String description;
  private Integer coin;
  private Active isActive;

  public static PromotionResponse from(
        String code,
        String startDay,
        String endDay,
        Integer type,
        Double discountPercent,
        Integer moneyDeducted,
        String description,
        Integer coin,
        Active isActive
  ) {
    return PromotionResponse.of(code, startDay, endDay, type, discountPercent, moneyDeducted, description, coin, isActive);
  }

  public PromotionResponse(
        String code,
        Long startDay,
        Long endDay,
        Double discountPercent,
        Integer moneyDeducted,
        String description,
        Integer coin,
        Active isActive
  ) {
    this.code = code;
    this.startDay = DateUtils.convertToDateString(startDay);
    this.endDay = DateUtils.convertToDateString(endDay);
    this.discountPercent = discountPercent;
    this.moneyDeducted = moneyDeducted;
    this.description = description;
    this.coin = coin;
    this.isActive = isActive;
  }

}
