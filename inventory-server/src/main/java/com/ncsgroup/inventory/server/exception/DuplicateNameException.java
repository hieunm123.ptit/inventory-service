package com.ncsgroup.inventory.server.exception;

import com.ncsgroup.inventory.server.exception.base.ConflictException;

public class DuplicateNameException extends ConflictException {
  public DuplicateNameException(){
    setCode("com.ncsgroup.inventory.server.exception.DuplicateNameException");
  }
}
