package com.ncsgroup.inventory.server.service;

import com.ncsgroup.inventory.client.dto.product.ProductAdminFilterRequest;
import com.ncsgroup.inventory.client.dto.product.ProductFilterRequest;
import com.ncsgroup.inventory.client.dto.product.ProductOrderRequest;
import com.ncsgroup.inventory.client.dto.product.ProductRequest;
import com.ncsgroup.inventory.server.dto.response.base.PageResponse;
import com.ncsgroup.inventory.server.dto.response.cart.CartItemResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductAdminResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductClientResponse;
import com.ncsgroup.inventory.server.dto.response.product.ProductResponse;

import java.util.List;

public interface ProductService {
  ProductResponse create(ProductRequest request);


  ProductResponse update(String id, ProductRequest request);

  ProductResponse detail(String id);

  ProductResponse updateActive(String id);

  PageResponse<ProductClientResponse> listInClientSide(int page, int size);


  PageResponse<ProductClientResponse> filterInClientSide(ProductFilterRequest request, int page, int size);

  PageResponse<ProductAdminResponse> listInAdminSide(int page, int size);

  PageResponse<ProductAdminResponse> filterInAdminSide(ProductAdminFilterRequest request, int page, int size);

  List<CartItemResponse> getItems(List<String> itemIds);

  void deductProduct(List<ProductOrderRequest> request, String warehouseId);

  void deleteById(String id);

  void checkProductExist(String id);

  void checkProductExist(List<String> id);

}
